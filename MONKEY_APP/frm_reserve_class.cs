﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThaiNationalIDCard;

namespace MONKEY_APP
{
    public partial class frm_reserve_class : Form
    {
        private DataTable dt;
        private DataTable dtm;
        public ReserveClass reserveClass;
        public string active = "";
        private string picCus = "";

        private void bs_Click(object sender, EventArgs e)
        {
            DataView dv = dt.DefaultView;
            dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);
            dataGridView1.DataSource = dv.ToTable();
            if (dataGridView1.Rows.Count > 0)
            {
                dataGridView1_CellClick(sender, null);
            }
            else if (dataGridView1.Rows.Count == 0)
            {
                dataGridView1.ClearSelection();
                perSonCode = "";
                clearData();
            }

            if (txtSearch.Text.Equals(""))
            {
                dataGridView1.ClearSelection();
                perSonCode = "";
                clearData();
            }
            else
            {
                dataGridView1.ClearSelection();
            }
        }

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        public frm_reserve_class(ReserveClass reserveClass)
        {
            this.reserveClass = reserveClass;
            InitializeComponent();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }



        private void frm_manage_cus_Load(object sender, EventArgs e)
        {
            txtClassName.Text = reserveClass.name_class;
            txtDate.Text = reserveClass.date_class_txt;
            txtTime.Text = reserveClass.time_start + " - " + reserveClass.time_end;

            if (!reserveClass.image_class.Equals(""))
            {
                Utils.setImage(reserveClass.image_class, picClass);
            }
             

            getPersons();
            reserveList(reserveClass.schedule_day_id);
        }


        private void getPersons()
        {
            try
            {
                dt = new DataTable();
                dt.Columns.Add("รหัส", typeof(string));
                dt.Columns.Add("ชื่อเล่น", typeof(string));
                dt.Columns.Add("ชื่อ", typeof(string));
                dt.Columns.Add("โทษแบน", typeof(string));
                dt.Columns.Add("Note", typeof(string));
                dt.Columns.Add("รูป", typeof(string));
                dt.Columns.Add("เพศ", typeof(string));
                dt.Columns.Add("ID", typeof(string));
                dt.Columns.Add("regDate", typeof(string));
                dt.Columns.Add("expDatre", typeof(string));
                dt.Columns.Add("status", typeof(string));
                
                for (int i = 0; i < Global.PersonList.Count(); i++)
                {
                    Person person = Global.PersonList[i];

                    dt.Rows.Add(
                        person.PERSON_CODE, person.PERSON_NICKNAME, person.PERSON_TITLE + person.PERSON_NAME+" "+ person.PERSON_LASTNAME,
                        person.BAN_RESERVE_TXT,
                        person.PERSON_NOTE, person.PERSON_IMAGE, person.PERSON_SEX, person.PERSON_CARD_ID,
                        person.PERSON_REGISTER_DATE_TXT, person.PERSON_EXPIRE_DATE_TXT, person.PERSON_STATUS
                    );
                }

                dataGridView1.DataSource = dt;

                dataGridView1.Columns[0].Width = 100;
                dataGridView1.Columns[1].Width = 107;
                dataGridView1.Columns[2].Width = 211;
                dataGridView1.Columns[3].Width = 100;

                dataGridView1.Columns[4].Visible = false;
                dataGridView1.Columns[5].Visible = false;
                dataGridView1.Columns[6].Visible = false;
                dataGridView1.Columns[7].Visible = false;
                dataGridView1.Columns[8].Visible = false;
                dataGridView1.Columns[9].Visible = false;
                dataGridView1.Columns[10].Visible = false;

                dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: getPersons ::" + ex.ToString(), "frm_reserve_class");
            }
        }


        public static byte[] ImageToByte(Image img)
        {
            byte[] byteArr;
            try
            {
                ImageConverter converter = new ImageConverter();
                var i2 = new Bitmap(img);
                byteArr = (byte[])converter.ConvertTo(i2, typeof(byte[]));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
            return byteArr;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = dt.DefaultView;
                dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);
                dataGridView1.DataSource = dv.ToTable();

                if (dataGridView1.Rows.Count > 0)
                {
                    dataGridView1_CellClick(sender, null);
                }
                else if (dataGridView1.Rows.Count == 0)
                {
                    dataGridView1.ClearSelection();
                    perSonCode = "";
                    clearData();
                }

                if (txtSearch.Text.Equals(""))
                {
                    dataGridView1.ClearSelection();
                    perSonCode = "";
                    clearData();
                }
                else
                {
                    dataGridView1.ClearSelection();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            reserveClass.status = "C";
            this.DialogResult = DialogResult.OK;
        }

        private void frm_reserve_class_Shown(object sender, EventArgs e)
        {
            dataGridView1.ClearSelection();
            clearData();
        }

        private void clearData()
        {
            cusCode.Text = "";
            txtName.Text = "";
            reserveClass.PERSON_CODE = "";
            reserveClass.person_name = "";
            button1.Enabled = false;
            button4.Enabled = false;
        }


        private string perSonCode = "";


        private void button2_Click(object sender, EventArgs e)
        {
            frm_fg_checkin_member fgc = new frm_fg_checkin_member("All", "", "PERSON");
            var result = fgc.ShowDialog();

            if (result == DialogResult.OK)
            {
                txtSearch.Text = Global.PERSON.PERSON_CODE;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            reserveClass.status = "S";
            this.DialogResult = DialogResult.OK;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                clearData();
                if (dataGridView1.CurrentCell != null)
                {
                   int rowindex = dataGridView1.CurrentCell.RowIndex;
                   if (dataGridView1.Rows[rowindex].Cells[3].Value.ToString().Equals(""))
                    {

                        perSonCode = dataGridView1.Rows[rowindex].Cells[0].Value.ToString();

                        cusCode.Text = perSonCode;
                        txtName.Text = dataGridView1.Rows[rowindex].Cells[2].Value.ToString();

                        if (!dataGridView1.Rows[rowindex].Cells[5].Value.ToString().Equals(""))
                        {
                            picCus = dataGridView1.Rows[rowindex].Cells[5].Value.ToString();
                            Utils.setImage(picCus, picPerson);
                        }
                        else
                        {
                            picCus = "";
                            picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                            picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
                            picPerson.Size = new System.Drawing.Size(143, 152);
                        }
                        button1.Enabled = true;
                        button4.Enabled = true;
                        reserveClass.PERSON_CODE    = cusCode.Text;
                        reserveClass.person_name    = txtName.Text;
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        private async void reserveList(string schedule_day_id)
        {
            try
            {
                List<ReserveClass> reserveClassList = await ApiProcess.getReserveClassList(schedule_day_id);

                Bitmap Image    = MONKEY_APP.Properties.Resources.printer_tool30;
                Bitmap Image2   = MONKEY_APP.Properties.Resources.error30;
                Bitmap Image3   = MONKEY_APP.Properties.Resources.error30_d;
                Bitmap checkin  = MONKEY_APP.Properties.Resources.point;

                
                byte[] cancelImage = ImageToByte(Image2);

                string status;
                byte[] printImage;

                dtm = new DataTable();
                dtm.Columns.Add("   ", typeof(byte[]));
                dtm.Columns.Add("วันที่", typeof(string));
                dtm.Columns.Add("เวลา", typeof(string));
                dtm.Columns.Add("รายการ", typeof(string));
                dtm.Columns.Add("ชื่อ - สกุล", typeof(string));
                dtm.Columns.Add("ชื่อเล่น", typeof(string));
                dtm.Columns.Add("วันที่ทำรายการ", typeof(string));
                dtm.Columns.Add("หมดอายุ", typeof(string));
                dtm.Columns.Add("สถานะ", typeof(string));
                dtm.Columns.Add("reserve_id", typeof(string));
                dtm.Columns.Add("ยกเลิก", typeof(byte[]));
                dtm.Columns.Add("schedule_day_id", typeof(string));
                dtm.Columns.Add("date_class", typeof(string));
                dtm.Columns.Add("time_start", typeof(string));
                dtm.Columns.Add("time_end", typeof(string));
                dtm.Columns.Add("EMP_NICKNAME", typeof(string));
                dtm.Columns.Add("PERSON_CODE", typeof(string));
                dtm.Columns.Add("status", typeof(string));
                dtm.Columns.Add("unit", typeof(string));
                dtm.Columns.Add("person_join", typeof(string));


                if (reserveClassList != null)
                {
                    for (int i = 0; i < reserveClassList.Count(); i++)
                    {
                        ReserveClass rs = reserveClassList[i];
                        //if (Global.USER.EMP_IS_STAFF != null && Global.USER.EMP_IS_STAFF.Equals("Y"))
                        //{
                        //    cancelImage = ImageToByte(Image2);
                        //}                     
                        
                        if (rs.status.Equals("S"))
                        {
                            status = "จอง";
                            printImage = ImageToByte(checkin);
                        }
                        else
                        {
                            printImage = ImageToByte(Image);
                            status = "Check In";
                        }

                        dtm.Rows.Add(
                            printImage, Utils.getDateEntoTh(rs.date_class), rs.time_start + " - " + rs.time_end, rs.name_class, rs.person_name, rs.PERSON_NICKNAME,
                            Utils.getDateTimeEntoTh(rs.date_reserve), Utils.getDateTimeEntoTh(rs.expire_date), status, rs.reserve_id, cancelImage, rs.schedule_day_id,
                            rs.date_class, rs.time_start, rs.time_end, rs.EMP_NICKNAME, rs.PERSON_CODE, rs.status, rs.unit, rs.person_join);

                    }
                }

                dataGridView2.DataSource = dtm;

                dataGridView2.Columns[0].Width = 70;
                dataGridView2.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[1].Width = 90;
                dataGridView2.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;


                dataGridView2.Columns[2].Width = 100;
                dataGridView2.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;


                dataGridView2.Columns[3].Width = 120;
                dataGridView2.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dataGridView2.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[4].Width = 200;
                dataGridView2.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dataGridView2.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[5].Width = 115;
                dataGridView2.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dataGridView2.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;


                dataGridView2.Columns[6].Width = 140;
                dataGridView2.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[7].Width = 140;
                dataGridView2.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[8].Width = 98;
                dataGridView2.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[1].Visible = false;
                dataGridView2.Columns[2].Visible = false;
                dataGridView2.Columns[3].Visible = false;
                dataGridView2.Columns[9].Visible = false;
                dataGridView2.Columns[11].Visible = false;
                dataGridView2.Columns[12].Visible = false;
                dataGridView2.Columns[13].Visible = false;
                dataGridView2.Columns[14].Visible = false;
                dataGridView2.Columns[15].Visible = false;
                dataGridView2.Columns[16].Visible = false;
                dataGridView2.Columns[17].Visible = false;
                dataGridView2.Columns[18].Visible = false;
                dataGridView2.Columns[19].Visible = false;

                dataGridView2.Columns[10].Width = 70;
                dataGridView2.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[10].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch
            {

            }
        }

        private async void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowindex = dataGridView2.CurrentCell.RowIndex;

                ReserveClass reserveClass = new ReserveClass();

                reserveClass.reserve_id = dataGridView2.Rows[rowindex].Cells[9].Value.ToString();
                reserveClass.schedule_day_id = dataGridView2.Rows[rowindex].Cells[11].Value.ToString();
                reserveClass.name_class = dataGridView2.Rows[rowindex].Cells[3].Value.ToString();
                reserveClass.date_class = dataGridView2.Rows[rowindex].Cells[12].Value.ToString();
                reserveClass.time_start = dataGridView2.Rows[rowindex].Cells[13].Value.ToString();
                reserveClass.time_end = dataGridView2.Rows[rowindex].Cells[14].Value.ToString();
                reserveClass.EMP_NICKNAME = dataGridView2.Rows[rowindex].Cells[15].Value.ToString();
                reserveClass.PERSON_CODE = dataGridView2.Rows[rowindex].Cells[16].Value.ToString();
                reserveClass.person_name = dataGridView2.Rows[rowindex].Cells[4].Value.ToString();
                reserveClass.status      = dataGridView2.Rows[rowindex].Cells[17].Value.ToString();
                reserveClass.unit         = int.Parse(dataGridView2.Rows[rowindex].Cells[18].Value.ToString());
                reserveClass.person_join  = int.Parse(dataGridView2.Rows[rowindex].Cells[19].Value.ToString());


                if (e.ColumnIndex == dataGridView2.Columns["   "].Index)
                {
                    picLoading.Visible = true;
                    if (reserveClass.status.Equals("S"))
                    {
                        ApiRest.InitailizeClient();
                        Response ress = await ApiProcess.managerReserveClass(Global.BRANCH.branch_code, "CHANGE", reserveClass);

                        Bitmap Image = MONKEY_APP.Properties.Resources.printer_tool30;

                        dataGridView2.Rows[rowindex].Cells[0].Value = ImageToByte(Image);
                        dataGridView2.Rows[rowindex].Cells[8].Value = "Check In";

                    }
                    Utils.eformReserveClass(reserveClass);

                }

                if (dataGridView2.Columns["ยกเลิก"] != null && e.ColumnIndex == dataGridView2.Columns["ยกเลิก"].Index)
                {
                    picLoading.Visible = true;
                    frm_dialog d = new frm_dialog("ยืนยันการลบรายการ ! \r\nสมาชิก : " + reserveClass.person_name, "");
                    d.ShowDialog();
                    if (d.DialogResult == DialogResult.OK)
                    {
                        ApiRest.InitailizeClient();
                        Response ress = await ApiProcess.managerReserveClass(Global.BRANCH.branch_code, "DEL", reserveClass);
                        if (ress.status)
                        {
                            dataGridView2.Rows.RemoveAt(rowindex);
                            active = "CHANGE";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: dataGridView1_CellClick ::" + ex.ToString(), "frm_history_inv");
            }
            finally
            {
                picLoading.Visible = false;
            }
        }
    }
}
