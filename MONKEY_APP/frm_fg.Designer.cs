﻿namespace MONKEY_APP
{
    partial class frm_fg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_fg));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TextSensorSN = new System.Windows.Forms.TextBox();
            this.TextSensorIndex = new System.Windows.Forms.TextBox();
            this.Frame2 = new System.Windows.Forms.GroupBox();
            this.OptionBmp = new System.Windows.Forms.RadioButton();
            this.OptionJpg = new System.Windows.Forms.RadioButton();
            this.cmdSaveImage = new System.Windows.Forms.Button();
            this.cmdInit = new System.Windows.Forms.Button();
            this.TextSensorCount = new System.Windows.Forms.TextBox();
            this.StatusBar = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labCountFng = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ZKFPEngX1 = new AxZKFPEngXControl.AxZKFPEngX();
            this.cmdBeep = new System.Windows.Forms.Button();
            this.cmdGreen = new System.Windows.Forms.Button();
            this.cmdRED = new System.Windows.Forms.Button();
            this.cmdEnroll = new System.Windows.Forms.Button();
            this.cmdVerify = new System.Windows.Forms.Button();
            this.cmdIdentify = new System.Windows.Forms.Button();
            this.TextFingerName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.labf5 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.fig2 = new System.Windows.Forms.PictureBox();
            this.fig1 = new System.Windows.Forms.PictureBox();
            this.fig6 = new System.Windows.Forms.PictureBox();
            this.fig7 = new System.Windows.Forms.PictureBox();
            this.fig3 = new System.Windows.Forms.PictureBox();
            this.fig8 = new System.Windows.Forms.PictureBox();
            this.fig4 = new System.Windows.Forms.PictureBox();
            this.fig9 = new System.Windows.Forms.PictureBox();
            this.fig5 = new System.Windows.Forms.PictureBox();
            this.fig10 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnf5 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.btnf4 = new System.Windows.Forms.Button();
            this.labf4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.btnf3 = new System.Windows.Forms.Button();
            this.labf3 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.btnf2 = new System.Windows.Forms.Button();
            this.labf2 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.btnf1 = new System.Windows.Forms.Button();
            this.labf1 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ZKFPEngX1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.labName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.TextSensorSN);
            this.panel1.Controls.Add(this.TextSensorIndex);
            this.panel1.Controls.Add(this.Frame2);
            this.panel1.Controls.Add(this.cmdSaveImage);
            this.panel1.Controls.Add(this.cmdInit);
            this.panel1.Controls.Add(this.TextSensorCount);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1117, 58);
            this.panel1.TabIndex = 116;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::MONKEY_APP.Properties.Resources.fingerprint_scanning2;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(13, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(38, 37);
            this.pictureBox1.TabIndex = 117;
            this.pictureBox1.TabStop = false;
            // 
            // labName
            // 
            this.labName.AutoSize = true;
            this.labName.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(150)))), ((int)(((byte)(121)))));
            this.labName.Location = new System.Drawing.Point(207, 15);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(41, 27);
            this.labName.TabIndex = 116;
            this.labName.Text = "text";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(58, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 27);
            this.label1.TabIndex = 116;
            this.label1.Text = "เก็บข้อมูลลายนิ้วมือ";
            // 
            // button7
            // 
            this.button7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.button7.BackgroundImage = global::MONKEY_APP.Properties.Resources.close__1_;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(1087, 9);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(20, 20);
            this.button7.TabIndex = 114;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(754, 9);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(73, 17);
            this.label6.TabIndex = 126;
            this.label6.Text = "จำนวนหัวอ่าน";
            this.label6.Visible = false;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.Control;
            this.label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(754, 33);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label8.Size = new System.Drawing.Size(89, 25);
            this.label8.TabIndex = 124;
            this.label8.Text = "Sensor SN";
            this.label8.Visible = false;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(938, 9);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label7.Size = new System.Drawing.Size(49, 17);
            this.label7.TabIndex = 125;
            this.label7.Text = "หัวอ่าน:";
            this.label7.Visible = false;
            // 
            // TextSensorSN
            // 
            this.TextSensorSN.AcceptsReturn = true;
            this.TextSensorSN.BackColor = System.Drawing.SystemColors.Window;
            this.TextSensorSN.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSensorSN.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TextSensorSN.Location = new System.Drawing.Point(826, 33);
            this.TextSensorSN.MaxLength = 0;
            this.TextSensorSN.Name = "TextSensorSN";
            this.TextSensorSN.ReadOnly = true;
            this.TextSensorSN.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextSensorSN.Size = new System.Drawing.Size(217, 20);
            this.TextSensorSN.TabIndex = 121;
            this.TextSensorSN.Visible = false;
            // 
            // TextSensorIndex
            // 
            this.TextSensorIndex.AcceptsReturn = true;
            this.TextSensorIndex.BackColor = System.Drawing.SystemColors.Window;
            this.TextSensorIndex.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSensorIndex.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TextSensorIndex.Location = new System.Drawing.Point(986, 9);
            this.TextSensorIndex.MaxLength = 0;
            this.TextSensorIndex.Name = "TextSensorIndex";
            this.TextSensorIndex.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextSensorIndex.Size = new System.Drawing.Size(57, 20);
            this.TextSensorIndex.TabIndex = 122;
            this.TextSensorIndex.Visible = false;
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.SystemColors.Control;
            this.Frame2.Controls.Add(this.OptionBmp);
            this.Frame2.Controls.Add(this.OptionJpg);
            this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame2.Location = new System.Drawing.Point(470, 9);
            this.Frame2.Name = "Frame2";
            this.Frame2.Padding = new System.Windows.Forms.Padding(0);
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(145, 41);
            this.Frame2.TabIndex = 128;
            this.Frame2.TabStop = false;
            this.Frame2.Text = "ชนิดของรูป";
            this.Frame2.Visible = false;
            // 
            // OptionBmp
            // 
            this.OptionBmp.BackColor = System.Drawing.SystemColors.Control;
            this.OptionBmp.Checked = true;
            this.OptionBmp.Cursor = System.Windows.Forms.Cursors.Default;
            this.OptionBmp.ForeColor = System.Drawing.SystemColors.ControlText;
            this.OptionBmp.Location = new System.Drawing.Point(8, 16);
            this.OptionBmp.Name = "OptionBmp";
            this.OptionBmp.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OptionBmp.Size = new System.Drawing.Size(49, 17);
            this.OptionBmp.TabIndex = 4;
            this.OptionBmp.TabStop = true;
            this.OptionBmp.Text = "BMP";
            this.OptionBmp.UseVisualStyleBackColor = false;
            // 
            // OptionJpg
            // 
            this.OptionJpg.BackColor = System.Drawing.SystemColors.Control;
            this.OptionJpg.Cursor = System.Windows.Forms.Cursors.Default;
            this.OptionJpg.ForeColor = System.Drawing.SystemColors.ControlText;
            this.OptionJpg.Location = new System.Drawing.Point(80, 16);
            this.OptionJpg.Name = "OptionJpg";
            this.OptionJpg.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OptionJpg.Size = new System.Drawing.Size(57, 17);
            this.OptionJpg.TabIndex = 3;
            this.OptionJpg.TabStop = true;
            this.OptionJpg.Text = "JPEG";
            this.OptionJpg.UseVisualStyleBackColor = false;
            // 
            // cmdSaveImage
            // 
            this.cmdSaveImage.BackColor = System.Drawing.SystemColors.Control;
            this.cmdSaveImage.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdSaveImage.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdSaveImage.Location = new System.Drawing.Point(341, 10);
            this.cmdSaveImage.Name = "cmdSaveImage";
            this.cmdSaveImage.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdSaveImage.Size = new System.Drawing.Size(113, 25);
            this.cmdSaveImage.TabIndex = 127;
            this.cmdSaveImage.Text = "บันทึกรูปลายนิ้วมือ";
            this.cmdSaveImage.UseVisualStyleBackColor = false;
            this.cmdSaveImage.Visible = false;
            this.cmdSaveImage.Click += new System.EventHandler(this.cmdSaveImage_Click);
            // 
            // cmdInit
            // 
            this.cmdInit.BackColor = System.Drawing.SystemColors.Control;
            this.cmdInit.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdInit.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdInit.Location = new System.Drawing.Point(621, 7);
            this.cmdInit.Name = "cmdInit";
            this.cmdInit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdInit.Size = new System.Drawing.Size(113, 25);
            this.cmdInit.TabIndex = 129;
            this.cmdInit.Text = "เชื่อมต่อระบบ";
            this.cmdInit.UseVisualStyleBackColor = false;
            this.cmdInit.Visible = false;
            this.cmdInit.Click += new System.EventHandler(this.cmdInit_Click);
            // 
            // TextSensorCount
            // 
            this.TextSensorCount.AcceptsReturn = true;
            this.TextSensorCount.BackColor = System.Drawing.SystemColors.Window;
            this.TextSensorCount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSensorCount.Enabled = false;
            this.TextSensorCount.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TextSensorCount.Location = new System.Drawing.Point(826, 9);
            this.TextSensorCount.MaxLength = 0;
            this.TextSensorCount.Name = "TextSensorCount";
            this.TextSensorCount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextSensorCount.Size = new System.Drawing.Size(65, 20);
            this.TextSensorCount.TabIndex = 123;
            this.TextSensorCount.Visible = false;
            // 
            // StatusBar
            // 
            this.StatusBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.StatusBar.Cursor = System.Windows.Forms.Cursors.Default;
            this.StatusBar.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.StatusBar.ForeColor = System.Drawing.Color.Yellow;
            this.StatusBar.Location = new System.Drawing.Point(621, 290);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StatusBar.Size = new System.Drawing.Size(458, 32);
            this.StatusBar.TabIndex = 138;
            this.StatusBar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH SarabunPSK", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.label2.Location = new System.Drawing.Point(228, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 39);
            this.label2.TabIndex = 116;
            this.label2.Text = "เลือกสแกนนิ้วมือ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("TH SarabunPSK", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.label3.Location = new System.Drawing.Point(185, 496);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(191, 39);
            this.label3.TabIndex = 116;
            this.label3.Text = "เก็บลายนิ้วมือแล้ว";
            // 
            // labCountFng
            // 
            this.labCountFng.AutoSize = true;
            this.labCountFng.Font = new System.Drawing.Font("TH SarabunPSK", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labCountFng.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.labCountFng.Location = new System.Drawing.Point(379, 486);
            this.labCountFng.Name = "labCountFng";
            this.labCountFng.Size = new System.Drawing.Size(41, 53);
            this.labCountFng.TabIndex = 116;
            this.labCountFng.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("TH SarabunPSK", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.label5.Location = new System.Drawing.Point(423, 496);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 39);
            this.label5.TabIndex = 116;
            this.label5.Text = "นิ้ว";
            // 
            // ZKFPEngX1
            // 
            this.ZKFPEngX1.Enabled = true;
            this.ZKFPEngX1.Location = new System.Drawing.Point(351, 69);
            this.ZKFPEngX1.Name = "ZKFPEngX1";
            this.ZKFPEngX1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("ZKFPEngX1.OcxState")));
            this.ZKFPEngX1.Size = new System.Drawing.Size(24, 24);
            this.ZKFPEngX1.TabIndex = 119;
            this.ZKFPEngX1.OnFeatureInfo += new AxZKFPEngXControl.IZKFPEngXEvents_OnFeatureInfoEventHandler(this.ZKFPEngX1_OnFeatureInfo);
            this.ZKFPEngX1.OnImageReceived += new AxZKFPEngXControl.IZKFPEngXEvents_OnImageReceivedEventHandler(this.ZKFPEngX1_OnImageReceived);
            this.ZKFPEngX1.OnEnroll += new AxZKFPEngXControl.IZKFPEngXEvents_OnEnrollEventHandler(this.ZKFPEngX1_OnEnroll);
            this.ZKFPEngX1.OnFingerTouching += new System.EventHandler(this.ZKFPEngX1_OnFingerTouching);
            // 
            // cmdBeep
            // 
            this.cmdBeep.Location = new System.Drawing.Point(941, 526);
            this.cmdBeep.Name = "cmdBeep";
            this.cmdBeep.Size = new System.Drawing.Size(83, 26);
            this.cmdBeep.TabIndex = 137;
            this.cmdBeep.Text = "Beep";
            this.cmdBeep.UseVisualStyleBackColor = true;
            this.cmdBeep.Visible = false;
            this.cmdBeep.Click += new System.EventHandler(this.cmdBeep_Click);
            // 
            // cmdGreen
            // 
            this.cmdGreen.Location = new System.Drawing.Point(853, 526);
            this.cmdGreen.Name = "cmdGreen";
            this.cmdGreen.Size = new System.Drawing.Size(83, 26);
            this.cmdGreen.TabIndex = 136;
            this.cmdGreen.Text = "Green LED";
            this.cmdGreen.UseVisualStyleBackColor = true;
            this.cmdGreen.Visible = false;
            this.cmdGreen.Click += new System.EventHandler(this.cmdGreen_Click);
            // 
            // cmdRED
            // 
            this.cmdRED.Location = new System.Drawing.Point(764, 526);
            this.cmdRED.Name = "cmdRED";
            this.cmdRED.Size = new System.Drawing.Size(83, 26);
            this.cmdRED.TabIndex = 135;
            this.cmdRED.Text = "Red LED";
            this.cmdRED.UseVisualStyleBackColor = true;
            this.cmdRED.Visible = false;
            this.cmdRED.Click += new System.EventHandler(this.cmdRED_Click);
            // 
            // cmdEnroll
            // 
            this.cmdEnroll.BackColor = System.Drawing.SystemColors.Control;
            this.cmdEnroll.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdEnroll.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdEnroll.Location = new System.Drawing.Point(341, 41);
            this.cmdEnroll.Name = "cmdEnroll";
            this.cmdEnroll.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdEnroll.Size = new System.Drawing.Size(113, 25);
            this.cmdEnroll.TabIndex = 133;
            this.cmdEnroll.Text = "เก็บลายนิ้วมือ";
            this.cmdEnroll.UseVisualStyleBackColor = false;
            this.cmdEnroll.Visible = false;
            this.cmdEnroll.Click += new System.EventHandler(this.cmdEnroll_Click);
            // 
            // cmdVerify
            // 
            this.cmdVerify.BackColor = System.Drawing.SystemColors.Control;
            this.cmdVerify.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdVerify.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdVerify.Location = new System.Drawing.Point(478, 526);
            this.cmdVerify.Name = "cmdVerify";
            this.cmdVerify.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdVerify.Size = new System.Drawing.Size(137, 25);
            this.cmdVerify.TabIndex = 132;
            this.cmdVerify.Text = "ค้นหาแบบ (1:1)";
            this.cmdVerify.UseVisualStyleBackColor = false;
            this.cmdVerify.Visible = false;
            this.cmdVerify.Click += new System.EventHandler(this.cmdVerify_Click);
            // 
            // cmdIdentify
            // 
            this.cmdIdentify.BackColor = System.Drawing.SystemColors.Control;
            this.cmdIdentify.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdIdentify.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdIdentify.Location = new System.Drawing.Point(621, 526);
            this.cmdIdentify.Name = "cmdIdentify";
            this.cmdIdentify.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdIdentify.Size = new System.Drawing.Size(137, 25);
            this.cmdIdentify.TabIndex = 131;
            this.cmdIdentify.Text = "ค้นหาแบบ (1:N)";
            this.cmdIdentify.UseVisualStyleBackColor = false;
            this.cmdIdentify.Visible = false;
            this.cmdIdentify.Click += new System.EventHandler(this.cmdIdentify_Click);
            // 
            // TextFingerName
            // 
            this.TextFingerName.AcceptsReturn = true;
            this.TextFingerName.BackColor = System.Drawing.SystemColors.Window;
            this.TextFingerName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextFingerName.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TextFingerName.Location = new System.Drawing.Point(1011, 58);
            this.TextFingerName.MaxLength = 0;
            this.TextFingerName.Name = "TextFingerName";
            this.TextFingerName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextFingerName.Size = new System.Drawing.Size(105, 20);
            this.TextFingerName.TabIndex = 130;
            this.TextFingerName.Text = "fingerprint1";
            this.TextFingerName.Visible = false;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.SystemColors.Control;
            this.label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(955, 61);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label9.Size = new System.Drawing.Size(50, 20);
            this.label9.TabIndex = 134;
            this.label9.Text = "ชื่อนิ้ว :";
            this.label9.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.pictureBox5);
            this.panel2.Location = new System.Drawing.Point(621, 78);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(183, 200);
            this.panel2.TabIndex = 139;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.White;
            this.pictureBox5.Image = global::MONKEY_APP.Properties.Resources.fingerprint__1_;
            this.pictureBox5.Location = new System.Drawing.Point(25, 21);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(133, 148);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 120;
            this.pictureBox5.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("TH SarabunPSK", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.label10.Location = new System.Drawing.Point(69, 429);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 39);
            this.label10.TabIndex = 140;
            this.label10.Text = "Left";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("TH SarabunPSK", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.label11.Location = new System.Drawing.Point(483, 429);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 39);
            this.label11.TabIndex = 140;
            this.label11.Text = "Right";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.label12.Location = new System.Drawing.Point(616, 335);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(189, 27);
            this.label12.TabIndex = 140;
            this.label12.Text = "ขั้นตอนการบันทึกลายนิ้วมือ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("TH SarabunPSK", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.label17.Location = new System.Drawing.Point(3, 1);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(23, 30);
            this.label17.TabIndex = 140;
            this.label17.Text = "5";
            // 
            // labf5
            // 
            this.labf5.BackColor = System.Drawing.Color.GreenYellow;
            this.labf5.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labf5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.labf5.Location = new System.Drawing.Point(29, 2);
            this.labf5.Name = "labf5";
            this.labf5.Size = new System.Drawing.Size(195, 30);
            this.labf5.TabIndex = 140;
            this.labf5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labf5.Visible = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = global::MONKEY_APP.Properties.Resources.fingerprint_scanning;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(135, 492);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(44, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 118;
            this.pictureBox4.TabStop = false;
            // 
            // fig2
            // 
            this.fig2.BackColor = System.Drawing.Color.Transparent;
            this.fig2.BackgroundImage = global::MONKEY_APP.Properties.Resources._2h;
            this.fig2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fig2.Location = new System.Drawing.Point(365, 165);
            this.fig2.Name = "fig2";
            this.fig2.Size = new System.Drawing.Size(35, 35);
            this.fig2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fig2.TabIndex = 118;
            this.fig2.TabStop = false;
            this.fig2.Tag = "2";
            this.fig2.Click += new System.EventHandler(this.savefinger);
            this.fig2.MouseLeave += new System.EventHandler(this.figMouseLeave);
            this.fig2.MouseHover += new System.EventHandler(this.figMouseHover);
            // 
            // fig1
            // 
            this.fig1.BackColor = System.Drawing.Color.Transparent;
            this.fig1.BackgroundImage = global::MONKEY_APP.Properties.Resources._1h;
            this.fig1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fig1.Location = new System.Drawing.Point(307, 281);
            this.fig1.Name = "fig1";
            this.fig1.Size = new System.Drawing.Size(52, 36);
            this.fig1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fig1.TabIndex = 118;
            this.fig1.TabStop = false;
            this.fig1.Tag = "1";
            this.fig1.Click += new System.EventHandler(this.savefinger);
            this.fig1.MouseLeave += new System.EventHandler(this.figMouseLeave);
            this.fig1.MouseHover += new System.EventHandler(this.figMouseHover);
            // 
            // fig6
            // 
            this.fig6.BackColor = System.Drawing.Color.Transparent;
            this.fig6.BackgroundImage = global::MONKEY_APP.Properties.Resources._6h;
            this.fig6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fig6.Location = new System.Drawing.Point(252, 281);
            this.fig6.Name = "fig6";
            this.fig6.Size = new System.Drawing.Size(52, 36);
            this.fig6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fig6.TabIndex = 118;
            this.fig6.TabStop = false;
            this.fig6.Tag = "6";
            this.fig6.Click += new System.EventHandler(this.savefinger);
            this.fig6.MouseLeave += new System.EventHandler(this.figMouseLeave);
            this.fig6.MouseHover += new System.EventHandler(this.figMouseHover);
            // 
            // fig7
            // 
            this.fig7.BackColor = System.Drawing.Color.Transparent;
            this.fig7.BackgroundImage = global::MONKEY_APP.Properties.Resources._7h;
            this.fig7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fig7.Location = new System.Drawing.Point(212, 165);
            this.fig7.Name = "fig7";
            this.fig7.Size = new System.Drawing.Size(35, 35);
            this.fig7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fig7.TabIndex = 118;
            this.fig7.TabStop = false;
            this.fig7.Tag = "7";
            this.fig7.Click += new System.EventHandler(this.savefinger);
            this.fig7.MouseLeave += new System.EventHandler(this.figMouseLeave);
            this.fig7.MouseHover += new System.EventHandler(this.figMouseHover);
            // 
            // fig3
            // 
            this.fig3.BackColor = System.Drawing.Color.Transparent;
            this.fig3.BackgroundImage = global::MONKEY_APP.Properties.Resources._3h;
            this.fig3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fig3.Location = new System.Drawing.Point(403, 129);
            this.fig3.Name = "fig3";
            this.fig3.Size = new System.Drawing.Size(35, 35);
            this.fig3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fig3.TabIndex = 118;
            this.fig3.TabStop = false;
            this.fig3.Tag = "3";
            this.fig3.Click += new System.EventHandler(this.savefinger);
            this.fig3.MouseLeave += new System.EventHandler(this.figMouseLeave);
            this.fig3.MouseHover += new System.EventHandler(this.figMouseHover);
            // 
            // fig8
            // 
            this.fig8.BackColor = System.Drawing.Color.Transparent;
            this.fig8.BackgroundImage = global::MONKEY_APP.Properties.Resources._8h;
            this.fig8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fig8.Location = new System.Drawing.Point(173, 129);
            this.fig8.Name = "fig8";
            this.fig8.Size = new System.Drawing.Size(35, 35);
            this.fig8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fig8.TabIndex = 118;
            this.fig8.TabStop = false;
            this.fig8.Tag = "8";
            this.fig8.Click += new System.EventHandler(this.savefinger);
            this.fig8.MouseLeave += new System.EventHandler(this.figMouseLeave);
            this.fig8.MouseHover += new System.EventHandler(this.figMouseHover);
            // 
            // fig4
            // 
            this.fig4.BackColor = System.Drawing.Color.Transparent;
            this.fig4.BackgroundImage = global::MONKEY_APP.Properties.Resources._4h;
            this.fig4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fig4.Location = new System.Drawing.Point(441, 165);
            this.fig4.Name = "fig4";
            this.fig4.Size = new System.Drawing.Size(35, 35);
            this.fig4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fig4.TabIndex = 118;
            this.fig4.TabStop = false;
            this.fig4.Tag = "4";
            this.fig4.Click += new System.EventHandler(this.savefinger);
            this.fig4.MouseLeave += new System.EventHandler(this.figMouseLeave);
            this.fig4.MouseHover += new System.EventHandler(this.figMouseHover);
            // 
            // fig9
            // 
            this.fig9.BackColor = System.Drawing.Color.Transparent;
            this.fig9.BackgroundImage = global::MONKEY_APP.Properties.Resources._9h;
            this.fig9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fig9.Location = new System.Drawing.Point(135, 166);
            this.fig9.Name = "fig9";
            this.fig9.Size = new System.Drawing.Size(35, 35);
            this.fig9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fig9.TabIndex = 118;
            this.fig9.TabStop = false;
            this.fig9.Tag = "9";
            this.fig9.Click += new System.EventHandler(this.savefinger);
            this.fig9.MouseLeave += new System.EventHandler(this.figMouseLeave);
            this.fig9.MouseHover += new System.EventHandler(this.figMouseHover);
            // 
            // fig5
            // 
            this.fig5.BackColor = System.Drawing.Color.Transparent;
            this.fig5.BackgroundImage = global::MONKEY_APP.Properties.Resources._5h;
            this.fig5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fig5.Location = new System.Drawing.Point(480, 199);
            this.fig5.Name = "fig5";
            this.fig5.Size = new System.Drawing.Size(35, 35);
            this.fig5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fig5.TabIndex = 118;
            this.fig5.TabStop = false;
            this.fig5.Tag = "5";
            this.fig5.Click += new System.EventHandler(this.savefinger);
            this.fig5.MouseLeave += new System.EventHandler(this.figMouseLeave);
            this.fig5.MouseHover += new System.EventHandler(this.figMouseHover);
            // 
            // fig10
            // 
            this.fig10.BackColor = System.Drawing.Color.Transparent;
            this.fig10.BackgroundImage = global::MONKEY_APP.Properties.Resources._10h1;
            this.fig10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fig10.Location = new System.Drawing.Point(96, 199);
            this.fig10.Name = "fig10";
            this.fig10.Size = new System.Drawing.Size(35, 35);
            this.fig10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fig10.TabIndex = 118;
            this.fig10.TabStop = false;
            this.fig10.Tag = "10";
            this.fig10.Click += new System.EventHandler(this.savefinger);
            this.fig10.MouseLeave += new System.EventHandler(this.figMouseLeave);
            this.fig10.MouseHover += new System.EventHandler(this.figMouseHover);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::MONKEY_APP.Properties.Resources.hand;
            this.pictureBox2.Location = new System.Drawing.Point(8, 78);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(595, 442);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 117;
            this.pictureBox2.TabStop = false;
            // 
            // btnf5
            // 
            this.btnf5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.btnf5.FlatAppearance.BorderSize = 0;
            this.btnf5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnf5.Image = global::MONKEY_APP.Properties.Resources.error;
            this.btnf5.Location = new System.Drawing.Point(224, 2);
            this.btnf5.Name = "btnf5";
            this.btnf5.Size = new System.Drawing.Size(37, 30);
            this.btnf5.TabIndex = 141;
            this.btnf5.UseVisualStyleBackColor = false;
            this.btnf5.Visible = false;
            this.btnf5.Click += new System.EventHandler(this.delFng);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.btnf5);
            this.panel3.Controls.Add(this.labf5);
            this.panel3.Location = new System.Drawing.Point(815, 243);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(264, 34);
            this.panel3.TabIndex = 142;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.btnf4);
            this.panel4.Controls.Add(this.labf4);
            this.panel4.Location = new System.Drawing.Point(815, 201);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(264, 34);
            this.panel4.TabIndex = 142;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("TH SarabunPSK", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.label13.Location = new System.Drawing.Point(3, 1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(23, 30);
            this.label13.TabIndex = 140;
            this.label13.Text = "4";
            // 
            // btnf4
            // 
            this.btnf4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.btnf4.FlatAppearance.BorderSize = 0;
            this.btnf4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnf4.Image = global::MONKEY_APP.Properties.Resources.error;
            this.btnf4.Location = new System.Drawing.Point(224, 2);
            this.btnf4.Name = "btnf4";
            this.btnf4.Size = new System.Drawing.Size(37, 30);
            this.btnf4.TabIndex = 141;
            this.btnf4.UseVisualStyleBackColor = false;
            this.btnf4.Visible = false;
            this.btnf4.Click += new System.EventHandler(this.delFng);
            // 
            // labf4
            // 
            this.labf4.BackColor = System.Drawing.Color.GreenYellow;
            this.labf4.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labf4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.labf4.Location = new System.Drawing.Point(29, 2);
            this.labf4.Name = "labf4";
            this.labf4.Size = new System.Drawing.Size(195, 30);
            this.labf4.TabIndex = 140;
            this.labf4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labf4.Visible = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.btnf3);
            this.panel5.Controls.Add(this.labf3);
            this.panel5.Location = new System.Drawing.Point(815, 160);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(264, 34);
            this.panel5.TabIndex = 142;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("TH SarabunPSK", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.label15.Location = new System.Drawing.Point(3, 1);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(23, 30);
            this.label15.TabIndex = 140;
            this.label15.Text = "3";
            // 
            // btnf3
            // 
            this.btnf3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.btnf3.FlatAppearance.BorderSize = 0;
            this.btnf3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnf3.Image = global::MONKEY_APP.Properties.Resources.error;
            this.btnf3.Location = new System.Drawing.Point(224, 2);
            this.btnf3.Name = "btnf3";
            this.btnf3.Size = new System.Drawing.Size(37, 30);
            this.btnf3.TabIndex = 141;
            this.btnf3.UseVisualStyleBackColor = false;
            this.btnf3.Visible = false;
            this.btnf3.Click += new System.EventHandler(this.delFng);
            // 
            // labf3
            // 
            this.labf3.BackColor = System.Drawing.Color.GreenYellow;
            this.labf3.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labf3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.labf3.Location = new System.Drawing.Point(29, 2);
            this.labf3.Name = "labf3";
            this.labf3.Size = new System.Drawing.Size(195, 30);
            this.labf3.TabIndex = 140;
            this.labf3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labf3.Visible = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.btnf2);
            this.panel6.Controls.Add(this.labf2);
            this.panel6.Location = new System.Drawing.Point(815, 119);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(264, 34);
            this.panel6.TabIndex = 142;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("TH SarabunPSK", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.label18.Location = new System.Drawing.Point(3, 1);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 30);
            this.label18.TabIndex = 140;
            this.label18.Text = "2";
            // 
            // btnf2
            // 
            this.btnf2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.btnf2.FlatAppearance.BorderSize = 0;
            this.btnf2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnf2.Image = global::MONKEY_APP.Properties.Resources.error;
            this.btnf2.Location = new System.Drawing.Point(224, 3);
            this.btnf2.Name = "btnf2";
            this.btnf2.Size = new System.Drawing.Size(37, 30);
            this.btnf2.TabIndex = 141;
            this.btnf2.UseVisualStyleBackColor = false;
            this.btnf2.Visible = false;
            this.btnf2.Click += new System.EventHandler(this.delFng);
            // 
            // labf2
            // 
            this.labf2.BackColor = System.Drawing.Color.GreenYellow;
            this.labf2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labf2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.labf2.Location = new System.Drawing.Point(29, 2);
            this.labf2.Name = "labf2";
            this.labf2.Size = new System.Drawing.Size(195, 30);
            this.labf2.TabIndex = 140;
            this.labf2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labf2.Visible = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.Controls.Add(this.label20);
            this.panel7.Controls.Add(this.btnf1);
            this.panel7.Controls.Add(this.labf1);
            this.panel7.Location = new System.Drawing.Point(815, 78);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(264, 34);
            this.panel7.TabIndex = 142;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("TH SarabunPSK", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.label20.Location = new System.Drawing.Point(3, 1);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(23, 30);
            this.label20.TabIndex = 140;
            this.label20.Text = "1";
            // 
            // btnf1
            // 
            this.btnf1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.btnf1.FlatAppearance.BorderSize = 0;
            this.btnf1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnf1.Image = global::MONKEY_APP.Properties.Resources.error;
            this.btnf1.Location = new System.Drawing.Point(224, 2);
            this.btnf1.Name = "btnf1";
            this.btnf1.Size = new System.Drawing.Size(37, 30);
            this.btnf1.TabIndex = 141;
            this.btnf1.UseVisualStyleBackColor = false;
            this.btnf1.Visible = false;
            this.btnf1.Click += new System.EventHandler(this.delFng);
            // 
            // labf1
            // 
            this.labf1.BackColor = System.Drawing.Color.GreenYellow;
            this.labf1.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labf1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.labf1.Location = new System.Drawing.Point(29, 2);
            this.labf1.Name = "labf1";
            this.labf1.Size = new System.Drawing.Size(195, 30);
            this.labf1.TabIndex = 140;
            this.labf1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labf1.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.label23.Location = new System.Drawing.Point(617, 367);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(156, 22);
            this.label23.TabIndex = 140;
            this.label23.Text = "1. ตรวจสอบว่า ถูกคนหรือไม่";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.label24.Location = new System.Drawing.Point(617, 422);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(359, 22);
            this.label24.TabIndex = 140;
            this.label24.Text = "3. เก็บข้อมูล 1 คน อย่างน้อย 2 ลายนิ้วมือ (1 นิ้ว แตะแล้วยก 3 ครั้ง)";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.label25.Location = new System.Drawing.Point(617, 395);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(179, 22);
            this.label25.TabIndex = 140;
            this.label25.Text = "2. เลือกเลขนิ้วมือที่ต้องการบันทึก";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(941, 336);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 35);
            this.button1.TabIndex = 141;
            this.button1.Text = "ยกเลิกเก็บลายนิ้วมือ";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frm_fg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1117, 552);
            this.ControlBox = false;
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.cmdBeep);
            this.Controls.Add(this.cmdGreen);
            this.Controls.Add(this.cmdRED);
            this.Controls.Add(this.cmdEnroll);
            this.Controls.Add(this.cmdVerify);
            this.Controls.Add(this.cmdIdentify);
            this.Controls.Add(this.TextFingerName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.ZKFPEngX1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.fig2);
            this.Controls.Add(this.fig1);
            this.Controls.Add(this.fig6);
            this.Controls.Add(this.fig7);
            this.Controls.Add(this.fig3);
            this.Controls.Add(this.fig8);
            this.Controls.Add(this.fig4);
            this.Controls.Add(this.fig9);
            this.Controls.Add(this.fig5);
            this.Controls.Add(this.fig10);
            this.Controls.Add(this.labCountFng);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_fg";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frm_fg_cus_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ZKFPEngX1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fig10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labCountFng;
        private System.Windows.Forms.PictureBox fig10;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox4;
        public AxZKFPEngXControl.AxZKFPEngX ZKFPEngX1;
        internal System.Windows.Forms.PictureBox pictureBox5;
        public System.Windows.Forms.TextBox TextSensorCount;
        public System.Windows.Forms.TextBox TextSensorIndex;
        public System.Windows.Forms.TextBox TextSensorSN;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Button cmdBeep;
        internal System.Windows.Forms.Button cmdGreen;
        internal System.Windows.Forms.Button cmdRED;
        public System.Windows.Forms.Button cmdEnroll;
        public System.Windows.Forms.Button cmdVerify;
        public System.Windows.Forms.Button cmdIdentify;
        public System.Windows.Forms.TextBox TextFingerName;
        public System.Windows.Forms.Button cmdInit;
        public System.Windows.Forms.GroupBox Frame2;
        public System.Windows.Forms.RadioButton OptionBmp;
        public System.Windows.Forms.RadioButton OptionJpg;
        public System.Windows.Forms.Button cmdSaveImage;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label StatusBar;
        private System.Windows.Forms.PictureBox fig9;
        private System.Windows.Forms.PictureBox fig8;
        private System.Windows.Forms.PictureBox fig7;
        private System.Windows.Forms.PictureBox fig2;
        private System.Windows.Forms.PictureBox fig4;
        private System.Windows.Forms.PictureBox fig5;
        private System.Windows.Forms.PictureBox fig3;
        private System.Windows.Forms.PictureBox fig6;
        private System.Windows.Forms.PictureBox fig1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labf5;
        private System.Windows.Forms.Button btnf5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnf4;
        private System.Windows.Forms.Label labf4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnf3;
        private System.Windows.Forms.Label labf3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnf2;
        private System.Windows.Forms.Label labf2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnf1;
        private System.Windows.Forms.Label labf1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Button button1;
    }
}