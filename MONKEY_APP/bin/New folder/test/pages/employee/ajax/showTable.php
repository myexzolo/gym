<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$companycode  = $_SESSION['branchCode'];
?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td>Num</td>
      <td>Code</td>
      <td>NickName</td>
      <td>Name</td>
      <td>วันเดือนปีเกิด</td>
      <td>ตำแหน่ง</td>
      <td>สังกัด/แผนก</td>
      <td>วันที่เริ่มงาน</td>
      <td>สิทธิการใช้งาน</td>
      <td>Edit/Del</td>
    </tr>
  </thead>
  <tbody>
    <?php
    $sql = "SELECT * FROM data_mas_employee where emp_code != '0000' and COMPANY_CODE ='$companycode'";


    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $errorInfo  = $json['errorInfo'];
    $rows       = $json['data'];
    $dataCount  = $json['dataCount'];

    for($i=0 ; $i < $dataCount ; $i++) {

      $emp_user = $rows[$i]['EMP_USER'];

      $sqlu         = "SELECT * FROM t_user where user_login = '$emp_user' and  branch_code ='$companycode'";
      $queryu       = DbQuery($sqlu,null);
      $jsonu        = json_decode($queryu, true);
      $dataCountu   = $jsonu['dataCount'];
      $rowu         = $jsonu['data'];
      //echo $sqlu;
      $roleName     = "-";
      if($dataCountu > 0){
        $roleName   = getRoleName($rowu[0]['role_list']);
      }

      $empName = $rows[$i]['EMP_TITLE'].$rows[$i]['EMP_NAME']." ".$rows[$i]['EMP_LASTNAME'];

    ?>
    <tr class="text-center">
      <td><?=$i + 1;?></td>
      <td><?=$rows[$i]['EMP_CODE'];?></td>
      <td align="left"><?=$rows[$i]['EMP_NICKNAME'];?></td>
      <td align="left"><?= $empName?></td>
      <td align="left"><?= formatDateTh($rows[$i]['EMP_BIRTH_DATE']);?></td>
      <td align="left"><?= getDataMaster('EMP_POSITION',$rows[$i]['EMP_POSITION']);?></td>
      <td align="left"><?= getDataMaster('EMP_DEPARTMENT',$rows[$i]['EMP_DEPARTMENT']);?></td>
      <td><?= formatDateTh($rows[$i]['EMP_DATE_INCOME']);?></td>
      <td align="center"><?= $roleName ?></td>
      <td>
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm('EDIT','<?=$rows[$i]['EMP_CODE']?>')">Edit</button>
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="del('<?=$rows[$i]['EMP_CODE']?>','<?=$empName?>')">Del</button>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable();
  })
</script>
