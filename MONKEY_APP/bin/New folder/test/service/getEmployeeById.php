<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companycode = isset($_GET['companycode'])?$_GET['companycode']:"GYMMK01";
$empcode = isset($_GET['empcode'])?$_GET['empcode']:"";


$dateNow = date("Y-m-d");

$sql = "SELECT e.*, u.user_img FROM data_mas_employee  e LEFT JOIN t_user u
ON e.EMP_CODE = u.user_login
where e.COMPANY_CODE ='$companycode' and e.EMP_CODE = '$empcode' order by e.EMP_CODE";

//echo $sql;

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row[0]));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array()));
}

?>
