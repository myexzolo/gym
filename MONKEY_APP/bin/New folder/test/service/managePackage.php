<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$typeActive = isset($_GET['typeActive'])?$_GET['typeActive']:"";

$package_id       = isset($_GET['package_id'])?$_GET['package_id']:"";
$package_code     = isset($_GET['package_code'])?$_GET['package_code']:"";
$package_name     = isset($_GET['package_name'])?$_GET['package_name']:"";
$package_detail   = isset($_GET['package_detail'])?$_GET['package_detail']:"";
$package_type     = isset($_GET['package_type'])?$_GET['package_type']:"";
$num_use          = isset($_GET['num_use'])?$_GET['num_use']:"";
$package_unit     = isset($_GET['package_unit'])?$_GET['package_unit']:"";
$max_use          = isset($_GET['max_use'])?$_GET['max_use']:"";
$package_price    = isset($_GET['package_price'])?$_GET['package_price']:"";
$type_vat         = isset($_GET['type_vat'])?$_GET['type_vat']:"";
$package_status   = isset($_GET['package_status'])?$_GET['package_status']:"";
$notify_num       = isset($_GET['notify_num'])?$_GET['notify_num']:"";
$notify_unit      = isset($_GET['notify_unit'])?$_GET['notify_unit']:"";
$seq              = isset($_GET['seq'])?$_GET['seq']:"";


$user_id_update   = isset($_GET['user_id_update'])?$_GET['user_id_update']:"";


// --ADD EDIT DELETE Module--

if($typeActive == "ADD"){
  $sql   = "INSERT INTO data_mas_package (
            package_code,
            package_name,package_detail,
            package_type,num_use,
            package_unit,max_use,
            package_price,type_vat,
            package_status,notify_num,notify_unit,
            create_by,create_date,seq)
           VALUES(
             '$package_code',
             '$package_name','$package_detail',
             '$package_type','$num_use',
             '$package_unit','$max_use',
             '$package_price','$type_vat',
             '$package_status','$notify_num','$notify_unit',
             '$user_id_update',NOW(),'$seq'
           )";
}else if($typeActive == "EDIT"){
  $sql = "UPDATE data_mas_package SET
          package_name    = '$package_name',
          package_detail  = '$package_detail',
          package_type    = '$package_type',
          num_use         = '$num_use',
          package_unit    = '$package_unit',
          max_use         = '$max_use',
          package_price   = '$package_price',
          type_vat        = '$type_vat',
          package_status  = '$package_status',
          package_status  = '$package_status',
          notify_num      = '$notify_num',
          notify_unit     = '$notify_unit',
          seq             = '$seq',
          update_date     = NOW()
          WHERE package_id = '$package_id'";
}
else if($typeActive == "DEL")
{
  $sql = "UPDATE data_mas_package SET
          package_status  = 'D',
          update_by       = '$user_id_update',
          update_date     = NOW()
          WHERE package_id = '$package_id'";
}
//echo $sql."<br>";
//--ADD EDIT Package-- //
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => true,'message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail :'.$sql."<br>".$sql2)));
}

?>
