<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companycode = isset($_GET['companycode'])?$_GET['companycode']:"GYMMK01";


$sql = "SELECT max(EMP_CODE) as EMP_CODE FROM data_mas_employee where COMPANY_CODE ='$companycode' ";
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];;

if($dataCount > 0){
  $lastNum = $row[0]['EMP_CODE'] + 1;
  $EMP_CODE = sprintf("%04d", $lastNum);
}else{
  $EMP_CODE = 990001;
}

header('Content-Type: application/json');
exit(json_encode(array('status' => true,'message' => $EMP_CODE)));
?>
