<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$package_type = isset($_GET['package_type'])?$_GET['package_type']:"";


$sql = "SELECT max(package_code) as package_code FROM data_mas_package where package_type like '$package_type%' ";
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];;

if($dataCount > 0){
  $Code     = $row[0]['package_code'];
  $lastNum  = substr($Code,strlen($package_type));
  $lastNum  = $lastNum + 1;
  $packageCode = $package_type.sprintf("%04d", $lastNum);  
}else{
  $packageCode = $package_type."0001";
}

header('Content-Type: application/json');
exit(json_encode(array('status' => true,'message' => $packageCode)));
?>
