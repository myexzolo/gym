<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companycode  = isset($_GET['companycode'])?$_GET['companycode']:"GYMMK01";
$companyId    = isset($_GET['companyId'])?$_GET['companyId']:"";
$typePackage  = isset($_GET['typePackage'])?$_GET['typePackage']:"";


$companyId = sprintf("%02d", $companyId);
$code = $typePackage.$companyId.date("ymd");

$sql = "SELECT max(reg_no) as reg_no FROM trans_package_person where company_code ='$companycode' and reg_no like '$code%' ";
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];;

$REGNO = "";
if($dataCount > 0){
  $regNo    = $row[0]['reg_no'];
  $lastNum  = substr($regNo,strlen($code));
  $lastNum  = $lastNum + 1;
  $REGNO = $code.sprintf("%03d", $lastNum);
}else{
  $REGNO = $code."001";
}

header('Content-Type: application/json');
exit(json_encode(array('status' => true,'message' => $REGNO)));
?>
