<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companycode = isset($_GET['companycode'])?$_GET['companycode']:"GYMMK01";
$typeSearch = isset($_GET['typeSearch'])?$_GET['typeSearch']:"";

$con = "";

$dateNow = date("Y-m-d");


if($typeSearch == "TODAY"){
  $con .= " and DATE_FORMAT(PERSON_REGISTER_DATE,'%Y-%m-%d') = '$dateNow'";
}else if($typeSearch == "ACTIVE"){
  $con .= " and PERSON_STATUS in ('Y','A')";
}else if($typeSearch == "MEMBER"){
  $con .= " and PERSON_STATUS not in ('A')";
}else if($typeSearch == "EXPIRE"){
  $con .= " and PERSON_STATUS not in ('Y','A')";
}

$sql = "SELECT COMPANY_CODE, PERSON_RUNNO, PERSON_CODE_INT,
        PERSON_CODE, PERSON_CODE_OLD, PERSON_TITLE,
        PERSON_NICKNAME, PERSON_NAME, PERSON_LASTNAME,
        PERSON_NAME_MIDDLE, PERSON_TITLE_ENG, PERSON_NAME_ENG,
        PERSON_LASTNAME_ENG, PERSON_SEX, PERSON_BIRTH_DATE,
        PERSON_GRADE, PERSON_PROVINCE_CODE, PERSON_DOOR_CARDNO,
        PERSON_DOOR_PIN, PERSON_DOOR_PASSWORD, PERSON_DOOR_GROUP,
        PERSON_CARD_ID, PERSON_CARD_SDATE,
        PERSON_CARD_EDATE,
        PERSON_CARD_CREATE_BY,
        PERSON_TEL_MOBILE, PERSON_TEL_MOBILE2, PERSON_TEL_OFFICE,
        PERSON_FAX, PERSON_IMAGE, PERSON_IMAGE_ROTATE,
        PERSON_GROUP_AGE, PERSON_PRIORITY, PERSON_GROUP,
        PERSON_STATUS, PERSON_REGISTER_DATE, PERSON_EXPIRE_DATE,
        PERSON_NOTE, PERSON_LAST_VISIT, PERSON_MUST_AT,
        PERSON_ADDRESS_ZONE, PERSON_HOME1_ADDR1, PERSON_HOME1_SUBDISTRICT,
        PERSON_HOME1_DISTRICT, PERSON_HOME1_PROVINCE, PERSON_HOME1_COUNTRY,
        PERSON_HOME1_POSTAL, PERSON_HOME1_ADDR2, PERSON_HOME2_SUBDISTRICT,
        PERSON_HOME2_DISTRICT, PERSON_HOME2_PROVINCE, PERSON_HOME2_COUNTRY,
        PERSON_HOME2_POSTAL, PERSON_BILL_TYPE, PERSON_BILL_NAME,
        PERSON_BILL_ADDR1, PERSON_BILL_ADDR2, PERSON_BILL_SUBDISTRICT,
        PERSON_BILL_DISTRICT, PERSON_BILL_PROVINCE, PERSON_BILL_POSTAL,
        PERSON_BILL_COUNTRY, PERSON_BILL_TAXNO, PERSON_ACC_NO,
        RECV_NEWS_VIA, PERSON_MAX_ORDER, PERSON_PRICE,
        SEND_NEWS_TO, PERSON_SALE_CODE, FNG_TEMPLATE1,
        FNG_TEMPLATE2, FNG_TEMPLATE3, FNG_TEMPLATE4, FNG_TEMPLATE5,
        DATA_SHIFT_DATE, DATA_SHIFT_CODE, DATA_STATUS,
        DATA_CREATE_DATE, DATA_CREATE_BY, DATA_CREATE_SHIFT,
        DATA_MODIFY_DATE, DATA_MODIFY_BY, DATA_CANCLE_DATE,
        DATA_CANCEL_BY, DATA_CANCEL_NOTE, DATA_DELETE_DATE,
        DATA_DELETE_BY, DATA_RECORD_RUNNO, PERSON_KEYWORDS
        FROM person where COMPANY_CODE ='$companycode' $con ORDER BY PERSON_LAST_VISIT DESC";

//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array()));
}

?>
