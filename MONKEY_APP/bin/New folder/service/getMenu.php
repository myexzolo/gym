<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$role_list = isset($_GET['role_list'])?$_GET['role_list']:"";

$sql = "SELECT * FROM t_role where role_id in ($role_list) and is_active = 'Y'";
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'];
$dataCount  = $json['dataCount'];

$strPageList = '';
foreach ($row as $k => $value) {
  if($k == 0){
    $strPageList .= $value['page_list'];
  }else{
    $strPageList .= ','.$value['page_list'];
  }
}
if($strPageList != ""){
  $sql = "SELECT * FROM t_page where page_id in ($strPageList) and is_active = 'Y' order by page_seq";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $row        = $json['data'];
  $dataCount  = $json['dataCount'];

  if(intval($errorInfo[0]) == 0 && $dataCount > 0){
    header('Content-Type: application/json');
    exit(json_encode($row));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => false,'message' => 'Fail')));
  }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail')));
}



?>
