<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companycode    = isset($_GET['companycode'])?$_GET['companycode']:"";
$startDate      = isset($_GET['startDate'])?$_GET['startDate']:"";
$endDate        = isset($_GET['endDate'])?$_GET['endDate']:"";


$sql = "SELECT * FROM trans_checkin_person cp, data_mas_employee e
where cp.COMPANY_CODE ='$companycode' and e.COMPANY_CODE ='$companycode'
AND cp.trainer_code = e.EMP_CODE AND sign_person = 'Y' AND sign_emp = 'Y' AND sign_manager = 'Y'
AND cp.sign_manager_date BETWEEN '$startDate 00:00:01' and '$endDate 23:59:59'
AND e.EMP_IS_TRAINER = 'Y' and e.DATA_DELETE_STATUS = 'N' order by e.EMP_CODE ,cp.sign_manager_date";

//echo $sql;

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array()));
}

?>
