<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');


$sql = "SELECT * FROM data_mas_add_subdistrict order by PROVINCE_CODE, DISTRICT_CODE, SUBDISTRICT_CODE";


$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array()));
}

?>
