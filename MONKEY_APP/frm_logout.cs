﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class frm_logout : Form
    {
        public frm_logout()
        {
            InitializeComponent();
            int x = (Screen.PrimaryScreen.Bounds.Width - 610);
            this.Location = new System.Drawing.Point(x, 80);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            
        }

        private void frm_logout_Load(object sender, EventArgs e)
        {
            if (!Global.USER.user_id.Equals(""))
            {
                labNameUser.Text = Global.USER.user_name + " " + Global.USER.user_last;
                labDateLog.Text = Global.USER.date_login;
                labDepName.Text = Global.USER.department_name;
                labPos.Text = Global.USER.emp_position;
                labRoleName.Text = Global.USER.role_name;
            }
            if (!Global.USER.user_img.Equals(""))
            {
                Utils.setImage(Global.USER.user_img, picUser);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
