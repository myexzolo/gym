﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class frm_emp : Form
    {

        public frm_emp()
        {
            InitializeComponent();
        }

        string active = "";

        private DataTable dt;
        Employee employee = new Employee();
        //private DataView dv;

        private void button7_Click(object sender, EventArgs e)
        {
            DataView dv = dt.DefaultView;
            dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);
            dataGridView1.DataSource = dv.ToTable();
        }


        private void frm_customer_Load(object sender, EventArgs e)
        {
            int scrWidth = Screen.PrimaryScreen.Bounds.Width;
            int scrHeight =  Screen.PrimaryScreen.Bounds.Height;

            int panelw1 = scrWidth - 265;

            int panelM  = panelw1 / 100 * 95;

            panelMemberList.Width = panelw1;
            panelMemberList.Height = scrHeight - 265;


            int num = 0;
            tableLayoutPanel.ColumnCount = (num + 1);

            List<Button> buttons = new List<Button>();

            for (var i = 0; i < Global.MENULIST.Count; i++)
            {
                string btnName = "";
                int width = 125;
                string menuName = Global.MENULIST[i].page_name;
                string code = Global.MENULIST[i].page_code;
                string moduleId = Global.MENULIST[i].module_id;
                Image img = global::MONKEY_APP.Properties.Resources.folder2;

                if (moduleId.Equals("5"))
                {
                    if (code.Equals("AEMP"))
                    {
                        width = 140;
                        img = global::MONKEY_APP.Properties.Resources.folder__2_;
                    }
                    else if (code.Equals("EEMP"))
                    {
                        width = 150;
                        img = global::MONKEY_APP.Properties.Resources.folder2;
                    }
                    else if (code.Equals("DEMP"))
                    {
                        width = 140;
                        img = global::MONKEY_APP.Properties.Resources.del;
                    }
                    else if (code.Equals("AEFIN"))
                    {
                        width = 150;
                        img = global::MONKEY_APP.Properties.Resources.fingerprints2;
                    }
                    else if (code.Equals("TEFIN"))
                    {
                        width = 180;
                        img = global::MONKEY_APP.Properties.Resources.fingerprint3;
                    }

                    btnName = code;
                    buttons.Add(genButtonMenu(btnName, width, menuName, img));
                    num++;
                }

            }

            Button button = genButtonMenu("VMEM", 157, "ดูข้อมูลทั้งหมด", global::MONKEY_APP.Properties.Resources.eye2);

            buttons.Add(button);
            tableLayoutPanel.ColumnCount = buttons.Count;

            for (var i = 0; i < buttons.Count; i++)
            {
                Button btn = buttons[i];
                tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
                tableLayoutPanel.Controls.Add(btn, i, 0);
            }

            txtSearch.Focus();
            getEmps("ACTIVE");
        }

        private async void getEmps(string typeSearch)
        {

            try
            {
                if (active != null && (active.Equals("ADD") || active.Equals("EDIT") || active.Equals("ADDF") || active.Equals("REF")))
                {
                    var thread1 = new Thread(showloading);
                    thread1.Start();
                    ApiRest.InitailizeClient();
                    Global.EmployeeList = await ApiProcess.getEmployee(typeSearch);
                }

                if (Global.EmployeeList == null) {
                    var thread1 = new Thread(showloading);
                    thread1.Start();
                    ApiRest.InitailizeClient();
                    Global.EmployeeList = await ApiProcess.getEmployee(typeSearch);
                }


                dt = new DataTable();

                dt.Columns.Add("ลายนิ้วมือ", typeof(bool));
                dt.Columns.Add("รหัส", typeof(string));
                dt.Columns.Add("ชื่อเล่น", typeof(string));
                dt.Columns.Add("คำนำหน้า", typeof(string));
                dt.Columns.Add("ชื่อ", typeof(string));
                dt.Columns.Add("นามสกุล", typeof(string));
                dt.Columns.Add("วันเกิด", typeof(string));
                dt.Columns.Add("เบอร์โทร", typeof(string));
                dt.Columns.Add("ตำแหน่ง", typeof(string));
                dt.Columns.Add("แผนก", typeof(string));
                dt.Columns.Add("วันที่เริ่มงาน", typeof(string));
                dt.Columns.Add("รูป", typeof(string));

                for (int i = 0; i < Global.EmployeeList.Count(); i++)
                {
                    Employee emp = Global.EmployeeList[i];

                    bool fgn = false;

                    if (emp.FNG_TEMPLATE1 != null && !emp.FNG_TEMPLATE1.Equals(""))
                    {
                        fgn = true;
                    }
                    else if (emp.FNG_TEMPLATE2 != null && !emp.FNG_TEMPLATE2.Equals(""))
                    {
                        fgn = true;
                    }
                    else if (emp.FNG_TEMPLATE3 != null && !emp.FNG_TEMPLATE3.Equals(""))
                    {
                        fgn = true;
                    }
                    else if (emp.FNG_TEMPLATE4 != null && !emp.FNG_TEMPLATE4.Equals(""))
                    {
                        fgn = true;
                    }
                    else if (emp.FNG_TEMPLATE5 != null && !emp.FNG_TEMPLATE5.Equals(""))
                    {
                        fgn = true;
                    }

                    string masPos = "";
                    string masDep = "";

                    for (int x = 0; x < Global.EMP_POSITION.Count; x++)
                    {
                        if (Global.EMP_POSITION[x].DATA_GROUP == "EMP_POSITION" && Global.EMP_POSITION[x].DATA_CODE == emp.EMP_POSITION)
                        {
                            masPos = Global.EMP_POSITION[x].DATA_DESC1;
                            break;
                        }
                    }

                    for (int y = 0; y < Global.EMP_DEPARTMENT.Count; y++)
                    {
                        if (Global.EMP_DEPARTMENT[y].DATA_GROUP == "EMP_DEPARTMENT" && Global.EMP_DEPARTMENT[y].DATA_CODE == emp.EMP_DEPARTMENT)
                        {
                            masDep = Global.EMP_DEPARTMENT[y].DATA_DESC1;
                            break;
                        }
                    }

                    if (emp.user_img == null)
                    {
                        if (emp.EMP_PICTURE != null && !emp.EMP_PICTURE.Equals(""))
                        {
                            emp.user_img = emp.EMP_PICTURE;
                        }
                        else
                        {
                            emp.user_img = "";
                        }
                        
                    }

                    dt.Rows.Add(fgn,
                        emp.EMP_CODE, emp.EMP_NICKNAME, emp.EMP_TITLE,
                        emp.EMP_NAME, emp.EMP_LASTNAME, emp.EMP_BIRTH_DATE,
                        emp.EMP_TEL, masPos, masDep, emp.EMP_DATE_INCOME, emp.user_img
                    );
                }

                dataGridView1.DataSource = dt;

                dataGridView1.Columns[0].Width = 100;
                dataGridView1.Columns[1].Width = 100;
                dataGridView1.Columns[2].Width = 100;
                dataGridView1.Columns[3].Width = 120;
                dataGridView1.Columns[4].Width = 200;
                dataGridView1.Columns[5].Width = 220;
                dataGridView1.Columns[6].Width = 120;
                dataGridView1.Columns[7].Width = 120;
                dataGridView1.Columns[8].Width = 200;
                dataGridView1.Columns[9].Width = 200;
                dataGridView1.Columns[10].Width = 120;
                dataGridView1.Columns[11].Visible = false;

                dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[9].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[10].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally {
                picLoading.Visible = false;
            }
            
        }


        private DataGridViewTextBoxColumn setColumnTextBox(string HeaderText, string Name, int Width) {
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();

            column.HeaderText = HeaderText;
            column.Name = Name;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            column.Width = Width;

            return column;
        }

        private Button genButtonMenu(string btnName, int width, string menuName, Image img)
        {
            Button btn = new Button();
            btn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            btn.FlatAppearance.BorderSize = 0;
            btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btn.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            btn.Image = img;
            btn.Name = btnName;
            btn.Tag = btnName;
            btn.Size = new System.Drawing.Size(width, 40);
            btn.Text = menuName;
            btn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btn.UseVisualStyleBackColor = false;
            btn.Click += new System.EventHandler(this.buttonClick);

            return btn;
        }

        private void buttonClick(object sender, EventArgs e)
        {
            string code = ((Button)sender).Tag.ToString();
            

            if (code.Equals("AEMP"))
            {
                frm_manage_emp fmc = new frm_manage_emp("ADD", employee);
                var result = fmc.ShowDialog();
                if (result == DialogResult.OK) {
                    active = "ADD";
                    getEmps("ACTIVE");
                }
            }
            else if (code.Equals("EEMP"))
            {
                frm_manage_emp fmc = new frm_manage_emp("EDIT", employee);
                var result = fmc.ShowDialog();
                if (result == DialogResult.OK)
                {
                    active = "EDIT";
                    getEmps("ACTIVE");
                }
            }
            else if (code.Equals("DEMP"))
            {

                frm_dialog d = new frm_dialog("ยืนยันการลบพนักงาน ! \r\n" + employee.EMP_CODE+":" + employee.EMP_NAME + " " + employee.EMP_LASTNAME, "");
                d.ShowDialog();
                if (d.DialogResult == DialogResult.OK)
                {
                    var thread1 = new Thread(showloading);
                    thread1.Start();
                    delData();
                }
            }
            else if (code.Equals("AEFIN"))
            {
                frm_fg fgc = new frm_fg(employee.EMP_CODE, employee.EMP_NAME + " " + employee.EMP_LASTNAME, "EMP");
                var result = fgc.ShowDialog();
                if (result == DialogResult.OK)
                {
                    active = "ADDF";
                    getEmps("ACTIVE");
                }
            }
            else if (code.Equals("TEFIN"))
            {
                frm_fg_test fgc = new frm_fg_test(employee.EMP_CODE, employee.EMP_NAME + " " + employee.EMP_LASTNAME, "EMP");
                var result = fgc.ShowDialog();
            }
            else if (code.Equals("VEMEM"))
            {
                ;
            }
        }

        private void showloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = true;
                }));
            }
            else
            {
                picLoading.Visible = true;
            }

        }

        private async void delData()
        {
            try
            {
                ApiRest.InitailizeClient();
                Response res = await ApiProcess.manageEmployee(Global.BRANCH.branch_code, "DEL", employee, Global.USER.user_login);
                if (res.status)
                {
                    active = "REF";
                    getEmps("ACTIVE");
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: delData ::" + ex.ToString(), "frm_emp");
            }
            finally
            {
                picLoading.Visible = false;
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private async void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("xxx");
            if (dataGridView1.CurrentCell != null)
            {
                int rowindex = dataGridView1.CurrentCell.RowIndex;

                employee.EMP_CODE = dataGridView1.Rows[rowindex].Cells[1].Value.ToString();
                employee.EMP_NICKNAME = dataGridView1.Rows[rowindex].Cells[2].Value.ToString();
                employee.EMP_NAME = dataGridView1.Rows[rowindex].Cells[4].Value.ToString();
                employee.EMP_LASTNAME = dataGridView1.Rows[rowindex].Cells[5].Value.ToString();

                labCode.Text = employee.EMP_CODE;
                labName.Text = employee.EMP_NAME;
                labLname.Text = employee.EMP_LASTNAME;

                if (!dataGridView1.Rows[rowindex].Cells[11].Value.ToString().Equals(""))
                {
                    Utils.setImage(dataGridView1.Rows[rowindex].Cells[11].Value.ToString(), picUser);
                }
                else
                {
                    picUser.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                    picUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
                    picUser.Size = new System.Drawing.Size(148, 178);
                }

                Global.FingerScanList = await ApiProcess.getFingerScan(employee.EMP_CODE, "EMP", Global.BRANCH.branch_code);
            }
        }
        

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            DataView dv = dt.DefaultView;
            dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);
            dataGridView1.DataSource = dv.ToTable();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var thread1 = new Thread(showloading);
            thread1.Start();
            active = "REF";
            getEmps("ACTIVE");
        }
    }
}
