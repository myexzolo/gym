﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class dialogRpt : Form
    {
        public string reportName = "";
        public string dateStart = "";
        public string dateEnd = "";

        public dialogRpt()
        {
            InitializeComponent();
        }

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void dialogRpt_Load(object sender, EventArgs e)
        {
            dateStart = "";
            dateEnd = "";

            for (int x = 1; x <= 31; x++)
            {
                comboDate.Items.Add(new Item(x.ToString("00"), x.ToString("00")));
                comboDate2.Items.Add(new Item(x.ToString("00"), x.ToString("00")));
            }

            for (int monthNo = 1; monthNo <= 12; monthNo++)
            {
                var bar = new DateTime(DateTime.Now.Year, monthNo, 1);
                comboMonth.Items.Add(new Item(bar.ToString("MMM", new CultureInfo("th-TH")), monthNo.ToString("00")));
                comboMonth2.Items.Add(new Item(bar.ToString("MMM", new CultureInfo("th-TH")), monthNo.ToString("00")));
            }

            int ey = int.Parse(DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"))) - 5;
            int sy = (ey - 90);
            for (int x = sy; x < ey; x++)
            {
                int yearEn = (x - 543);
                comboYear.Items.Add(new Item(x.ToString(), yearEn.ToString("0000")));
                comboYear2.Items.Add(new Item(x.ToString(), yearEn.ToString("0000")));
            }

            DateTime dob = DateTime.Now;

            comboDate.Text = "01";
            comboMonth.Text = dob.ToString("MMM", new CultureInfo("th-TH"));
            comboYear.Text = dob.ToString("yyyy", new CultureInfo("th-TH"));

            comboDate2.Text = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month).ToString();
            comboMonth2.Text = dob.ToString("MMM", new CultureInfo("th-TH"));
            comboYear2.Text = dob.ToString("yyyy", new CultureInfo("th-TH"));

            label1.Text = reportName;
        }

        private void datePicker_ValueChanged(object sender, EventArgs e)
        {
            DateTime dN = Convert.ToDateTime(datePicker.Value.ToString());
            comboDate.Text = dN.ToString("dd");
            comboMonth.Text = dN.ToString("MMM", new CultureInfo("th-TH"));
            int yy = int.Parse(dN.ToString("yyyy", new CultureInfo("th-TH")));

            if (yy > 3000)
            {
                yy -= 543;
            }

            comboYear.Text = yy.ToString("0000");
        }

        private void datePicker2_ValueChanged(object sender, EventArgs e)
        {
            DateTime dN = Convert.ToDateTime(datePicker2.Value.ToString());
            comboDate2.Text = dN.ToString("dd");
            comboMonth2.Text = dN.ToString("MMM", new CultureInfo("th-TH"));
            int yy = int.Parse(dN.ToString("yyyy", new CultureInfo("th-TH")));

            if (yy > 3000)
            {
                yy -= 543;
            }

            comboYear2.Text = yy.ToString("0000");
        }

        private void changeDate(object sender, EventArgs e)
        {
            try
            {
                DateTime dN = Convert.ToDateTime(datePicker.Value.ToString());
                int yy = int.Parse(dN.ToString("yyyy"));
                string mm = dN.ToString("MM");
                string dd = dN.ToString("dd");

                if (comboDate.SelectedItem != null)
                {
                    dd = (comboDate.SelectedItem as Item).Value.ToString();
                }
                if (comboMonth.SelectedItem != null)
                {
                    mm = (comboMonth.SelectedItem as Item).Value.ToString();
                }

                if (comboYear.SelectedItem != null)
                {
                    yy = int.Parse(comboYear.Text);

                    if (yy > 2400)
                    {
                        yy -= 543;
                    }
                }

                string D = yy.ToString("0000") + "/" + mm + "/" + dd;
                datePicker.Text = D;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

        private void changeDate2(object sender, EventArgs e)
        {
            try
            {
                DateTime dN = Convert.ToDateTime(datePicker2.Value.ToString());
                int yy = int.Parse(dN.ToString("yyyy"));
                string mm = dN.ToString("MM");
                string dd = dN.ToString("dd");

                if (comboDate2.SelectedItem != null)
                {
                    dd = (comboDate2.SelectedItem as Item).Value.ToString();
                }
                if (comboMonth2.SelectedItem != null)
                {
                    mm = (comboMonth2.SelectedItem as Item).Value.ToString();
                }

                if (comboYear2.SelectedItem != null)
                {
                    yy = int.Parse(comboYear2.Text);

                    if (yy > 2400)
                    {
                        yy -= 543;
                    }
                }

                string D = yy.ToString("0000") + "/" + mm + "/" + dd;
                datePicker2.Text = D;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            dateStart = datePicker.Text;
            dateEnd = datePicker2.Text;
            this.DialogResult = DialogResult.OK;
        }
    }
}
