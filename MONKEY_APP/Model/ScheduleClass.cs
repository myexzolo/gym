﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class ScheduleClass
    {
        public string id { get; set; }
        public int schedule_id { get; set; }
        public int id_class { get; set; }
        public string EMP_CODE { get; set; }
        public int day { get; set; }
        public int row { get; set; }
        private string dateClass;
        public string date_class
        {
            get { return dateClass; }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                else
                {
                    DateTime dob = DateTime.Parse(value);
                    string dm = dob.ToString("dd/MM", new CultureInfo("th-TH"));
                    int yy = int.Parse(dob.ToString("yyyy", new CultureInfo("th-TH")));

                    if (yy > 3000)
                    {
                        yy -= 543;
                    }
                    date_class_txt = dm + "/" + yy.ToString();
                }

                dateClass = value;
            }

        }
        public string date_class_txt { get; set; }

        public int unit { get; set; }
        public int person_join { get; set; }
        public string time_start { get; set; }
        public string time_end { get; set; }
        public string name_class { get; set; }
        public string EMP_NICKNAME { get; set; }
        public string image_class { get; set; }
        public string sign_emp { get; set; }
    }

}
