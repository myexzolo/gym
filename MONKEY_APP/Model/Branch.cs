﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class Branch
    {
        public bool status  { get; set; }
        public string branch_id { get; set; }
        public string branch_code { get; set; }
        public string cname { get; set; }
        public string branch_name { get; set; }
        public string branch_address { get; set; }
        public string branch_tel { get; set; }
        public string branch_fax { get; set; }
        public string branch_tax { get; set; }
        public string branch_open { get; set; }
        public string branch_close { get; set; }
        public string branch_logo { get; set; }
    }
}
