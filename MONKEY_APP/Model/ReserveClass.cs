﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class ReserveClass
    {
        public string reserve_id { get; set; }
        public string schedule_day_id { get; set; }
        public string PERSON_CODE { get; set; }
        public string PERSON_TEL_MOBILE { get; set; }
        public string person_name { get; set; }
        private string dateReserve;
        public string date_reserve
        {
            get { return dateReserve; }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                else
                {
                    DateTime dob = DateTime.Parse(value);
                    string dm = dob.ToString("dd/MM", new CultureInfo("th-TH"));
                    string time = dob.ToString("HH:mm:ss");
                    int yy = int.Parse(dob.ToString("yyyy", new CultureInfo("th-TH")));

                    if (yy > 3000)
                    {
                        yy -= 543;
                    }
                    date_reserve_txt = dm + "/" + yy.ToString() +" "+ time;
                }

                dateReserve = value;
            }

        }
        public string date_reserve_txt { get; set; }
        public string status { get; set; }
        private string expireDate;
        public string expire_date
        {
            get { return expireDate; }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                else
                {
                    DateTime dob = DateTime.Parse(value);
                    string dm = dob.ToString("dd/MM", new CultureInfo("th-TH"));
                    int yy = int.Parse(dob.ToString("yyyy", new CultureInfo("th-TH")));

                    if (yy > 3000)
                    {
                        yy -= 543;
                    }
                    expire_date_txt = dm + "/" + yy.ToString();
                }

                expireDate = value;
            }

        }
        public string expire_date_txt { get; set; }
        public string branch_code { get; set; }
        public string name_class { get; set; }
        public int unit { get; set; }
        public int person_join { get; set; }
        public int join_seq { get; set; }
        private string dateClass;
        public string date_class
        {
            get { return dateClass; }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                else
                {
                    DateTime dob = DateTime.Parse(value);
                    string dm = dob.ToString("dd/MM", new CultureInfo("th-TH"));
                    int yy = int.Parse(dob.ToString("yyyy", new CultureInfo("th-TH")));

                    if (yy > 3000)
                    {
                        yy -= 543;
                    }
                    date_class_txt = dm + "/" + yy.ToString();
                }

                dateClass = value;
            }

        }
        public string date_class_txt { get; set; }
        public string time_start { get; set; }
        public string time_end { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_NICKNAME { get; set; }
        public string PERSON_NICKNAME { get; set; }
        public string image_class { get; set; }

    }
}
