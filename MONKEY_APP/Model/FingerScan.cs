﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class FingerScan
    {
        public bool status { get; set; }
        public int finger_id { get; set; }
        public string code { get; set; }
        public string finger_num { get; set; }
        public string finger_str { get; set; }
        public string finger_pic { get; set; }
        public string finger_binary { get; set; }
        public string finger_v10 { get; set; }
        public string finger_detail { get; set; }
        public string COMPANY_CODE { get; set; }

    }

    public class FingerScanList
    {
        public List<FingerScan> fingerScanList { get; set; }
    }
}
