﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class Invoice
    {
        public string invoice_id { get; set; }
        public string invoice_code { get; set; }
        private string invoiceDate;
        public string invoice_date
        {
            get { return invoiceDate; }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                else
                {
                    DateTime dDate;
                    if (DateTime.TryParse(value, out dDate))
                    {
                        string dm = dDate.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(dDate.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }
                        invoice_date_txt = dm + "/" + yy.ToString();
                    }
                    else
                    {
                        invoice_date_txt = "";
                    }
                }

                invoiceDate = value;
            }

        }
        public string invoice_date_txt { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string tel { get; set; }
        public string fax { get; set; }
        public string tax { get; set; }
        public string type_payment { get; set; }
        public string total_price { get; set; }
        public string discount { get; set; }
        public string vat { get; set; }
        public string total_net { get; set; }
        public string create_by { get; set; }
        public string typeActive { get; set; }
        public string receipt { get; set; }
        public string type_vat { get; set; }
        public float cash { get; set; }
        public float transfer { get; set; }
        public float credit { get; set; }
        public float cheque { get; set; }
        public string status { get; set; }
        public int num_print { get; set; }
        

    }
}
