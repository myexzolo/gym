﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class Person
    {
        public string COMPANY_CODE { get; set; }
        public string PERSON_RUNNO { get; set; }
        public string PERSON_CODE_INT { get; set; }
        public string PERSON_CODE { get; set; }
        public string PERSON_CODE_OLD { get; set; }
        public string PERSON_TITLE { get; set; }
        public string PERSON_NICKNAME { get; set; }
        public string PERSON_NAME { get; set; }
        public string PERSON_LASTNAME { get; set; }
        public string PERSON_NAME_MIDDLE { get; set; }
        public string PERSON_TITLE_ENG { get; set; }
        public string PERSON_NAME_ENG { get; set; }
        public string PERSON_LASTNAME_ENG { get; set; }
        public string PERSON_SEX { get; set; }
        private string PersonBirthDate;
        public string PERSON_BIRTH_DATE
        {
            get { return PersonBirthDate; }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                else
                {
                    DateTime dDate;
                    if (DateTime.TryParse(value, out dDate))
                    {
                        string dm = dDate.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(dDate.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }
                        PERSON_BIRTH_DATE_TXT = dm + "/" + yy.ToString();
                    }
                    else
                    {
                        PERSON_BIRTH_DATE_TXT = "";
                    }
                }
                
                PersonBirthDate = value;
            }

        }
        public string PERSON_BIRTH_DATE_TXT { get; set; }
        public string PERSON_GRADE { get; set; }
        public string PERSON_PROVINCE_CODE { get; set; }
        public string PERSON_DOOR_CARDNO { get; set; }
        public string PERSON_DOOR_PIN { get; set; }
        public string PERSON_DOOR_PASSWORD { get; set; }
        public string PERSON_DOOR_GROUP { get; set; }
        public string PERSON_CARD_ID { get; set; }
        public string PERSON_CARD_SDATE { get; set; }
        public string PERSON_CARD_SDATE_TXT { get; set; }
        public string PERSON_CARD_EDATE { get; set; }
        public string PERSON_CARD_EDATE_TXT { get; set; }
        public string PERSON_CARD_CREATE_BY { get; set; }
        public string PERSON_TEL_MOBILE { get; set; }
        public string PERSON_TEL_MOBILE2 { get; set; }
        public string PERSON_TEL_OFFICE { get; set; }
        public string PERSON_ER_TEL { get; set; }
        public string PERSON_EMAIL { get; set; }
        public string PERSON_FAX { get; set; }
        public string PERSON_IMAGE { get; set; }
        public string PERSON_IMAGE_ROTATE { get; set; }
        public string PERSON_GROUP_AGE { get; set; }
        public string PERSON_PRIORITY { get; set; }
        public string PERSON_GROUP { get; set; }
        public string PERSON_STATUS { get; set; }
        private string PersonRegDate;
        public string PERSON_REGISTER_DATE
        {
            get { return PersonRegDate; }
            set {
                    if (value == null)
                    {
                        value = "";
                    }
                    else
                    {
                        DateTime dDate;
                        if (DateTime.TryParse(value, out dDate))
                        {
                            string dm = dDate.ToString("dd/MM", new CultureInfo("th-TH"));
                            int yy = int.Parse(dDate.ToString("yyyy", new CultureInfo("th-TH")));

                            if (yy > 3000)
                            {
                                yy -= 543;
                            }
                        PERSON_REGISTER_DATE_TXT = dm + "/" + yy.ToString();
                        }
                        else
                        {
                        PERSON_REGISTER_DATE_TXT = "";
                        }
                    }
                
                    PersonRegDate = value;
                }

        }
        public string PERSON_REGISTER_DATE_TXT { get; set; }

        private string PersonExpDate;
        public string PERSON_EXPIRE_DATE
        {
            get { return PersonExpDate; }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                else
                {
                    DateTime dDate;
                    if (DateTime.TryParse(value, out dDate))
                    {
                        string dm = dDate.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(dDate.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }
                        PERSON_EXPIRE_DATE_TXT = dm + "/" + yy.ToString();
                    }
                    else
                    {
                        PERSON_EXPIRE_DATE_TXT = "";
                    }
                }
                
                PersonExpDate = value;
            }

        }
        public string PERSON_EXPIRE_DATE_TXT { get; set; }
        public string PERSON_NOTE { get; set; }
        private string PersonlastVisit;
        public string PERSON_LAST_VISIT
        {
            get { return PersonlastVisit; }
            set
            {
                if (value != null)
                {
                    DateTime dDate;
                    if (DateTime.TryParse(value, out dDate))
                    {
                        string dm = dDate.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(dDate.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }
                        PERSON_LAST_VISIT_TXT = dm + "/" + yy.ToString();
                    }
                    else
                    {
                        PERSON_LAST_VISIT_TXT = "";
                    }
                }
                PersonlastVisit = value;
            }

        }
        public string PERSON_LAST_VISIT_TXT { get; set; }
        public string PERSON_MUST_AT { get; set; }
        public string PERSON_ADDRESS_ZONE { get; set; }
        public string PERSON_HOME1_ADDR1 { get; set; }
        public string PERSON_HOME1_SUBDISTRICT { get; set; }
        public string PERSON_HOME1_DISTRICT { get; set; }
        public string PERSON_HOME1_PROVINCE { get; set; }
        public string PERSON_HOME1_COUNTRY { get; set; }
        public string PERSON_HOME1_POSTAL { get; set; }
        public string PERSON_HOME1_ADDR2 { get; set; }
        public string PERSON_HOME2_SUBDISTRICT { get; set; }
        public string PERSON_HOME2_DISTRICT { get; set; }
        public string PERSON_HOME2_PROVINCE { get; set; }
        public string PERSON_HOME2_COUNTRY { get; set; }
        public string PERSON_HOME2_POSTAL { get; set; }
        public string PERSON_BILL_TYPE { get; set; }
        public string PERSON_BILL_NAME { get; set; }
        public string PERSON_BILL_ADDR1 { get; set; }
        public string PERSON_BILL_ADDR2 { get; set; }
        public string PERSON_BILL_SUBDISTRICT { get; set; }
        public string PERSON_BILL_DISTRICT { get; set; }
        public string PERSON_BILL_PROVINCE { get; set; }
        public string PERSON_BILL_POSTAL { get; set; }
        public string PERSON_BILL_COUNTRY { get; set; }
        public string PERSON_BILL_TAXNO { get; set; }
        public string PERSON_ACC_NO { get; set; }
        public string RECV_NEWS_VIA { get; set; }
        public string PERSON_MAX_ORDER { get; set; }
        public string PERSON_PRICE { get; set; }
        public string SEND_NEWS_TO { get; set; }
        public string PERSON_SALE_CODE { get; set; }
        public string FNG_TEMPLATE1 { get; set; }
        public string FNG_TEMPLATE2 { get; set; }
        public string FNG_TEMPLATE3 { get; set; }
        public string FNG_TEMPLATE4 { get; set; }
        public string FNG_TEMPLATE5 { get; set; }
        public string DATA_SHIFT_DATE { get; set; }
        public string DATA_SHIFT_CODE { get; set; }
        public string DATA_STATUS { get; set; }
        public string DATA_CREATE_DATE { get; set; }
        public string DATA_CREATE_BY { get; set; }
        public string DATA_CREATE_SHIFT { get; set; }
        public string DATA_MODIFY_DATE { get; set; }
        public string DATA_MODIFY_BY { get; set; }
        public string DATA_CANCLE_DATE { get; set; }
        public string DATA_CANCEL_BY { get; set; }
        public string DATA_CANCEL_NOTE { get; set; }
        public string DATA_DELETE_DATE { get; set; }
        public string DATA_DELETE_BY { get; set; }
        public string DATA_RECORD_RUNNO { get; set; }
        public string PERSON_KEYWORDS { get; set; }
        public string CHECKIN { get; set; }
        public string EMP_CODE_SALE { get; set; }
        private string banReserve;
        public string BAN_RESERVE
        {
            get { return banReserve; }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                else
                {
                    DateTime dDate;
                    if (DateTime.TryParse(value, out dDate))
                    {
                        string dm = dDate.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(dDate.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }
                        BAN_RESERVE_TXT = dm + "/" + yy.ToString();
                    }
                    else
                    {
                        BAN_RESERVE_TXT = "";
                    }
                }

                banReserve = value;
            }
        }
        public string BAN_RESERVE_TXT { get; set; }

    }

    public class PersonList
    {
        public List<Person> Users { get; set; }
    }


}
