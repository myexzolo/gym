﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class District
    {
        public string PROVINCE_CODE { get; set; }
        public string DISTRICT_CODE { get; set; }
        public string DISTRICT_NAME { get; set; }
    }

    public class DistrictList
    {
        public List<District> Districts { get; set; }
    }
}
