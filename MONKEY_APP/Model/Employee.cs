﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class Employee
    {
        public string COMPANY_CODE { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_TITLE { get; set; }
        public string EMP_NAME { get; set; }
        public string EMP_LASTNAME { get; set; }
        public string EMP_POSITION { get; set; }
        public string EMP_DEPARTMENT { get; set; }
        public string EMP_SEX { get; set; }
        public string EMP_BIRTH_DATE { get; set; }
        public string EMP_TYPE { get; set; }
        public string EMP_GROUP { get; set; }
        public string EMP_MONEY_CODE { get; set; }
        public string EMP_PAYROLL { get; set; }
        public string EMP_TYPE_COMMS { get; set; }
        public string EMP_RATE_COMMS { get; set; }
        public string EMP_FIXED_COMMS { get; set; }
        public string EMP_COMMS_SUM_GROUP { get; set; }
        public string EMP_GROUP_AUTHEN { get; set; }
        public string EMP_USER { get; set; }
        public string EMP_PSW { get; set; }
        public string EMP_PSW_EDIT { get; set; }
        public string EMP_PSW_DELETE { get; set; }
        public string EMP_PSW_CANCEL { get; set; }
        public string EMP_NICKNAME { get; set; }
        public string EMP_CARD_ID { get; set; }
        public string EMP_PKS_NO { get; set; }
        public string EMP_TAX_NO { get; set; }
        public string EMP_LICENSE_NO { get; set; }
        public string EMP_SHIFT { get; set; }
        public string EMP_NATION { get; set; }
        public string EMP_RELIGION { get; set; }
        public string EMP_ADDRESS1 { get; set; }
        public string EMP_ADDRESS2 { get; set; }
        public string EMP_ADDR_SUBDISTRICT { get; set; }
        public string EMP_ADDR_DISTRICT { get; set; }
        public string EMP_ADDR_PROVINCE { get; set; }
        public string EMP_ADDR_PROVINCE_CODE { get; set; }
        public string EMP_ADDR_POSTAL { get; set; }
        public string EMP_DOC_NO { get; set; }
        public string EMP_EMAIL { get; set; }
        public string EMP_TEL { get; set; }
        public string EMP_DATE_INCOME { get; set; }
        public string EMP_DATE_RETRY { get; set; }
        public string EMP_PICTURE { get; set; }
        public string EMP_IS_STAFF { get; set; }
        public string EMP_IS_TRAINER { get; set; }
        public string EMP_IS_DOCTOR { get; set; }
        public string EMP_IS_INSTRUCTOR { get; set; }
        public string EMP_IS_SALE { get; set; }
        public string FNG_TEMPLATE1 { get; set; }
        public string FNG_TEMPLATE2 { get; set; }
        public string FNG_TEMPLATE3 { get; set; }
        public string FNG_TEMPLATE4 { get; set; }
        public string FNG_TEMPLATE5 { get; set; }
        public string user_img { get; set; }
        public string EMP_WAGE { get; set; }
        public string branch1 { get; set; }
        public string branch2 { get; set; }
        public string branch3 { get; set; }
        public string branch4 { get; set; }
        public string branch5 { get; set; }
    }

    public class EmployeeList
    {
        public List<Employee> Employees { get; set; }
    }
}
