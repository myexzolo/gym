﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class Menu
    {
        public bool status { get; set; }
        public string page_code { get; set; }
        public string page_name { get; set; }
        public string page_path { get; set; }
        public string module_id { get; set; }
    }


    public class MenuList
    {
        public List<Menu> list { get; set; }
    }
}
