﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThaiNationalIDCard;

namespace MONKEY_APP
{
    public partial class frm_manage_inv : Form
    {
        Package PackageMas;

        private string typeActive;

        public PackagePerson packagePerson;

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        public frm_manage_inv(string typeActive, Package PackageMas, PackagePerson packagePerson)
        {
            this.PackageMas = PackageMas;
            this.packagePerson = packagePerson;
            InitializeComponent();
            this.typeActive = typeActive;

        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private async void TrainerList(string empCode)
        {
            ApiRest.InitailizeClient();

            if (Global.TrainerList == null)
            {
                Global.TrainerList = await ApiProcess.getTrainers();

            }

            for (int x = 0; x < Global.TrainerList.Count; x++)
            {
                Employee Trainer = Global.TrainerList[x];
                comboTrainer.Items.Add(new Item(Trainer.EMP_NAME + " " + Trainer.EMP_LASTNAME, Trainer.EMP_CODE));
                if (Trainer.EMP_CODE.Equals(empCode))
                {
                    comboTrainer.SelectedIndex = x;
                }
            }
        }

        private async void EmpList()
        {
            ApiRest.InitailizeClient();

            if (Global.EmployeeList == null)
            {
                Global.EmployeeList = await ApiProcess.getEmployee("ACTIVE");

            }
            comboEmp.Items.Add(new Item("", ""));
            for (int x = 0; x < Global.EmployeeList.Count; x++)
            {
                Employee employee = Global.EmployeeList[x];
                comboEmp.Items.Add(new Item(employee.EMP_NAME + " " + employee.EMP_LASTNAME, employee.EMP_CODE));
                if (packagePerson != null && packagePerson.sale_code == employee.EMP_CODE)
                {
                    comboEmp.SelectedIndex = x;
                }
            }
        }

        private void frm_manage_cus_Load(object sender, EventArgs e)
        {
            try
            {

                for (int x = 0; x < Global.TIME_UNIT.Count; x++)
                {
                    MASTER master = Global.TIME_UNIT[x];
                    packageUnit.Items.Add(new Item(master.DATA_DESC1, master.DATA_CODE));
                    if (PackageMas.package_unit == master.DATA_DESC1)
                    {
                        packageUnit.SelectedIndex = x;
                    }
                }

                for (int x = 1; x <= 31; x++)
                {
                    comboDate.Items.Add(new Item(x.ToString("00"), x.ToString("00")));
                    comboDate2.Items.Add(new Item(x.ToString("00"), x.ToString("00")));
                }

                for (int monthNo = 1; monthNo <= 12; monthNo++)
                {
                    var bar = new DateTime(DateTime.Now.Year, monthNo, 1);
                    comboMonth.Items.Add(new Item(bar.ToString("MMM", new CultureInfo("th-TH")), monthNo.ToString("00")));
                    comboMonth2.Items.Add(new Item(bar.ToString("MMM", new CultureInfo("th-TH")), monthNo.ToString("00")));
                }

                int ey = int.Parse(DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"))) + 5;
                int sy = (ey - 8);
                for (int x = sy; x < ey; x++)
                {
                    int yearEn = (x - 543);
                    comboYear.Items.Add(new Item(x.ToString(), yearEn.ToString("0000")));
                    comboYear2.Items.Add(new Item(x.ToString(), yearEn.ToString("0000")));
                }

                TrainerList("");
                EmpList();

                if (Global.PERSON.PERSON_EXPIRE_DATE != null && !Global.PERSON.PERSON_EXPIRE_DATE.Equals(""))
                {
                    labStatus.Text = "Member";
                }
                else if (Global.PERSON.PERSON_NAME != null && !Global.PERSON.PERSON_NAME.Equals(""))
                {
                    labStatus.Text = "สมาชิกใหม่";
                }
                else
                {
                    labStatus.Text = "";
                }

               

                if (typeActive.Equals("ADD"))
                {
                    packageId.Text = PackageMas.package_id;
                    packageCode.Text = PackageMas.package_code;
                    packageName.Text = PackageMas.package_name;
                    packageDetail.Text = PackageMas.package_detail;

                    if (PackageMas.package_type.Equals("PT"))
                    {
                        comboTrainer.Enabled = true;
                    }

                    DateTime dNow;
                    DateTime dN = DateTime.Now;
                    if (Global.PERSON.PERSON_EXPIRE_DATE != null && PackageMas.package_type != null && PackageMas.package_type.Equals("MB") && !Global.PERSON.PERSON_EXPIRE_DATE.Equals(""))
                    {
                        //MessageBox.Show(Global.PERSON.PERSON_EXPIRE_DATE);
 
                        dNow = Convert.ToDateTime(Global.PERSON.PERSON_EXPIRE_DATE).AddDays(1);
                        dN = Convert.ToDateTime(Global.PERSON.PERSON_EXPIRE_DATE).AddDays(1);
                        comboDate.Text = dN.ToString("dd");
                        comboMonth.Text = dN.ToString("MMM",new CultureInfo("th-TH"));
                        comboYear.Text = dN.ToString("yyyy", new CultureInfo("th-TH"));
                    }
                    else
                    {
                        dNow = dN;
                        comboDate.Text = dN.ToString("dd", new CultureInfo("th-TH"));
                        comboMonth.Text = dN.ToString("MMM", new CultureInfo("th-TH"));
                        comboYear.Text = dN.ToString("yyyy", new CultureInfo("th-TH"));
                    }



                    label1.Text = "สร้างรายการ";
                    genRegNo(PackageMas.package_type);


                    int yy = int.Parse(dNow.ToString("yyyy"));

                    if (yy > 2400)
                    {
                        yy -= 543;
                    }

                    string D = yy.ToString("0000") + dNow.ToString("/MM/dd");

                    dateIncome.Text     = D;


                    maxUse.Value        = PackageMas.max_use;
                    packagePrice.Text   = String.Format("{0:n}", PackageMas.package_price);
                
                    //numUse.Text       = PackageMas.package_type;
                    numUse.Value        = PackageMas.num_use;
                    //numDic.Value      = "";

                    calDateUse();

                }
                else if (typeActive.Equals("EDIT"))
                {
                    label1.Text = "แก้ไขรายการ";
                    try
                    {
                        if (packagePerson != null)
                        {
                            packageId.Text      = packagePerson.package_id;
                            packageCode.Text    = packagePerson.package_code;
                            packageName.Text    = packagePerson.package_name;
                            packageDetail.Text  = packagePerson.package_detail;

                            if (packagePerson.type_package.Equals("PT"))
                            {
                                comboTrainer.Enabled = true;
                            }

                            if (packagePerson.type_buy.Equals("N"))
                            {
                                radNew.Checked = true;
                            }
                            else if (packagePerson.type_buy.Equals("R")) {
                                radRENew.Checked = true;
                            }


                            txtRunReg.Text = packagePerson.reg_no;
                            numUse.Value = packagePerson.num_use;

                            for (int x = 0; x < Global.TIME_UNIT.Count; x++)
                            {
                                MASTER master = Global.TIME_UNIT[x];
                                if (packagePerson.package_unit == master.DATA_CODE)
                                {
                                    packageUnit.SelectedIndex = x;
                                }
                            }

                            for (int x = 0; x < Global.EmployeeList.Count; x++)
                            {
                                Employee employee = Global.EmployeeList[x];
                                if (packagePerson != null && packagePerson.sale_code == employee.EMP_CODE)
                                {
                                    comboEmp.SelectedIndex = (x+1);
                                }
                            }

                            DateTime dateStart = Convert.ToDateTime(packagePerson.date_start);

                            int yy = int.Parse(dateStart.ToString("yyyy"));

                            if (yy > 2400)
                            {
                                yy -= 543;
                            }

                            dateIncome.Text = yy.ToString("0000") + dateStart.ToString("/MM/dd");


                            DateTime dateExpire = Convert.ToDateTime(packagePerson.date_expire);

                            yy = int.Parse(dateExpire.ToString("yyyy"));

                            if (yy > 2400)
                            {
                                yy -= 543;
                            }

                            expDate.Text = yy.ToString("0000") + dateExpire.ToString("/MM/dd");


                            maxUse.Value = packagePerson.max_use;
                            packagePrice.Text = packagePerson.package_price;
                            numPack.Value = packagePerson.package_num;
                            txtPt.Text = packagePerson.package_price_total;
                            numDic.Value = packagePerson.percent_discount;
                            txtDiscount.Text = packagePerson.discount;
                            txtTotal.Text = packagePerson.net_total;
                            txtID.Text = packagePerson.id;
                            comboTrainer.Text = packagePerson.trainer_name;
                            PackageMas.type_vat = "";
                        }
                        button4.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }

                calPrice();
                }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

        private async void genRegNo(string typePackage)
        {
            try
            {
                ApiRest.InitailizeClient();
                Response res = await ApiProcess.genRegNo(Global.BRANCH.branch_code, Global.BRANCH.branch_id, typePackage);
                txtRunReg.Text = res.message;
                button4.Enabled = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


            private void setAlertMessage(String str)
        {
            if (this.InvokeRequired)
            {

                this.Invoke(new MethodInvoker(delegate
                {
                    alertTxt.Visible = true;
                    alertTxt.Text = str;
                }));
            }
            else
            {
                alertTxt.Visible = true;
                alertTxt.Text = str;
            }
        }


        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void maxUse_ValueChanged(object sender, EventArgs e)
        {
            if (!this.dateIncome.Text.Equals(""))
            {
                DateTime inDate = Convert.ToDateTime(this.dateIncome.Text);
                int max = (int)maxUse.Value;
                string expirePackD = inDate.AddDays(max).ToString("yyyy/MM/dd");
                expDate.Text = expirePackD;



            }
            
        }

        private void dateIncome_ValueChanged(object sender, EventArgs e)
        {

            DateTime dN = Convert.ToDateTime(dateIncome.Value.ToString());
            comboDate.Text = dN.ToString("dd");
            comboMonth.Text = dN.ToString("MMM", new CultureInfo("th-TH"));
            int yy = int.Parse(dN.ToString("yyyy", new CultureInfo("th-TH")));

            if (yy > 3000)
            {
                yy -= 543;
            }

            comboYear.Text = yy.ToString("0000");
            calDateUse();
        }

        private void expDate_ValueChanged(object sender, EventArgs e)
        {
            DateTime dN = Convert.ToDateTime(expDate.Value.ToString());
            comboDate2.Text = dN.ToString("dd");
            comboMonth2.Text = dN.ToString("MMM", new CultureInfo("th-TH"));
            int yy = int.Parse(dN.ToString("yyyy", new CultureInfo("th-TH")));

            if (yy > 3000)
            {
                yy -= 543;
            }

            comboYear2.Text = yy.ToString("0000");
        }

        private void calDateUse()
        {
            try
            {

                DateTime inDate = Convert.ToDateTime(this.dateIncome.Text);
                DateTime edate = Convert.ToDateTime(this.expDate.Text);
                string expireD = "";
                double diffdate = 0;
                string packageUnitCode = "";
                if (packageUnit.SelectedItem != null)
                {
                    packageUnitCode = (packageUnit.SelectedItem as Item).Value.ToString();
                }
                else
                {
                    return;
                }
                

                if (packageUnitCode == "MONTHS")
                {
                    if (numUse.Value <= 100)
                    {
                        edate = inDate.AddMonths((int)numUse.Value);
                        expireD = edate.ToString("yyyy/MM/dd");
                        diffdate = (edate - inDate).TotalDays;
                        maxUse.Value = Convert.ToInt32(diffdate);
                    } 
                }
                else if (packageUnitCode == "DAYS")
                {
                    edate = inDate.AddDays((int)numUse.Value);
                    expireD = edate.ToString("yyyy/MM/dd");
                    diffdate = (edate - inDate).TotalDays;
                    maxUse.Value = Convert.ToInt32(diffdate);
                }
                else if (packageUnitCode == "YEAR")
                {
                    edate = inDate.AddYears((int)numUse.Value);
                    expireD = inDate.AddYears((int)numUse.Value).ToString("yyyy/MM/dd");
                    diffdate = (edate - inDate).TotalDays;
                    maxUse.Value = Convert.ToInt32(diffdate);
                }
                else if (packageUnitCode == "VIP")
                {
                    edate = inDate.AddYears((int)numUse.Value);
                    expireD = inDate.AddYears(10).ToString("yyyy/MM/dd");
                    diffdate = 9999;
                    maxUse.Value = Convert.ToInt32(diffdate);
                }
                else
                {
                    int max = (int)maxUse.Value;
                    if (max > 0)
                    {
                        expireD = inDate.AddDays(max).ToString("yyyy/MM/dd");
                    }
                    else
                    {
                        expireD = inDate.ToString("yyyy/MM/dd");
                    }   
                }
                expDate.Text = expireD;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
           
        }

        private void calPrice()
        {
            try
            {
                float price = 0;
                float vat = 0;
                float total = 0;
                float discount = 0;

                float package_price = 0;

                if (typeActive.Equals("EDIT"))
                {
                    package_price = float.Parse(packagePerson.package_price);
                }
                else {
                    package_price = PackageMas.package_price;
                }


                int numP    = (int)numPack.Value;
                discount    = (int)numDic.Value;
                float net   = (package_price * numP);

                //discount = (numDiscount * net) / 100;


                txtPt.Text = String.Format("{0:n}", net);
                txtDiscount.Text = String.Format("{0:n}", discount);


                if (PackageMas.type_vat.Equals("I"))
                {
                    price = net;
                    vat = price - (price * 100 / 107);
                    total = price - discount;
                }
                else if (PackageMas.type_vat.Equals("E"))
                {
                    price = net;
                    vat = price * (7 / 100);
                    total = price + vat - discount;
                }
                else if (PackageMas.type_vat.Equals("N"))
                {
                    price = net;
                    vat = 0;
                    total = price - discount;
                }
                else if (PackageMas.type_vat.Equals(""))
                {
                    price = net;
                    vat = 0;
                    total = price - discount;
                }

                //txtVat.Text     = String.Format("{0:n}", vat);
                txtTotal.Text = String.Format("{0:n}", total);

            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: calPrice ::" + ex.ToString(), "frm_manage_inv");

            }

        }

        private void numUse_ValueChanged(object sender, EventArgs e)
        {
            calDateUse();
        }

        private void packageUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            calDateUse();
        }

        private void numPack_ValueChanged(object sender, EventArgs e)
        {
            calPrice();
        }

        private void numDic_ValueChanged(object sender, EventArgs e)
        {
            calPrice();
        }

        private void numDic_Enter(object sender, EventArgs e)
        {
            calPrice();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                alertTxt.Text = "";
                alertTxt.Visible = false;
                this.DialogResult = DialogResult.None;

                if (numUse.Value == 0)
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ อายุสมาชิก **");
                    numUse.Focus();
                }
                else if (packageUnit.Text.Trim().Equals(""))
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุประเภทช่วงเวลา **");
                    packageUnit.Focus();
                }
                else if (dateIncome.Value > expDate.Value)
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** ระบุวันที่เริ่มไม่ถูกต้อง **");
                    dateIncome.Focus();
                }
                else if (maxUse.Value == 0)
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ จำนวนวันหมดอายุแพคเกจ **");
                    maxUse.Focus();
                }
                else if (numPack.Value == 0)
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ จำนวนเพคเกจ **");
                    numPack.Focus();
                }
                else if (!radNew.Checked && !radRENew.Checked)
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ ประเภทการซื้อ **");
                }
                else if (comboEmp.SelectedIndex <= 0)
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ ผู้ดูแล **");
                    comboEmp.Focus();
                }
                else
                {
                    PackagePerson ps = new PackagePerson();

                    ps.company_code     = Global.BRANCH.branch_code;
                    ps.person_code      = Global.PERSON.PERSON_CODE;
                    ps.package_code     = packageCode.Text;
                    ps.package_name     = packageName.Text;
                    ps.package_detail   = packageDetail.Text;
                    ps.reg_no            = txtRunReg.Text;
                    ps.num_use          = (int)numUse.Value;
                    ps.package_unit     = (packageUnit.SelectedItem as Item).Value.ToString();
                    ps.date_start       = dateIncome.Value.ToString("yyyy-MM-dd");
                    ps.date_expire      = expDate.Value.ToString("yyyy-MM-dd");
                    ps.max_use          = (int)maxUse.Value;
                    ps.type_package         = PackageMas.package_type;
                    ps.package_price        = packagePrice.Text;
                    ps.package_num          = (int)numPack.Value;
                    ps.package_price_total  = txtPt.Text;
                    ps.percent_discount = (int)numDic.Value;
                    ps.discount             = txtDiscount.Text;
                    ps.vat                  = "0";
                    ps.net_total            = txtTotal.Text;
                    ps.id                   = txtID.Text;
                    ps.trainer_code         = comboTrainer.SelectedItem != null ? (comboTrainer.SelectedItem as Item).Value.ToString() : "";
                    ps.trainer_name         = comboTrainer.SelectedItem != null ? (comboTrainer.SelectedItem as Item).Name.ToString() : "";
                    ps.sale_code            = comboEmp.SelectedItem != null ? (comboEmp.SelectedItem as Item).Value.ToString() : "";
                    ps.notify_num           = PackageMas.notify_num;
                    ps.notify_unit          = PackageMas.notify_unit;
                    ps.type_vat             = PackageMas.type_vat;
                    ps.package_id           = PackageMas.package_id;
                    ps.invoice_code = "";

                    if (radNew.Checked) {
                        ps.type_buy = "N";
                    } else if (radRENew.Checked)
                    {
                        ps.type_buy = "R";
                    }
                    

                    packagePerson = ps;

                    this.DialogResult = DialogResult.OK;

                    //ApiRest.InitailizeClient();
                    //Response res = await ApiProcess.managePerson(Global.BRANCH.branch_code, typeActive, person, Global.USER.user_login);

                }
            }
            catch
            {
                alertTxt.Visible = true;
                alertTxt.Text = "** ไม่สามารถเชื่อมต่อ Server ได้ **";
            }
            finally
            {
               
            }
        }

        private void numDic_KeyUp(object sender, KeyEventArgs e)
        {
            calPrice();
        }

        private void numPack_KeyUp(object sender, KeyEventArgs e)
        {
            calPrice();
        }

        private void changeDateIn(object sender, EventArgs e)
        {
            try
            {
                DateTime dN = Convert.ToDateTime(dateIncome.Value.ToString());
                int yy = int.Parse(dN.ToString("yyyy"));
                string mm = dN.ToString("MM");
                string dd = dN.ToString("dd");

                if (comboDate.SelectedItem != null)
                {
                    dd = (comboDate.SelectedItem as Item).Value.ToString();
                }
                if (comboMonth.SelectedItem != null)
                {
                    mm = (comboMonth.SelectedItem as Item).Value.ToString();
                }

                if (comboYear.SelectedItem != null)
                {
                    yy = int.Parse(comboYear.Text);

                    if (yy > 2400)
                    {
                        yy -= 543;
                    }
                }

                string D = yy.ToString("0000") + "/" + mm + "/" + dd;
                dateIncome.Value = Convert.ToDateTime(D);
                //calDateUse();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }


        private void changeDateEx(object sender, EventArgs e)
        {
            try
            {
                DateTime dN = Convert.ToDateTime(expDate.Value.ToString());
                int yy = int.Parse(dN.ToString("yyyy"));
                string mm = dN.ToString("MM");
                string dd = dN.ToString("dd");

                if (comboDate2.SelectedItem != null)
                {
                    dd = (comboDate2.SelectedItem as Item).Value.ToString();
                }
                if (comboMonth2.SelectedItem != null)
                {
                    mm = (comboMonth2.SelectedItem as Item).Value.ToString();
                }
                if (comboYear2.SelectedItem != null)
                {
                    yy = int.Parse(comboYear2.Text);

                    if (yy > 2400)
                    {
                        yy -= 543;
                    }
                }

                string D = yy.ToString("0000") + "/" + mm + "/" + dd;
                expDate.Value = Convert.ToDateTime(D);
                //calDateUse();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void numUse_KeyDown(object sender, KeyEventArgs e)
        {
            //calDateUse();
        }

        private void numUse_KeyPress(object sender, KeyPressEventArgs e)
        {
            //calDateUse();
        }

        private void numUse_KeyUp(object sender, KeyEventArgs e)
        {
            calDateUse();
        }

        private void packageUnit_SelectedValueChanged(object sender, EventArgs e)
        {
            calDateUse();
        }
    }
}
