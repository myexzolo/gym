﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class frm_trainer : Form
    {

        public frm_trainer()
        {
            InitializeComponent();
        }

        string checkin = "";
        private DataTable dtm;
        //private DataView dv;

        string pamPersonCode = "";

        PersonCheckin pc;

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                DataView dv = dtm.DefaultView;
                dv.RowFilter = string.Format("รหัสลูกค้า like '%{0}%' or [ชื่อ - นามสกุล] like '%{0}%' or [ชื่อเล่น] like '%{0}%'", txtSearch.Text);

                dataGridView2.DataSource = dv.ToTable();

                if (dataGridView2.Rows.Count == 0)
                {
                    //dataGridView2.Rows.Clear();
                    clearData();
                }

                if (txtSearch.Text.Equals(""))
                {
                    clearData();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void frm_trainer_Load(object sender, EventArgs e)
        {
            int scrWidth = Screen.PrimaryScreen.Bounds.Width;
            int scrHeight = Screen.PrimaryScreen.Bounds.Height;

            int panelw1 = scrWidth - 265;

            int panelM = panelw1 / 100 * 96;

            //panelMemberList.Width = panelw1;
            panelHistory.Width = panelw1;
            panelHistory.Height = scrHeight - 280;


            int num = 0;
            tableLayoutPanel.ColumnCount = (num + 1);

            List<Button> buttons = new List<Button>();

            for (var i = 0; i < Global.MENULIST.Count; i++)
            {
                string btnName = "";
                int width = 125;
                string menuName = Global.MENULIST[i].page_name;
                string code = Global.MENULIST[i].page_code;
                string moduleId = Global.MENULIST[i].module_id;
                Image img = global::MONKEY_APP.Properties.Resources.folder2;

                if (moduleId.Equals("9"))
                {

                    if (code.Equals("HTRN"))
                    {
                        width = 160;
                        img = global::MONKEY_APP.Properties.Resources.eye2;
                    }
                    

                    btnName = code;
                    buttons.Add(genButtonMenu(btnName, width, menuName, img));

                    num++;
                }
            }

            tableLayoutPanel.ColumnCount = buttons.Count;

            for (var i = 0; i < buttons.Count; i++)
            {
                Button btn = buttons[i];
                tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
                tableLayoutPanel.Controls.Add(btn, i, 0);
            }

            txtSearch.Focus();

            if (Global.PERSON.PERSON_CODE != null && !Global.PERSON.PERSON_CODE.Equals(""))
            {
                pamPersonCode = Global.PERSON.PERSON_CODE;
                checkin = Global.PERSON.CHECKIN;
            }
            else
            {
                Global.PERSON = new Person();
                checkin = "";
            }


            setPersonCheckin("'CI'","'PT'","");

            if (!pamPersonCode.Equals("") && checkin != null && checkin.Equals("Y"))
            {
                txtSearch.Text = pamPersonCode;
                pamPersonCode = "";
            }

            picLoading.Visible = false;
        }

        private Button genButtonMenu(string btnName, int width, string menuName, Image img)
        {
            Button btn = new Button();
            btn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            btn.FlatAppearance.BorderSize = 0;
            btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btn.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            btn.Image = img;
            btn.Name = btnName;
            btn.Tag = btnName;
            btn.Size = new System.Drawing.Size(width, 40);
            btn.Text = menuName;
            btn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btn.UseVisualStyleBackColor = false;
            btn.Click += new System.EventHandler(this.buttonClick);

            return btn;
        }


        private void buttonClick(object sender, EventArgs e)
        {
            string code = ((Button)sender).Tag.ToString();
            if (code.Equals("HTRN"))
            {
                frm_history_trainer fmc = new frm_history_trainer();
                fmc.ShowDialog();
            } 
        }

        public byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }


        private async void setPersonCheckin(string stausCheckin,string typePakage,string id)
        {

            try
            {
                rowindex = -1;
                ApiRest.InitailizeClient();
                var psList = await ApiProcess.getPersonCheckin(Global.BRANCH.branch_code, stausCheckin, "", "1000", typePakage, "DESC", id);
                //dataGridView2.Rows.Clear();
                dtm = new DataTable();

                dtm.Columns.Add("ยกเลิก", typeof(byte[]));
                dtm.Columns.Add("เวลาเข้างาน", typeof(string));
                dtm.Columns.Add("รหัสลูกค้า", typeof(string));
                dtm.Columns.Add("ชื่อเล่น", typeof(string));
                dtm.Columns.Add("ชื่อ - นามสกุล", typeof(string));
                dtm.Columns.Add("รายการ", typeof(string));
                dtm.Columns.Add("ลูกค้า", typeof(bool));
                dtm.Columns.Add("ครูฝึก", typeof(bool));
                dtm.Columns.Add("ผู้จัดการ", typeof(bool));
                dtm.Columns.Add("สถานะ", typeof(string));
                dtm.Columns.Add("คงเหลือ", typeof(string));
                dtm.Columns.Add("ใช้ไป", typeof(string));
                dtm.Columns.Add("ครั้งนี้", typeof(string));
                dtm.Columns.Add("ทั้งหมด", typeof(string));
                dtm.Columns.Add("หน่วย", typeof(string));
                dtm.Columns.Add("เทรนCode", typeof(string));
                dtm.Columns.Add("เพศ", typeof(string));
                dtm.Columns.Add("id", typeof(string));
                dtm.Columns.Add("package_person_id", typeof(string));
                dtm.Columns.Add("trainerCode", typeof(string));
                dtm.Columns.Add("managerCode", typeof(string));
                dtm.Columns.Add("ชื่อ", typeof(string));
                dtm.Columns.Add("นามสกุล", typeof(string));
                dtm.Columns.Add("note", typeof(string));
                dtm.Columns.Add("รูป", typeof(string));

                DateTime dDate;
                Bitmap Image2 = global::MONKEY_APP.Properties.Resources.error30;
                Bitmap Image3 = global::MONKEY_APP.Properties.Resources.error30_d;

                byte[] cancelImage = ImageToByte(Image2);



                for (var i = 0; i < psList.Count; i++)
                {
                    PersonCheckin ps = psList[i];
                    bool sign_person = false;
                    bool sign_emp = false;
                    bool sign_manager = false;


                    string StatusScan = "ยังไม่มีการบันทึก";

                    if (ps.sign_person != null && !ps.sign_person.Equals(""))
                    {
                        sign_person = true;
                        StatusScan = "ลูกค้าสแกนนิ้ว";
                    }

                    if (ps.sign_emp != null &&  !ps.sign_emp.Equals(""))
                    {
                        sign_emp = true;
                        StatusScan = "เทรนเนอร์สแกนนิ้ว";
                    }

                    if (ps.sign_manager != null &&  !ps.sign_manager.Equals(""))
                    {
                        sign_manager = true;
                        StatusScan = "สมบูรณ์";
                    }

                    if (sign_person)
                    {
                        cancelImage = ImageToByte(Image3);
                    }
                    else
                    {
                        cancelImage = ImageToByte(Image2);
                    }

                    string checkinDate = "";
                    if (DateTime.TryParse(ps.checkin_date, out dDate))
                    {
                        DateTime ed = Convert.ToDateTime(ps.checkin_date);
                        checkinDate = ed.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("th-TH"));
                    }

                    //int use = int.Parse(ps.use_package) - 1;
                    int total = int.Parse(ps.num_use) - int.Parse(ps.use_package);

                    dtm.Rows.Add(cancelImage, checkinDate, ps.PERSON_CODE,ps.PERSON_NICKNAME, ps.PERSON_NAME + " " + ps.PERSON_LASTNAME, ps.package_name, sign_person, sign_emp, sign_manager,
                        StatusScan, ps.use_package, total, 1, ps.num_use, ps.package_unit, ps.trainer_code, ps.PERSON_SEX, ps.id, ps.package_person_id,
                        ps.trainerCode, ps.managerCode, ps.PERSON_NAME, ps.PERSON_LASTNAME, ps.PERSON_NOTE, ps.PERSON_IMAGE);
                }

                if (dtm != null)
                {
                    dataGridView2.DataSource = dtm;
                }

                dataGridView2.Columns[0].Width = 80;
                dataGridView2.Columns[1].Width = 155;
                dataGridView2.Columns[2].Width = 100;
                dataGridView2.Columns[3].Width = 100;
                dataGridView2.Columns[4].Width = 190;
                dataGridView2.Columns[5].Width = 340;
                dataGridView2.Columns[6].Width = 85;
                dataGridView2.Columns[7].Width = 85;
                dataGridView2.Columns[8].Width = 85;
                dataGridView2.Columns[9].Width = 120;
                dataGridView2.Columns[10].Width = 80;
                dataGridView2.Columns[11].Width = 80;
                dataGridView2.Columns[12].Width = 80;
                dataGridView2.Columns[13].Width = 80;
                dataGridView2.Columns[14].Width = 100;

                dataGridView2.Columns[15].Visible = false;
                dataGridView2.Columns[16].Visible = false;
                dataGridView2.Columns[17].Visible = false;
                dataGridView2.Columns[18].Visible = false;
                dataGridView2.Columns[19].Visible = false;
                dataGridView2.Columns[20].Visible = false;
                dataGridView2.Columns[21].Visible = false;
                dataGridView2.Columns[22].Visible = false;
                dataGridView2.Columns[23].Visible = false;
                dataGridView2.Columns[24].Visible = false;

                dataGridView2.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[12].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[13].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[14].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


                dataGridView2.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[9].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[10].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[11].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[12].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[13].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[14].SortMode = DataGridViewColumnSortMode.NotSortable;
                button2.Enabled = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //Utils.getErrorToLog(":: setPersonCheckin ::" + ex.ToString(), "frm_trainer");

            }
            finally
            {
                ClearSelection();
            }
        }



        private DataGridViewTextBoxColumn setColumnTextBox(string HeaderText, string Name, int Width)
        {
            try
            {
                DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();

                column.HeaderText = HeaderText;
                column.Name = Name;
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
                column.Width = Width;

                return column;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
            
        }

        private void clearData()
        {
            dataGridView2.ClearSelection();
            labCode.Text = "";
            labName.Text = "";
            labLname.Text = "";
            labStatus.Text = "";
            txtNote.Text = "";

            picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
            picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            picPerson.Size = new System.Drawing.Size(148, 178);
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = dtm.DefaultView;
                dv.RowFilter = string.Format("รหัสลูกค้า like '%{0}%' or [ชื่อ - นามสกุล] like '%{0}%' or [ชื่อเล่น] like '%{0}%'", txtSearch.Text);

                dataGridView2.DataSource = dv.ToTable();

                if (dataGridView2.Rows.Count == 0)
                {
                    //dataGridView2.Rows.Clear();
                    clearData();
                }

                if (txtSearch.Text.Equals(""))
                {
                    clearData();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }      
        }


        private async void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (pc != null && !pc.PERSON_CODE.Equals(""))
                {

                    string sign_person_tmp = pc.sign_person;
                    string sign_emp_tmp = pc.sign_emp;
                    string sign_manager_tmp = pc.sign_manager;

                    frm_manage_checkin_trainer fmc = new frm_manage_checkin_trainer(pc);
                    var result = fmc.ShowDialog();              
                    if (result == DialogResult.OK)
                    {
                        //rowindex = -1;
                        ApiRest.InitailizeClient();
                        var psList = await ApiProcess.getPersonCheckin(Global.BRANCH.branch_code, "'CI'", "", "1", "'PT'", "ASC", pc.id);

                        sign_person_tmp = pc.sign_person;
                        sign_emp_tmp = pc.sign_emp;
                        sign_manager_tmp = pc.sign_manager;

                        DateTime dDate;
                        for (var i = 0; i < psList.Count; i++)
                        {
                            PersonCheckin ps = psList[i];
                            bool sign_person = false;
                            bool sign_emp = false;
                            bool sign_manager = false;
                            string num = "1";

                            string StatusScan = "ยังไม่มีการบันทึก";

                            if (!ps.sign_person.Equals(""))
                            {
                                sign_person = true;
                                StatusScan = "ลูกค้าสแกนนิ้ว";
                            }

                            if (!ps.sign_emp.Equals(""))
                            {
                                sign_emp = true;
                                StatusScan = "เทรนเนอร์สแกนนิ้ว";
                            }

                            if (!ps.sign_manager.Equals(""))
                            {
                                sign_manager = true;
                                StatusScan = "สมบูรณ์";
                                num = "0";
                            }

                            //int use = int.Parse(ps.use_package) - 1;
                            string checkinDate = "";
                            int total = int.Parse(ps.num_use) - int.Parse(ps.use_package);

                            if (DateTime.TryParse(ps.checkin_date, out dDate))
                            {
                                DateTime ed = Convert.ToDateTime(ps.checkin_date);
                                checkinDate = ed.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("th-TH"));
                            }

                            Bitmap Image2 = global::MONKEY_APP.Properties.Resources.error30;
                            Bitmap Image3 = global::MONKEY_APP.Properties.Resources.error30_d;

                            byte[] cancelImage = ImageToByte(Image2);

                            if (sign_person)
                            {
                                cancelImage = ImageToByte(Image3);
                            }

                            dataGridView2.Rows[rowindex].Cells[0].Value = cancelImage;
                            dataGridView2.Rows[rowindex].Cells[1].Value = checkinDate;
                            dataGridView2.Rows[rowindex].Cells[2].Value = ps.PERSON_CODE;
                            dataGridView2.Rows[rowindex].Cells[3].Value = ps.PERSON_NICKNAME;
                            dataGridView2.Rows[rowindex].Cells[4].Value = ps.PERSON_NAME + " " + ps.PERSON_LASTNAME;
                            dataGridView2.Rows[rowindex].Cells[5].Value = ps.package_name;
                            dataGridView2.Rows[rowindex].Cells[6].Value = sign_person;
                            dataGridView2.Rows[rowindex].Cells[7].Value = sign_emp;
                            dataGridView2.Rows[rowindex].Cells[8].Value = sign_manager;
                            dataGridView2.Rows[rowindex].Cells[9].Value = StatusScan;
                            dataGridView2.Rows[rowindex].Cells[10].Value = ps.use_package;
                            dataGridView2.Rows[rowindex].Cells[11].Value = total;
                            dataGridView2.Rows[rowindex].Cells[12].Value = num;
                            dataGridView2.Rows[rowindex].Cells[13].Value = ps.num_use;
                            dataGridView2.Rows[rowindex].Cells[14].Value = ps.package_unit;
                            dataGridView2.Rows[rowindex].Cells[15].Value = ps.trainer_code;
                            dataGridView2.Rows[rowindex].Cells[16].Value = ps.PERSON_SEX;
                            dataGridView2.Rows[rowindex].Cells[17].Value = ps.id;
                            dataGridView2.Rows[rowindex].Cells[18].Value = ps.package_person_id;
                            dataGridView2.Rows[rowindex].Cells[19].Value = ps.trainerCode;
                            dataGridView2.Rows[rowindex].Cells[20].Value = ps.managerCode;
                            dataGridView2.Rows[rowindex].Cells[21].Value = ps.PERSON_NAME;
                            dataGridView2.Rows[rowindex].Cells[22].Value = ps.PERSON_LASTNAME;
                            dataGridView2.Rows[rowindex].Cells[23].Value = ps.PERSON_NOTE;
                            dataGridView2.Rows[rowindex].Cells[24].Value = ps.PERSON_IMAGE;

                            pc = ps;
                        }
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        pc.sign_person  = sign_person_tmp;
                        pc.sign_emp  = sign_emp_tmp;
                        pc.sign_manager = sign_manager_tmp;
                    }
                    else if (result == DialogResult.Abort)
                    {
                        setPersonCheckin("'CI'", "'PT'", "");
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            } 
        }

        private int rowindex = -1;
        private string id_pc =  "";

        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                dataGridView2.Refresh();
                if (dataGridView2.CurrentCell != null)
                {
                    pc = new PersonCheckin();
                    rowindex = dataGridView2.CurrentCell.RowIndex;

                    id_pc = dataGridView2.Rows[rowindex].Cells[17].Value.ToString();


                    pc.checkin_date = dataGridView2.Rows[rowindex].Cells[1].Value.ToString();
                    pc.PERSON_CODE = dataGridView2.Rows[rowindex].Cells[2].Value.ToString();
                    pc.PERSON_NICKNAME = dataGridView2.Rows[rowindex].Cells[3].Value.ToString();
                    pc.PERSON_NAME = dataGridView2.Rows[rowindex].Cells[4].Value.ToString();
                    pc.package_name = dataGridView2.Rows[rowindex].Cells[5].Value.ToString();
                    pc.sign_person = dataGridView2.Rows[rowindex].Cells[6].Value.ToString();
                    pc.sign_emp = dataGridView2.Rows[rowindex].Cells[7].Value.ToString();
                    pc.sign_manager = dataGridView2.Rows[rowindex].Cells[8].Value.ToString();
                    pc.status = dataGridView2.Rows[rowindex].Cells[9].Value.ToString();
                    pc.use_package = dataGridView2.Rows[rowindex].Cells[10].Value.ToString();
                    pc.pk_use = dataGridView2.Rows[rowindex].Cells[10].Value.ToString();
                    //Global.PERSON.PERSON_STATUS = dataGridView2.Rows[rowindex].Cells[11].Value.ToString();
                    pc.num_use = dataGridView2.Rows[rowindex].Cells[13].Value.ToString();
                    pc.package_unit = dataGridView2.Rows[rowindex].Cells[14].Value.ToString();
                    pc.trainer_code = dataGridView2.Rows[rowindex].Cells[15].Value.ToString();
                    pc.PERSON_SEX = dataGridView2.Rows[rowindex].Cells[16].Value.ToString();
                    pc.id = id_pc;
                    pc.package_person_id = dataGridView2.Rows[rowindex].Cells[18].Value.ToString();
                    pc.trainerCode = dataGridView2.Rows[rowindex].Cells[19].Value.ToString();
                    pc.managerCode = dataGridView2.Rows[rowindex].Cells[20].Value.ToString();
                    string Name = dataGridView2.Rows[rowindex].Cells[21].Value.ToString();
                    string Lname = dataGridView2.Rows[rowindex].Cells[22].Value.ToString();
                    pc.PERSON_NOTE = dataGridView2.Rows[rowindex].Cells[23].Value.ToString();


                    labCode.Text = pc.PERSON_CODE;
                    labName.Text = Name;
                    labLname.Text = Lname;
                    labStatus.Text = "ACTIVE";
                    txtNote.Text = pc.PERSON_NOTE;

                    if (!dataGridView2.Rows[rowindex].Cells[24].Value.ToString().Equals(""))
                    {
                        string picCus = dataGridView2.Rows[rowindex].Cells[24].Value.ToString();
                        Utils.setImage(picCus, picPerson);
                    }
                    else
                    {
                        picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                        picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
                        picPerson.Size = new System.Drawing.Size(148, 178);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void showloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = true;
                }));
            }
            else
            {
                picLoading.Visible = true;
            }

        }

        private async void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                string typeActive = "";
                int rowinx = dataGridView2.CurrentCell.RowIndex;
                PersonCheckin pci = new PersonCheckin();
                id_pc = dataGridView2.Rows[rowinx].Cells[17].Value.ToString();


                pci.checkin_date = dataGridView2.Rows[rowinx].Cells[1].Value.ToString();
                pci.PERSON_CODE = dataGridView2.Rows[rowinx].Cells[2].Value.ToString();
                pci.PERSON_NICKNAME = dataGridView2.Rows[rowinx].Cells[3].Value.ToString();
                pci.PERSON_NAME = dataGridView2.Rows[rowinx].Cells[4].Value.ToString();
                pci.package_name = dataGridView2.Rows[rowinx].Cells[5].Value.ToString();
                pci.sign_person = dataGridView2.Rows[rowinx].Cells[6].Value.ToString();
                pci.sign_emp = dataGridView2.Rows[rowinx].Cells[7].Value.ToString();
                pci.sign_manager = dataGridView2.Rows[rowinx].Cells[8].Value.ToString();
                pci.status = dataGridView2.Rows[rowinx].Cells[9].Value.ToString();
                pci.use_package = dataGridView2.Rows[rowinx].Cells[10].Value.ToString();
                pci.pk_use = dataGridView2.Rows[rowinx].Cells[11].Value.ToString();
                pci.num_use = dataGridView2.Rows[rowinx].Cells[13].Value.ToString();
                pci.package_unit = dataGridView2.Rows[rowinx].Cells[14].Value.ToString();
                pci.trainer_code = dataGridView2.Rows[rowinx].Cells[15].Value.ToString();
                pci.PERSON_SEX = dataGridView2.Rows[rowinx].Cells[16].Value.ToString();
                pci.id = id_pc;
                pci.package_person_id = dataGridView2.Rows[rowinx].Cells[18].Value.ToString();
                pci.trainerCode = dataGridView2.Rows[rowinx].Cells[19].Value.ToString();
                pci.managerCode = dataGridView2.Rows[rowinx].Cells[20].Value.ToString();
                string Name = dataGridView2.Rows[rowinx].Cells[21].Value.ToString();
                string Lname = dataGridView2.Rows[rowinx].Cells[22].Value.ToString();



                if (dataGridView2.Columns["ยกเลิก"] != null && e.ColumnIndex == dataGridView2.Columns["ยกเลิก"].Index && !pci.sign_person.ToLower().Equals("true"))
                {
                    frm_dialog d = new frm_dialog("ยืนยันการลบหักครั้งการเทรน ! \r\n ชื่อ : " + pci.PERSON_NAME, "");
                    d.ShowDialog();
                    if (d.DialogResult == DialogResult.OK)
                    {
                        var thread1 = new Thread(showloading);
                        thread1.Start();

                        typeActive = "DEL";
                        Response res = await ApiProcess.manageCheckin(typeActive, pci.id, pci.PERSON_CODE, pci.package_person_id, "", "", "", "", "", "", "");
                        if (res.status)
                        {
                            setPersonCheckin("'CI'", "'PT'", "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: dataGridView1_CellClick ::" + ex.ToString(), "frm_history_inv");
            }
            finally
            {
                picLoading.Visible = false;
            }
        }

        private void frm_trainer_Shown(object sender, EventArgs e)
        {
            ClearSelection();
        }

        private void ClearSelection()
        {
            dataGridView2.ClearSelection();
            id_pc = "";
            labCode.Text = "";
            labName.Text = "";
            labLname.Text = "";
            labStatus.Text = "";
            txtNote.Text = "";

            picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
            picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            picPerson.Size = new System.Drawing.Size(148, 178);

            pc = new PersonCheckin();
        }
    }
}
