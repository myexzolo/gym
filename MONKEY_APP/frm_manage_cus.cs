﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThaiNationalIDCard;

namespace MONKEY_APP
{
    public partial class frm_manage_cus : Form
    {
        private ThaiIDCard idcard;
        public string typeActive;

        private string picCus = "";

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        public frm_manage_cus(string typeActive)
        {
            InitializeComponent();
            this.typeActive = typeActive;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void xx_Click(object sender, EventArgs e)
        {

        }

        private void txtaddrProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                textaddrAmphur.Items.Clear();
                textaddrTambol.Items.Clear();
                txtZip.Text = "";
                textaddrAmphur.Text = "";
                textaddrTambol.Text = "";

                ComboBox cmb = (ComboBox)sender;
                int selectedIndex = cmb.SelectedIndex;
                string selectedValue = (string)cmb.SelectedValue;

                Item item = (Item)cmb.SelectedItem;

                for (int x = 0; x < Global.DistrictList.Count; x++)
                {
                    District data = Global.DistrictList[x];
                    if (data.PROVINCE_CODE.Equals(item.Value))
                    {
                        textaddrAmphur.Items.Add(new Item(data.DISTRICT_NAME, data.DISTRICT_CODE));
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: txtaddrProvince_SelectedIndexChanged ::" + ex.ToString(), "frm_manage_cus");
            }

        }



        private async void frm_manage_cus_Load(object sender, EventArgs e)
        {
            LoadMaster();
            LoadData();
            idcard = new ThaiIDCard();

            


            for (int x=1; x <= 31; x++)
            {
                comboDate.Items.Add(new Item(x.ToString("00"), x.ToString("00")));
            }

            for (int monthNo = 1; monthNo <= 12; monthNo++)
            {
                var bar = new DateTime(DateTime.Now.Year, monthNo, 1);
                comboMonth.Items.Add(new Item(bar.ToString("MMM", new CultureInfo("th-TH")), monthNo.ToString("00")));
            }

            int ey = int.Parse(DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"))) - 5;
            int sy = (ey - 90);
            for (int x = sy; x < ey; x++)
            {
                int yearEn = (x - 543);
                comboYear.Items.Add(new Item(x.ToString(), yearEn.ToString("0000")));
            }

        }

        private async void LoadData()
        {
            var thread1 = new Thread(showloading);
            try
            {
               

                ApiRest.InitailizeClient();

                if (Global.ProvinceList == null)
                {
                    
                    Global.ProvinceList = await ApiProcess.getProvince();
                }

                if (Global.DistrictList == null)
                {
                    Global.DistrictList = await ApiProcess.getDistrict();
                }

                if (Global.SubdistrictList == null)
                {
                    thread1.Start();
                    Global.SubdistrictList = await ApiProcess.getSubdistrict();
                }

                for (int x = 0; x < Global.ProvinceList.Count; x++)
                {
                    Province data = Global.ProvinceList[x];
                    txtaddrProvince.Items.Add(new Item(data.PROVINCE_NAME, data.PROVINCE_CODE));
                    txtaddrProvince2.Items.Add(new Item(data.PROVINCE_NAME, data.PROVINCE_CODE));
                }

                if (Global.EmployeeList == null)
                {
                    Global.EmployeeList = await ApiProcess.getEmployee("ACTIVE");

                }
                comboEmp.Items.Add(new Item("", ""));
                for (int x = 0; x < Global.EmployeeList.Count; x++)
                {
                    Employee emp = Global.EmployeeList[x];
                    comboEmp.Items.Add(new Item(emp.EMP_NAME + " " + emp.EMP_LASTNAME, emp.EMP_CODE));
                }

                nameUser.Text = Global.USER.user_name + " " + Global.USER.user_last;
                user_code.Text = Global.USER.user_login;

                if (typeActive.Equals("ADD"))
                {
                    
                    //genCusCode();
                    DateTime dob = DateTime.Now;

                    comboDate.Text = dob.ToString("dd", new CultureInfo("th-TH"));
                    comboMonth.Text = dob.ToString("MMM", new CultureInfo("th-TH"));
                    comboYear.Text = dob.ToString("yyyy", new CultureInfo("th-TH"));
                }
                else if (typeActive.Equals("EDIT"))
                {
                    label1.Text = "แก้ไขทะเบียนสมาชิก";
                    getCustomerById(Global.PERSON.PERSON_CODE);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                thread1.Abort();
                if (picLoading.InvokeRequired)
                {
                    picLoading.Invoke(new MethodInvoker(delegate
                    {
                        picLoading.Visible = false;
                    }));
                }
                else
                {
                    picLoading.Visible = false;
                }
            }
        }

        private void LoadMaster()
        {  

            for (int x = 0; x < Global.MEMBER_GROUP_AGE.Count; x++)
            {
                MASTER master = Global.MEMBER_GROUP_AGE[x];
                comboGroupAge.Items.Add(new Item(master.DATA_DESC1, master.DATA_CODE));
            }

            for (int x = 0; x < Global.MEMBER_GROUP.Count; x++)
            {
                MASTER master = Global.MEMBER_GROUP[x];
                comboGroup.Items.Add(new Item(master.DATA_DESC1, master.DATA_CODE));
            }

            comboGroup.SelectedIndex = 0;

            for (int x = 0; x < Global.SEX.Count; x++)
            {
                MASTER master = Global.SEX[x];
                comboSex.Items.Add(new Item(master.DATA_DESC1, master.DATA_CODE));
            }

            for (int x = 0; x < Global.PRENAME.Count; x++)
            {
                MASTER master = Global.PRENAME[x];
                comboTitle.Items.Add(new Item(master.DATA_DESC1, master.DATA_CODE));
            }
        }

        private async void genCusCode()
        {

            try
            {
                ApiRest.InitailizeClient();
                Response res = await ApiProcess.genCusCode(Global.BRANCH.branch_code, Global.BRANCH.branch_id);
                cusCode.Text = res.message;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                if (picLoading.InvokeRequired)
                {
                    picLoading.Invoke(new MethodInvoker(delegate
                    {
                        picLoading.Visible = false;
                    }));
                }
                else
                {
                    picLoading.Visible = false;
                }
            }
        }

        private async void getCustomerById(string cus_code)
        {
            try
            {
                ApiRest.InitailizeClient();
                Person person = await ApiProcess.getPersonById(Global.BRANCH.branch_code, cus_code);

                cusCode.Text = person.PERSON_CODE;
                comboTitle.Text = person.PERSON_TITLE;
                txtName.Text = person.PERSON_NAME;
                txtLname.Text = person.PERSON_LASTNAME;
                comboSex.Text   = person.PERSON_SEX;
                textEmail.Text = person.PERSON_EMAIL;
                string dth = Utils.getDateEntoTh(person.PERSON_BIRTH_DATE);

                DateTime dob = Convert.ToDateTime(person.PERSON_BIRTH_DATE);

                int y = int.Parse(dob.ToString("yyyy", new CultureInfo("th-TH")));

                if (y > 3000) {
                    y -= 543;
                }

                comboDate.Text = dob.ToString("dd", new CultureInfo("th-TH"));
                comboMonth.Text = dob.ToString("MMM", new CultureInfo("th-TH"));
                comboYear.Text = y.ToString();

                txtNote.Text    = person.PERSON_NOTE;
                txtNickName.Text = person.PERSON_NICKNAME;
                txtId.Text = person.PERSON_CARD_ID;
                txtAddress1.Text = person.PERSON_HOME1_ADDR1;
                txtAddress2.Text = person.PERSON_HOME1_ADDR2;
                txtaddrProvince.Text = person.PERSON_HOME1_PROVINCE;
                textaddrAmphur.Text = person.PERSON_HOME1_DISTRICT;
                textaddrTambol.Text = person.PERSON_HOME1_SUBDISTRICT;
                txtZip.Text = person.PERSON_HOME1_POSTAL;
                textTel.Text = person.PERSON_TEL_MOBILE;
                textTel2.Text = person.PERSON_TEL_MOBILE2;

                txtBillName.Text = person.PERSON_BILL_NAME;
                txtTexNo.Text = person.PERSON_BILL_TAXNO;
                textBillAdd.Text = person.PERSON_BILL_ADDR1;
                txtaddrProvince2.Text  = person.PERSON_BILL_PROVINCE;
                textaddrAmphur2.Text    = person.PERSON_BILL_DISTRICT;
                textaddrTambol2.Text    = person.PERSON_BILL_SUBDISTRICT;
                txtZip2.Text  = person.PERSON_BILL_POSTAL;
                txtFax.Text = person.PERSON_FAX;
                txtTelOffice.Text = person.PERSON_TEL_OFFICE;


                for (int x = 0; x < Global.MEMBER_GROUP.Count; x++)
                {
                    MASTER master = Global.MEMBER_GROUP[x];
                    if (person.PERSON_GROUP == master.DATA_CODE)
                    {
                        comboGroup.SelectedIndex = x;
                    }
                }

                for (int x = 0; x < Global.MEMBER_GROUP_AGE.Count; x++)
                {
                    MASTER master = Global.MEMBER_GROUP_AGE[x];
                    if (person.PERSON_GROUP_AGE == master.DATA_CODE)
                    {
                        comboGroupAge.SelectedIndex = x;
                    }
                }

                for (int x = 0; x < Global.EmployeeList.Count; x++)
                {
                    Employee emp = Global.EmployeeList[x];
                    if (person.EMP_CODE_SALE != null && person.EMP_CODE_SALE == emp.EMP_CODE)
                    {
                        comboEmp.SelectedIndex = (x + 1);
                    }
                }

                if (!person.PERSON_IMAGE.Equals(""))
                {
                    Utils.setImage(person.PERSON_IMAGE, pictureBox2);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                if (picLoading.InvokeRequired)
                {
                    picLoading.Invoke(new MethodInvoker(delegate
                    {
                        picLoading.Visible = false;
                    }));
                }
                else
                {
                    picLoading.Visible = false;
                }
            }

        }



        private void button1_Click(object sender, EventArgs e)
        {
            Refresh();

            var thread1 = new Thread(readCard);
            thread1.Start();
        }

        private void showloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = true;
                }));
            }
            else {
                picLoading.Visible = true;
            }
            
        }

        private void readCard()
        {
            try
            {
                showloading();
                string[] readers = idcard.GetReaders();

                Personal personal = idcard.readAllPhoto();
                if (personal != null)
                {
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            txtName.Text = personal.Th_Firstname;
                            txtLname.Text = personal.Th_Lastname;
                            txtId.Text = personal.Citizenid;
                            pictureBox2.BackgroundImage = personal.PhotoBitmap;

                            byte[] imgArray = ImageToByte(personal.PhotoBitmap);
                            picCus = Convert.ToBase64String(imgArray);

                            txtAddress1.Text = personal.addrHouseNo + " " + personal.addrVillageNo;
                            txtAddress2.Text = personal.addrLane + " " + personal.addrRoad;
                            txtaddrProvince.Text = personal.addrProvince;
                            textaddrAmphur.Text = personal.addrAmphur;
                            textaddrTambol.Text = personal.addrTambol;


                            comboSex.SelectedIndex = Int32.Parse(personal.Sex); 



                            comboTitle.Text = personal.Th_Prefix;
                            //bod.Text = personal.Birthday.ToLongDateString();

                            comboDate.Text = personal.Birthday.ToString("dd", new CultureInfo("th-TH"));
                            comboMonth.Text = personal.Birthday.ToString("MMM", new CultureInfo("th-TH"));
                            comboYear.Text = personal.Birthday.ToString("yyyy", new CultureInfo("th-TH"));

                            var today = DateTime.Today;
                            var age = today.Year - personal.Birthday.Year;

                            if (personal.Birthday > today.AddYears(-age)) age--;

                            if (age >= 10 && age <= 20) {
                                comboGroupAge.SelectedIndex = 1;
                            }
                            else if (age >= 21 && age <= 40)
                            {
                                comboGroupAge.SelectedIndex = 2;
                            }
                            else if (age >= 41 && age <= 60)
                            {
                                comboGroupAge.SelectedIndex = 3;
                            }
                            else if (age >= 61 && age <= 80)
                            {
                                comboGroupAge.SelectedIndex = 4;
                            }
                            else if (age >= 81)
                            {
                                comboGroupAge.SelectedIndex = 5;
                            }

                            picLoading.Visible = false;
                        }));
                    }
                    else
                    {
                        txtName.Text = personal.Th_Firstname;
                        txtLname.Text = personal.Th_Lastname;
                        txtId.Text = personal.Citizenid;
                        pictureBox2.BackgroundImage = personal.PhotoBitmap;

                        txtAddress1.Text = personal.addrHouseNo + " " + personal.addrVillageNo;
                        txtAddress2.Text = personal.addrLane + " " + personal.addrRoad;
                        txtaddrProvince.Text = personal.addrProvince;
                        textaddrAmphur.Text = personal.addrAmphur;
                        textaddrTambol.Text = personal.addrTambol;

                        picLoading.Visible = false;
                    }

                }
                else if (idcard.ErrorCode() > 0)
                {
                    //MessageBox.Show(idcard.Error() + " " + idcard.ErrorCode());
                    if (idcard.ErrorCode() == 256)
                    {
                        MessageBox.Show("ไม่พบบัตร Smartcard");
                    }
                    else
                    {
                        MessageBox.Show("ไม่สามารถอ่านบัตร Smartcard ได้");
                    }
                    
                }
                else
                {
                    MessageBox.Show("Catch all");
                }
            }
            catch
            {
                MessageBox.Show("ไม่พบเครื่องอ่านบัตร Smartcard");
            }
            finally {
                if (picLoading.InvokeRequired)
                {
                    picLoading.Invoke(new MethodInvoker(delegate
                    {
                        picLoading.Visible = false;
                    }));
                }
                else
                {
                    picLoading.Visible = false;
                }
            }
        }

        public static byte[] ImageToByte(Image img)
        {
            byte[] byteArr;
            try
            {
                ImageConverter converter = new ImageConverter();
                var i2 = new Bitmap(img);
                byteArr = (byte[])converter.ConvertTo(i2, typeof(byte[]));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
            return byteArr;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textaddrAmphur_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                textaddrTambol.Items.Clear();
                txtZip.Text = "";
                textaddrTambol.Text = "";

                ComboBox cmb = (ComboBox)sender;
                int selectedIndex = cmb.SelectedIndex;
                string selectedValue = (string)cmb.SelectedValue;

                Item item = (Item)cmb.SelectedItem;

                for (int x = 0; x < Global.SubdistrictList.Count; x++)
                {
                    Subdistrict data = Global.SubdistrictList[x];
                    if (data.DISTRICT_CODE.Equals(item.Value))
                    {
                        textaddrTambol.Items.Add(new Item(data.SUBDISTRICT_NAME, data.SUBDISTRICT_POSTAL));
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: textaddrAmphur_SelectedIndexChanged ::" + ex.ToString(), "frm_manage_cus");
            }
        }

        private void textaddrAmphur2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                textaddrTambol2.Items.Clear();
                txtZip2.Text = "";
                textaddrTambol2.Text = "";

                ComboBox cmb = (ComboBox)sender;
                int selectedIndex = cmb.SelectedIndex;
                string selectedValue = (string)cmb.SelectedValue;

                Item item = (Item)cmb.SelectedItem;

                for (int x = 0; x < Global.SubdistrictList.Count; x++)
                {
                    Subdistrict data = Global.SubdistrictList[x];
                    if (data.DISTRICT_CODE.Equals(item.Value))
                    {
                        textaddrTambol2.Items.Add(new Item(data.SUBDISTRICT_NAME, data.SUBDISTRICT_POSTAL));
                    }
                }
            }
            catch (Exception z)
            {
                Console.WriteLine(z);
            }
        }

        private void textaddrTambol_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtZip.Text = "";

                ComboBox cmb = (ComboBox)sender;
                int selectedIndex = cmb.SelectedIndex;
                string selectedValue = (string)cmb.SelectedValue;

                Item item = (Item)cmb.SelectedItem;

                txtZip.Text = item.Value;

            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: textaddrTambol_SelectedIndexChanged ::" + ex.ToString(), "frm_manage_cus");
            }
        }

        private void textaddrTambol2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtZip2.Text = "";

                ComboBox cmb = (ComboBox)sender;
                int selectedIndex = cmb.SelectedIndex;
                string selectedValue = (string)cmb.SelectedValue;

                Item item = (Item)cmb.SelectedItem;

                txtZip2.Text = item.Value;

            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: textaddrTambol2_SelectedIndexChanged ::" + ex.ToString(), "frm_manage_cus");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "Image files(*.jpg, *.jpeg, *.jpe, *.jfif, *.png)|*.jpg; *.jpeg; *.jpe; *.jfif; *.png";

                if (dialog.ShowDialog() == DialogResult.OK) {
                    Bitmap imgUser = new Bitmap(dialog.FileName);
                    pictureBox2.BackgroundImage = imgUser;

                    Bitmap imgUsertmp = new Bitmap(imgUser,148, 178);
                    byte[] imgArray = ImageToByte(imgUsertmp);
                    picCus = Convert.ToBase64String(imgArray);

                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: button2_Click ::" + ex.ToString(), "frm_manage_cus");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void setAlertMessage(String str)
        {
            if (this.InvokeRequired)
            {

                this.Invoke(new MethodInvoker(delegate
                {
                    alertTxt.Visible = true;
                    alertTxt.Text = str;
                }));
            }
            else
            {
                alertTxt.Visible = true;
                alertTxt.Text = str;
            }
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            try
            {
                var thread1 = new Thread(showloading);

                alertTxt.Text = "";
                alertTxt.Visible = false;
                this.DialogResult = DialogResult.None;

                if (checkBox1.Checked && cusCode.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอก รหัสสมาชิก **");
                    cusCode.Focus();
                }
                else if (comboTitle.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ คำนำหน้าชื่อ **");
                    comboTitle.Focus();
                }
                else if (txtName.Text.Trim().Equals(""))
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกชื่อ **");
                    txtName.Focus();
                }
                else if (txtLname.Text.Trim().Equals(""))
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกนามสกุล **");
                    txtLname.Focus();
                }
                else if (comboGroup.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ กลุ่มสมาชิก **");
                    comboGroup.Focus();
                }
                else if (txtNickName.Text.Trim().Equals(""))
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกชื่อเล่น **");
                    txtNickName.Focus();
                }
                else if (comboSex.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ เพศ **");
                    comboSex.Focus();
                }
                else if (txtId.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกเลขที่บัตรประชาชน **");
                    txtId.Focus();
                }
                else if (txtAddress1.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกที่อยู่ **");
                    txtAddress1.Focus();
                }
                else if (txtaddrProvince.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุจังหวัด **");
                    txtaddrProvince.Focus();
                }
                else if (textaddrAmphur.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุเขต/อำเภอ **");
                    textaddrAmphur.Focus();
                }
                else if (textaddrTambol.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุแขวง/ตำบล **");
                    textaddrTambol.Focus();
                }
                else if (textTel.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกเบอร์โทร **");
                    textTel.Focus();
                }
                else if (comboEmp.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุผู้ดูแล **");
                    comboEmp.Focus();
                }
                else
                {
                    try
                    {
                        if (typeActive.Equals("ADD") && !checkBox1.Checked) {
                            ApiRest.InitailizeClient();
                            Response ress = await ApiProcess.genCusCode(Global.BRANCH.branch_code, Global.BRANCH.branch_id);
                            cusCode.Text = ress.message;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                    Person person = new Person();
                    thread1.Start();
                    person.COMPANY_CODE     = Global.BRANCH.branch_code;
                    person.PERSON_CODE      = cusCode.Text;
                    person.PERSON_CODE_INT  = cusCode.Text;
                    person.PERSON_TITLE     = comboTitle.Text;
                    person.PERSON_NAME      = txtName.Text;
                    person.PERSON_LASTNAME  = txtLname.Text;
                    person.PERSON_SEX       = comboSex.Text;
                    person.PERSON_ER_TEL    = textTel2.Text;


                    //string y = (comboYear.SelectedItem as Item).Value.ToString();
                    //string m = (comboMonth.SelectedItem as Item).Value.ToString();
                    //string d = (comboDate.SelectedItem as Item).Value.ToString();

                    //string bod = y + "-" + m + "-" + d;
                    person.PERSON_BIRTH_DATE = dateIncome.Text;


                    person.PERSON_NOTE = txtNote.Text;
                    person.PERSON_GROUP = (comboGroup.SelectedItem as Item).Value.ToString();
                    if (!comboGroupAge.Text.Trim().Equals(""))
                    {
                        person.PERSON_GROUP_AGE = (comboGroupAge.SelectedItem as Item).Value.ToString();
                    }

                    person.PERSON_NICKNAME  = txtNickName.Text;
                    person.PERSON_CARD_ID   = txtId.Text;
                    person.PERSON_HOME1_ADDR1 = txtAddress1.Text;
                    person.PERSON_HOME1_ADDR2 = txtAddress2.Text;
                    person.PERSON_HOME1_SUBDISTRICT = (textaddrTambol.SelectedItem as Item).Name.ToString();
                    person.PERSON_HOME1_DISTRICT    = (textaddrAmphur.SelectedItem as Item).Name.ToString();
                    person.PERSON_HOME1_PROVINCE    = (txtaddrProvince.SelectedItem as Item).Name.ToString();
                    person.EMP_CODE_SALE            = (comboEmp.SelectedItem as Item).Value.ToString();
                    person.PERSON_HOME1_POSTAL      = txtZip.Text;
                    person.PERSON_TEL_MOBILE        = textTel.Text;
                    person.PERSON_TEL_MOBILE2       = textTel2.Text;
                    person.PERSON_EMAIL             = textEmail.Text;

                        
                    person.PERSON_BILL_NAME         = txtBillName.Text;
                    person.PERSON_BILL_TAXNO        = txtTexNo.Text;
                    person.PERSON_BILL_ADDR1        = textBillAdd.Text;
                    person.PERSON_BILL_SUBDISTRICT  = textaddrTambol2.SelectedItem != null?(textaddrTambol2.SelectedItem as Item).Name.ToString():"";
                    person.PERSON_BILL_DISTRICT     = textaddrAmphur2.SelectedItem != null?(textaddrAmphur2.SelectedItem as Item).Name.ToString():"";
                    person.PERSON_BILL_PROVINCE     = txtaddrProvince2.SelectedItem != null?(txtaddrProvince2.SelectedItem as Item).Name.ToString():"";
                    person.PERSON_BILL_POSTAL       = txtZip2.Text;
                    person.PERSON_FAX               = txtFax.Text;
                    person.PERSON_TEL_OFFICE        = txtTelOffice.Text;

                    person.PERSON_IMAGE = picCus;

                    try
                    {
                        ApiRest.InitailizeClient();
                        Response res = await ApiProcess.managePerson(Global.BRANCH.branch_code, typeActive, person, Global.USER.user_login);
                        if (res.status)
                        {
                            this.DialogResult = DialogResult.OK;
                        }
                    }
                    catch(Exception ex)
                    {
                        picLoading.Visible = false;
                        alertTxt.Visible = true;
                        alertTxt.Text = "** ไม่สามารถเชื่อมต่อ Server ได้ **";
                        Console.WriteLine(ex);
                        Utils.getErrorToLog(":: managePerson ::" + ex.ToString(), "frm_manage_cus");
                    }
                }
            }
            catch (Exception ex)
            {
                picLoading.Visible = false;
                alertTxt.Visible = true;
                alertTxt.Text = "** ระบบผิดพลาด **";
                picLoading.Visible = false;
                Console.WriteLine(ex);
                Utils.getErrorToLog(":: button4_Click ::" + ex.ToString(), "frm_manage_cus");
            }
            finally
            {
                picLoading.Visible = false;
            }
        }

        private void txtaddrProvince2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                textaddrAmphur2.Items.Clear();
                textaddrTambol2.Items.Clear();
                txtZip2.Text = "";
                textaddrAmphur2.Text = "";
                textaddrTambol2.Text = "";

                ComboBox cmb = (ComboBox)sender;
                int selectedIndex = cmb.SelectedIndex;
                string selectedValue = (string)cmb.SelectedValue;

                Item item = (Item)cmb.SelectedItem;

                for (int x = 0; x < Global.DistrictList.Count; x++)
                {
                    District data = Global.DistrictList[x];
                    if (data.PROVINCE_CODE.Equals(item.Value))
                    {
                        textaddrAmphur2.Items.Add(new Item(data.DISTRICT_NAME, data.DISTRICT_CODE));
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: txtaddrProvince2_SelectedIndexChanged ::" + ex.ToString(), "frm_manage_cus");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtBillName.Text = comboTitle.Text + txtName.Text + "  " + txtLname.Text;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBillAdd.Text        = txtAddress1.Text + " "+ txtAddress2.Text;
            txtaddrProvince2.Text   = txtaddrProvince.Text;
            textaddrAmphur2.Text    = textaddrAmphur.Text;
            textaddrTambol2.Text    = textaddrTambol.Text;
            txtZip2.Text            = txtZip.Text;
        }

        private void comboTitle_SelectedValueChanged(object sender, EventArgs e)
        {
            //comboTitle.SelectedItem
            //Console.WriteLine(comboTitle.SelectedValue);
        }

        private void dateIncome_ValueChanged(object sender, EventArgs e)
        {
            DateTime dN = Convert.ToDateTime(dateIncome.Value.ToString());
            comboDate.Text = dN.ToString("dd");
            comboMonth.Text = dN.ToString("MMM", new CultureInfo("th-TH"));
            int yy = int.Parse(dN.ToString("yyyy", new CultureInfo("th-TH")));

            if (yy > 3000)
            {
                yy -= 543;
            }

            comboYear.Text = yy.ToString("0000");


        }

        private void changeDateIn(object sender, EventArgs e)
        {
           try
            {
                DateTime dN = Convert.ToDateTime(dateIncome.Value.ToString());
                int yy          = int.Parse(dN.ToString("yyyy"));
                string mm       = dN.ToString("MM");
                string dd       = dN.ToString("dd");

                if (comboDate.SelectedItem != null)
                {
                    dd = (comboDate.SelectedItem as Item).Value.ToString();
                }
                if (comboMonth.SelectedItem != null)
                {
                    mm = (comboMonth.SelectedItem as Item).Value.ToString();
                }

                if (comboYear.SelectedItem != null)
                {
                    yy = int.Parse(comboYear.Text);

                    if (yy > 2400)
                    {
                        yy -= 543;
                    }
                }

                 string D = yy.ToString("0000") + "/" + mm + "/" + dd;
                 dateIncome.Text = D;     
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

        private void comboDate_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                cusCode.BackColor   = System.Drawing.SystemColors.Window;
                cusCode.Text        = "";
                cusCode.ReadOnly    = false;
            }
            else
            {
                cusCode.BackColor = System.Drawing.SystemColors.AppWorkspace;
                cusCode.Text = "";
                cusCode.ReadOnly = true;
            }
            
        }
    }
}
