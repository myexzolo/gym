﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThaiNationalIDCard;

namespace MONKEY_APP
{
    public partial class frm_manage_checkin : Form
    {
        public string typeActive;

        string picCus = "";

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        public frm_manage_checkin()
        {
            InitializeComponent();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }




        private void button1_Click(object sender, EventArgs e)
        {
            Refresh();

            var thread1 = new Thread(showloading);
            thread1.Start();
        }

        private void showloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = true;
                }));
            }
            else
            {
                picLoading.Visible = true;
            }

        }



        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }



        private void frm_manage_package_Load(object sender, EventArgs e)
        {
            var thread1 = new Thread(showloading);
            thread1.Start();

            button4.Enabled = false;

            if (Global.PERSON.PERSON_IMAGE != null && !Global.PERSON.PERSON_IMAGE.Equals(""))
            {
                picCus = Global.PERSON.PERSON_IMAGE;
                Utils.setImage(picCus, picPerson);
            }
            else
            {
                picCus = "";
                picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
                picPerson.Size = new System.Drawing.Size(148, 178);
            }

            labName.Text = Global.PERSON.PERSON_TITLE + Global.PERSON.PERSON_NAME + " " + Global.PERSON.PERSON_LASTNAME;
            labCode.Text = Global.PERSON.PERSON_CODE;
            labNickname.Text = Global.PERSON.PERSON_NICKNAME;
            labDstart.Text = Global.PERSON.PERSON_REGISTER_DATE;
            labDend.Text = Global.PERSON.PERSON_EXPIRE_DATE;
            labStatus.Text = Global.PERSON.PERSON_STATUS;

            setPersonPackage(Global.PERSON.PERSON_CODE);
        }

        private async void setPersonPackage(string personCode)
        {

            dataGridView2.Rows.Clear();
            try
            {
                ApiRest.InitailizeClient();
                var psList = await ApiProcess.getPackageByPersonCode(personCode);

                int mb = 0;
                int pt = 0;

                DateTime dDate;

                for (var i = 0; i < psList.Count; i++)
                {
                    PackagePerson ps = psList[i];

                    //string dstart = Utils.getDateEntoTh(ps.date_start);
                    //string dexp = Utils.getDateEntoTh(ps.date_expire);

                    string dstart = "";
                    string dexp = "";

                    if (ps.num_checkin >= ps.num_use && ps.package_unit.Equals("TIMES")) {
                        continue;
                    }

                    if (DateTime.TryParse(ps.date_start, out dDate))
                    {

                        DateTime ds = Convert.ToDateTime(ps.date_start);
                        string dm = ds.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(ds.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }

                        dstart = dm + "/" + yy.ToString();
                    }

                    if (DateTime.TryParse(ps.date_expire, out dDate))
                    {
                        DateTime de = Convert.ToDateTime(ps.date_expire);
                        string dm = de.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(de.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }
                        dexp = dm + "/" + yy.ToString();
                    }
                    //string dexpP    = Utils.getDateEntoTh(ps.date_expire_package);

                    bool chk = true;

                    if (ps.type_package.Equals("MB"))
                    {
                        if (mb > 0)
                        {
                            chk = false;
                        }
                        mb++;
                    }

                    if (ps.type_package.Equals("PT"))
                    {
                        if (pt > 0)
                        {
                            chk = false;
                        }
                        pt++;
                    }

                    if (ps.use_package > 0) {
                        dataGridView2.Rows.Add(chk, ps.package_name, dstart, dexp,
                        ps.use_package + "/" + ps.num_use, ps.package_unit, ps.reg_no, ps.invoice_code, ps.status, ps.package_detail, ps.invoice_date, ps.package_code, ps.id);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: setPersonPackage ::" + ex.ToString(), "frm_manage_checkin");

            }
            finally
            {
                picLoading.Visible = false;
                button4.Enabled = true;
            }


        }

        private async void button4_Click(object sender, EventArgs e)
        {
            var thread1 = new Thread(showloading);
            thread1.Start();
            try
            {
                int num = dataGridView2.Rows.Count;
                bool flag = true;

                for (int i = 0; i < num; i++)
                {

                    bool chk = (bool)dataGridView2.Rows[i].Cells[0].Value;
                    string package_person_id = dataGridView2.Rows[i].Cells[12].Value.ToString();

                    if (chk)
                    {
                        ApiRest.InitailizeClient();
                        Response res = await ApiProcess.manageCheckin("ADD", "", labCode.Text, package_person_id,"","","","","","","");
                        if (!res.status)
                        {
                            flag = false;
                            break;
                        }
                    }
                }

                if (flag)
                {
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    this.DialogResult = DialogResult.None;
                }

            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: setPersonPackage ::" + ex.ToString(), "button4_Click");
            }
            finally {
                picLoading.Visible = false;
            }
            

        }
    }

}
