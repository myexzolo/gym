﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThaiNationalIDCard;

namespace MONKEY_APP
{
    public partial class frm_checkout_class : Form
    {
        private DataTable dt;
        private DataTable dtm;
        public ScheduleClass sc;
        public string active = "";
        private string picCus = "";

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        public frm_checkout_class(ScheduleClass sc)
        {
            this.sc = sc;
            InitializeComponent();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }



        private void frm_manage_cus_Load(object sender, EventArgs e)
        {
            txtClassName.Text = sc.name_class;
            txtDate.Text = sc.date_class_txt;
            txtTime.Text = sc.time_start + " - " + sc.time_end;
            txtNickEmp.Text = sc.EMP_NICKNAME;
            trainerCode.Text = sc.EMP_CODE;

            if (!sc.image_class.Equals(""))
            {
                Utils.setImage(sc.image_class, picClass);
            }

            if (sc.sign_emp.Equals("Y"))
            {
                txtStatus.Text = "บันทึกแล้ว";
                button2.Enabled = false;
                button2.ForeColor = System.Drawing.Color.Gray;
                button2.Image = global::MONKEY_APP.Properties.Resources.security11;
                button1.Enabled = false;
                button1.ForeColor = System.Drawing.Color.Gray;
            }
            else if (sc.sign_emp.Equals("C"))
            {
                button2.Enabled = false;
                button2.ForeColor = System.Drawing.Color.Gray;
                button2.Image = global::MONKEY_APP.Properties.Resources.security11;
                button1.Enabled = false;
                button1.ForeColor = System.Drawing.Color.Gray;
            }
            else
            {
                txtStatus.Text = "รอการบันทึก";
                button2.Enabled = true;
                button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                button2.Image = global::MONKEY_APP.Properties.Resources.security1;
            }
             
            reserveList(sc.id);
        }

        public static byte[] ImageToByte(Image img)
        {
            byte[] byteArr;
            try
            {
                ImageConverter converter = new ImageConverter();
                var i2 = new Bitmap(img);
                byteArr = (byte[])converter.ConvertTo(i2, typeof(byte[]));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
            return byteArr;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        

        private void frm_reserve_class_Shown(object sender, EventArgs e)
        {

        }



        private async void reserveList(string schedule_day_id)
        {
            try
            {
                List<ReserveClass> reserveClassList = await ApiProcess.getReserveClassList(schedule_day_id);

                Bitmap Image    = MONKEY_APP.Properties.Resources.printer_tool30;
                Bitmap Image2   = MONKEY_APP.Properties.Resources.error30;
                Bitmap Image3   = MONKEY_APP.Properties.Resources.error30_d;
                Bitmap checkin  = MONKEY_APP.Properties.Resources.point;

                if (reserveClassList == null || reserveClassList.Count() == 0) {
                    if (button2.Enabled) {
                        button1.Enabled = true;
                        button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                    }
                }

                byte[] cancelImage = ImageToByte(Image2);

                string status;
                byte[] printImage;

                dtm = new DataTable();
                dtm.Columns.Add("   ", typeof(byte[]));
                dtm.Columns.Add("วันที่", typeof(string));
                dtm.Columns.Add("เวลา", typeof(string));
                dtm.Columns.Add("รายการ", typeof(string));
                dtm.Columns.Add("ชื่อ - สกุล", typeof(string));
                dtm.Columns.Add("ชื่อเล่น", typeof(string));
                dtm.Columns.Add("วันที่ทำรายการ", typeof(string));
                dtm.Columns.Add("หมดอายุ", typeof(string));
                dtm.Columns.Add("สถานะ", typeof(string));
                dtm.Columns.Add("reserve_id", typeof(string));
                dtm.Columns.Add("ยกเลิก", typeof(byte[]));
                dtm.Columns.Add("schedule_day_id", typeof(string));
                dtm.Columns.Add("date_class", typeof(string));
                dtm.Columns.Add("time_start", typeof(string));
                dtm.Columns.Add("time_end", typeof(string));
                dtm.Columns.Add("EMP_NICKNAME", typeof(string));
                dtm.Columns.Add("PERSON_CODE", typeof(string));
                dtm.Columns.Add("status", typeof(string));
                dtm.Columns.Add("unit", typeof(string));
                dtm.Columns.Add("person_join", typeof(string));


                if (reserveClassList != null)
                {
                    for (int i = 0; i < reserveClassList.Count(); i++)
                    {
                        ReserveClass rs = reserveClassList[i];
                        //if (Global.USER.EMP_IS_STAFF != null && Global.USER.EMP_IS_STAFF.Equals("Y"))
                        //{
                        //    cancelImage = ImageToByte(Image2);
                        //}                     
                        
                        if (rs.status.Equals("S"))
                        {
                            status = "จอง";
                            printImage = ImageToByte(checkin);
                        }
                        if (rs.status.Equals("E"))
                        {
                            status = "Expired";
                            printImage = ImageToByte(checkin);
                        }
                        else
                        {
                            printImage = ImageToByte(Image);
                            status = "Check In";
                        }

                        dtm.Rows.Add(
                            printImage, rs.date_class_txt, rs.time_start + " - " + rs.time_end, rs.name_class, rs.person_name, rs.PERSON_NICKNAME,
                            rs.date_reserve_txt, rs.expire_date_txt, status, rs.reserve_id, cancelImage, rs.schedule_day_id,
                            rs.date_class, rs.time_start, rs.time_end, rs.EMP_NICKNAME, rs.PERSON_CODE, rs.status, rs.unit, rs.person_join);

                    }
                }

                dataGridView2.DataSource = dtm;

                dataGridView2.Columns[0].Width = 70;
                dataGridView2.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[1].Width = 90;
                dataGridView2.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;


                dataGridView2.Columns[2].Width = 100;
                dataGridView2.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;


                dataGridView2.Columns[3].Width = 120;
                dataGridView2.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dataGridView2.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[4].Width = 200;
                dataGridView2.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dataGridView2.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[5].Width = 104;
                dataGridView2.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dataGridView2.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;


                dataGridView2.Columns[6].Width = 140;
                dataGridView2.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[7].Width = 140;
                dataGridView2.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[8].Width = 100;
                dataGridView2.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[0].Visible = false;
                dataGridView2.Columns[1].Visible = false;
                dataGridView2.Columns[2].Visible = false;
                dataGridView2.Columns[3].Visible = false;
                dataGridView2.Columns[9].Visible = false;
                dataGridView2.Columns[11].Visible = false;
                dataGridView2.Columns[12].Visible = false;
                dataGridView2.Columns[13].Visible = false;
                dataGridView2.Columns[14].Visible = false;
                dataGridView2.Columns[15].Visible = false;
                dataGridView2.Columns[16].Visible = false;
                dataGridView2.Columns[17].Visible = false;
                dataGridView2.Columns[18].Visible = false;
                dataGridView2.Columns[19].Visible = false;
                dataGridView2.Columns[10].Visible = false;

                dataGridView2.Columns[10].Width = 70;
                dataGridView2.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[10].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch
            {

            }
        }

        private async void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowindex = dataGridView2.CurrentCell.RowIndex;

                ReserveClass reserveClass = new ReserveClass();

                reserveClass.reserve_id = dataGridView2.Rows[rowindex].Cells[9].Value.ToString();
                reserveClass.schedule_day_id = dataGridView2.Rows[rowindex].Cells[11].Value.ToString();
                reserveClass.name_class = dataGridView2.Rows[rowindex].Cells[3].Value.ToString();
                reserveClass.date_class = dataGridView2.Rows[rowindex].Cells[12].Value.ToString();
                reserveClass.time_start = dataGridView2.Rows[rowindex].Cells[13].Value.ToString();
                reserveClass.time_end = dataGridView2.Rows[rowindex].Cells[14].Value.ToString();
                reserveClass.EMP_NICKNAME = dataGridView2.Rows[rowindex].Cells[15].Value.ToString();
                reserveClass.PERSON_CODE = dataGridView2.Rows[rowindex].Cells[16].Value.ToString();
                reserveClass.person_name = dataGridView2.Rows[rowindex].Cells[4].Value.ToString();
                reserveClass.status      = dataGridView2.Rows[rowindex].Cells[17].Value.ToString();
                reserveClass.unit         = int.Parse(dataGridView2.Rows[rowindex].Cells[18].Value.ToString());
                reserveClass.person_join  = int.Parse(dataGridView2.Rows[rowindex].Cells[19].Value.ToString());


                if (e.ColumnIndex == dataGridView2.Columns["   "].Index)
                {
                    picLoading.Visible = true;
                    if (reserveClass.status.Equals("S"))
                    {
                        ApiRest.InitailizeClient();
                        Response ress = await ApiProcess.managerReserveClass(Global.BRANCH.branch_code, "CHANGE", reserveClass);

                        Bitmap Image = MONKEY_APP.Properties.Resources.printer_tool30;

                        dataGridView2.Rows[rowindex].Cells[0].Value = ImageToByte(Image);
                        dataGridView2.Rows[rowindex].Cells[8].Value = "Check In";

                    }
                    Utils.eformReserveClass(reserveClass);

                }

                if (dataGridView2.Columns["ยกเลิก"] != null && e.ColumnIndex == dataGridView2.Columns["ยกเลิก"].Index)
                {
                    picLoading.Visible = true;
                    frm_dialog d = new frm_dialog("ยืนยันการลบรายการ ! \r\nสมาชิก : " + reserveClass.person_name, "");
                    d.ShowDialog();
                    if (d.DialogResult == DialogResult.OK)
                    {
                        ApiRest.InitailizeClient();
                        Response ress = await ApiProcess.managerReserveClass(Global.BRANCH.branch_code, "DEL", reserveClass);
                        if (ress.status)
                        {
                            dataGridView2.Rows.RemoveAt(rowindex);
                            active = "CHANGE";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: dataGridView1_CellClick ::" + ex.ToString(), "frm_history_inv");
            }
            finally
            {
                picLoading.Visible = false;
            }
        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private async void button2_Click(object sender, EventArgs e)
        {
            frm_fg_checkin fgc = new frm_fg_checkin(sc.EMP_CODE, sc.EMP_NICKNAME, "EMP");
            var result = fgc.ShowDialog();
            picLoading.Visible = false;
            if (result == DialogResult.OK)
            {
                ReserveClass reserveClass = new ReserveClass();
                reserveClass.schedule_day_id = sc.id;
                reserveClass.reserve_id = "";
                reserveClass.PERSON_CODE = "";
                reserveClass.status  = "";
                //reserveClass.expire_date  = "";

                ApiRest.InitailizeClient();
                Response ress = await ApiProcess.managerReserveClass(Global.BRANCH.branch_code, "EMP", reserveClass);

                txtStatus.Text = "บันทึกแล้ว";
                button2.Enabled = false;
                button2.ForeColor = System.Drawing.Color.Gray;
                button2.Image = global::MONKEY_APP.Properties.Resources.security11;

                this.DialogResult = DialogResult.OK;
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            frm_dialog d = new frm_dialog("ยืนยันการยกเลิก Class ! \r\n" + txtClassName.Text, "");
            d.ShowDialog();
            if (d.DialogResult == DialogResult.OK)
            {
                ReserveClass reserveClass = new ReserveClass();
                reserveClass.schedule_day_id = sc.id;
                reserveClass.reserve_id = "";
                reserveClass.PERSON_CODE = "";
                reserveClass.status = "";

                ApiRest.InitailizeClient();
                Response ress = await ApiProcess.managerReserveClass(Global.BRANCH.branch_code, "CANCEL", reserveClass);

                txtStatus.Text = "ยกเลิก Class";
                button2.Enabled = false;
                button2.ForeColor = System.Drawing.Color.Gray;
                button2.Image = global::MONKEY_APP.Properties.Resources.security11;

                button1.Enabled = false;
                button1.ForeColor = System.Drawing.Color.Gray;

                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
