﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class frm_fg_checkin_member : Form
    {
        object FRegTemplate;
        int FingerCount;
        int fpcHandle;
        string[] FFingerNames;
        short FMatchType;

        bool deviceOut;

        string typeSearch;
        string CODE;
        string NAME;
        string fingNumber = "";

        public string BeepStatus = System.Configuration.ConfigurationManager.AppSettings["Beep"];


        List<FingerScan> fingerScanList;

        public frm_fg_checkin_member(string code, string name, string typeSearch)
        {
            InitializeComponent();

            this.typeSearch = typeSearch;
            this.CODE = code;
            this.NAME = name;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void cmdInit_Click(object sender, EventArgs e)
        {
            ZKFPEngX1.SensorIndex = 0;
            //ทำการค้นหาหัวอ่านที่อยู่ในเครื่อง
            if (ZKFPEngX1.InitEngine() == 0)
            {

                ZKFPEngX1.FPEngineVersion = "10"; //ใช้รูปแบบค้นหาลายนิ้วมือแบบเวอร์ชั่น 10

                fpcHandle = ZKFPEngX1.CreateFPCacheDBEx();
                //ประกาศให้มีการสร้าง Cache ฐานข้อมูลใน Memory แบบใช้เวอร์ชั่น 9 และ 10 ซึ่งจะมีการ
                //เรียกใช้ข้อมูลของเวอร์ชั่น 9 และ 10 ในเวลาเดียวกัน การใช้งานในรูปแบบ อย่างใดอย่างหนึ่ง โปรดศึกษาด้วยตนเองเพิ่มเติม

                //MessageBox.Show("การเชื่อมต่อหัวอ่านสำเร็จ", "สำเร็จ");
                StatusBar.Text = "Sensor เชื่อมต่อแล้ว";
                TextSensorCount.Text = ZKFPEngX1.SensorCount + "";
                //นับหัวอ่าน
                TextSensorIndex.Text = ZKFPEngX1.SensorIndex + "";
                //เลือกใช้ตัวไหน
                TextSensorSN.Text = ZKFPEngX1.SensorSN;
                //ค่า Serial Number ของหัวอ่าน

                ZKFPEngX1.EnrollCount = 3;
                //กำหนดให้การเก็บลายนิ้วมือต้นฉบับต้อง วางนิ้ว 3 ครั้ง
                cmdInit.Enabled = false;
                FMatchType = 0;

                FingerCount = 1;
                //กำหนดนิ้วเริ่มต้นของนิ้วที่จะเก็บลงไปใน Memory

                //โหลดลายนิ้วมือเข้าระบบจากฐานข้อมูลที่ถูกเก็บไว้ก่อนหน้านี้()
                LoadDB();

                deviceOut = true;

                cmdIdentify_Click(sender, e);

            }
            else
            {
                StatusBar.Text = "ไม่สามารถเชื่อมต่อเครื่องสแกนนิ้ว";
            }
        }

        private void cmdInit_Click2(object sender, EventArgs e)
        {
            ZKFPEngX1.SensorIndex = 0;
            //ทำการค้นหาหัวอ่านที่อยู่ในเครื่อง
            if (ZKFPEngX1.InitEngine() == 0)
            {

                ZKFPEngX1.FPEngineVersion = "10"; //ใช้รูปแบบค้นหาลายนิ้วมือแบบเวอร์ชั่น 10

                fpcHandle = ZKFPEngX1.CreateFPCacheDBEx();
                //ประกาศให้มีการสร้าง Cache ฐานข้อมูลใน Memory แบบใช้เวอร์ชั่น 9 และ 10 ซึ่งจะมีการ
                //เรียกใช้ข้อมูลของเวอร์ชั่น 9 และ 10 ในเวลาเดียวกัน การใช้งานในรูปแบบ อย่างใดอย่างหนึ่ง โปรดศึกษาด้วยตนเองเพิ่มเติม
                StatusBar.Text = "พร้อมสแกนนิ้ว";
                //MessageBox.Show("การเชื่อมต่อหัวอ่านสำเร็จ", "สำเร็จ");

                StatusBar.Text = "Sensor เชื่อมต่อแล้ว";
                TextSensorCount.Text = ZKFPEngX1.SensorCount + "";
                //นับหัวอ่าน
                TextSensorIndex.Text = ZKFPEngX1.SensorIndex + "";
                //เลือกใช้ตัวไหน
                TextSensorSN.Text = ZKFPEngX1.SensorSN;
                //ค่า Serial Number ของหัวอ่าน



                //ค้นหา 1:1
                TextFingerName.Text = this.CODE;
                if (ZKFPEngX1.IsRegister)
                {
                    ZKFPEngX1.CancelEnroll();
                }

                StatusBar.Text = "เริ่มค้นหาแบบ (1:1)";
                // กำหนดให้หาแบบ 1 ต่อ 1
                FMatchType = 1;
                cmdInit.Enabled = false;

                ///////////////////////////////////////////////

                //บันทึกนิ้ว
                //ZKFPEngX1.EnrollCount = 3;
                //กำหนดให้การเก็บลายนิ้วมือต้นฉบับต้อง วางนิ้ว 3 ครั้ง
                //cmdInit.Enabled = false;
                //FMatchType = 0;

                //FingerCount = 1;
                //กำหนดนิ้วเริ่มต้นของนิ้วที่จะเก็บลงไปใน Memory

                /////////////////////////////////////////////

                //โหลดลายนิ้วมือเข้าระบบจากฐานข้อมูลที่ถูกเก็บไว้ก่อนหน้านี้()
                LoadDB();

                deviceOut = true;

                ZKFPEngX1.BeginEnroll();
                StatusBar.Text = "เชื่อมต่อเครื่องสแกนนิ้ว สำเร็จ";

                

            }
            else
            {
                StatusBar.Text = "ไม่สามารถเชื่อมต่อเครื่องสแกนนิ้ว";
                //MessageBox.Show("ผิดพลาด");

            }
        }




        private async void LoadDB()
        {

            ApiRest.InitailizeClient();
            fingerScanList = await ApiProcess.getFingerScan(CODE, typeSearch, Global.BRANCH.branch_code);

            string sTemp;
            //ตัวแปรลายนิ้วมือเวอร์ชั้่น 9
            string sTempV10;
            //ตัวแปรลายนิ้วมือเวอร์ชั้่น 10

            if (fingerScanList.Count > 0)
            {
                ZKFPEngX1.RemoveRegTemplateFromFPCacheDBEx(fpcHandle, FingerCount);
                for (int i = 0; i < fingerScanList.Count; i++)
                {
                    FingerScan fs = fingerScanList[i];

                    sTemp = Convert.ToString(fs.finger_str);

                    sTempV10 = Convert.ToString(fs.finger_v10);

                    //ฐานข้อมูลนิ้วเก็บในตรงนี้
                    ZKFPEngX1.AddRegTemplateStrToFPCacheDBEx(fpcHandle, FingerCount, sTemp, sTempV10);
                    //เพิ่ม ลายนิ้วมือเข้าไป โดยมี FingerCount เป็นตัวนับนิ้ว รูปแบบลายนิ้วมือที่ loop เข้าเป็นแบบ string
                    Array.Resize(ref FFingerNames, FingerCount + 1);
                    //สร้าง Array ของตัวแปร เพื่อเก็บชื่อ ของนิ้ว เอาไว้เรียกมาแสดงตอนแสดงผล
                    FFingerNames[FingerCount] = Convert.ToString(fs.finger_id);
                    //เอาข้อมูลเข้า
                    FingerCount = FingerCount + 1;
                }
            }
        }

        private void cmdEnroll_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextFingerName.Text))
            {
                MessageBox.Show("กรอก ชื่อของลายนิ้วมือ !");
                return;
            }
            //เริ่มสั่งให้เก็บลายนิ้วมือ
            ZKFPEngX1.BeginEnroll();
            StatusBar.Text = "เริ่มเก็บลายนิ้วมือ";
        }

        private void cmdSaveImage_Click(object sender, EventArgs e)
        {
            string sFileName = null;
            sFileName = "C:\\Finge-";
            if (OptionBmp.Checked)
            {
                ZKFPEngX1.SaveBitmap(sFileName + ".bmp");
                //บันทึกภาพที่แสดงขึ้นมาเป็น bitmap
            }
            else
            {
                ZKFPEngX1.SaveJPG(sFileName + ".jpg");
                //บันทึกแบบ jpg
            }
            MessageBox.Show("บันทึกลายนิ้วมือ (" + sFileName + ")! เรียบร้อย");
        }

        private async void cmdVerify_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextFingerName.Text))
            {
                MessageBox.Show("กรอกลายชื่อของลายนิ้วมือด้วย");
            }
            else
            {
                if (fingerScanList == null) {
                    ApiRest.InitailizeClient();
                    fingerScanList = await ApiProcess.getFingerScan(CODE, typeSearch, Global.BRANCH.branch_code);
                }

                //ส่งข้อมูลไปเช็คว่ามี รหัสอยู่ในระบบหรือไม่
                if (fingerScanList.Count < 1)
                {
                    //MessageBox.Show("ไม่มีชื่อหรือ ID นี้ในฐานข้อมูล");
                    StatusBar.Text = "ไม่พบลายนิ้วมือในฐานข้อมูล";
                    return;
                }
            }

            if (ZKFPEngX1.IsRegister)
            {
                ZKFPEngX1.CancelEnroll();
            }

            StatusBar.Text = "พร้อมสแกนลายนิ้วมือ";
            // กำหนดให้หาแบบ 1 ต่อ 1
            FMatchType = 1;
        }


        private void cmdIdentify_Click(object sender, EventArgs e)
        {
            // ตรวจสอบว่า อยู่ในโหมดเก็บลายนิ้วมือหรือไม่
            if (ZKFPEngX1.IsRegister)
            {
                ZKFPEngX1.CancelEnroll();
                //ยกเลิกการเก็บ
            }

            StatusBar.Text = "พร้อมสแกนลายนิ้วมือ";
            //ชนิดของรูปแบบการค้นหา 1 คือ แบบ 1 ต่อ 1   2. คือแบบ 1 ต่อ N
            FMatchType = 2;
        }

        //ฟังชั่นค้นหา จาก SQL 
        public bool CheckBySQL(string inSQL)
        {
            try
            {
                DataTable dt = new DataTable();

                //สร้าง SQL
                string strSQL = null;
                strSQL = inSQL;
                //ทำเป็นสตริง

                //Com = new OleDbCommand();
                //var _with1 = Com;
                //_with1.CommandText = strSQL;
                //_with1.CommandType = CommandType.Text;
                //_with1.Connection = Conn;
                //_with1.Parameters.Clear();
                //dr = _with1.ExecuteReader();
                //dt.Load(dr);
                //dr.Close();
                //if (dt.Rows.Count > 0)
                //{
                //    return true;
                //}
                //else
               // {
                   return false;
               // }
               // dt.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error ชุดค้นหาโดย SQL " + ex.Message);
                return false;
            }
        }

        private void ZKFPEngX1_OnEnroll(object sender, AxZKFPEngXControl.IZKFPEngXEvents_OnEnrollEvent e)
        {
            string sTemp = null;
            string sTempV10 = null;


            if (!e.actionResult)
            {
                //MessageBox.Show("บันทึกผิดพลาด");
                StatusBar.Text = "บันทึกผิดพลาด";
            }
            else
            {
                //MessageBox.Show("บันทึกเรียบร้อย");
                
                FRegTemplate = e.aTemplate;

                sTempV10 = ZKFPEngX1.GetTemplateAsStringEx("10");
                //เก็บข้อมูลแบบ String เข้าไว้ที่ตัวแปร โดยเป็นเวอร์ชั่น 10
                sTemp = ZKFPEngX1.GetTemplateAsStringEx("9");
                //เก็บข้อมูลแบบ String เข้าไว้ที่ตัวแปร โดยเป็นเวอร์ชั่น 10


                //ส่งข้อมูลไปเก็บที่ฐานข้อมูลภายใน DB
                //AddTP2DB(0, Convert.ToString(TextFingerName.Text), sTemp, sTempV10);
                //saveFingerScan(sTemp, sTempV10);


                ZKFPEngX1.AddRegTemplateStrToFPCacheDBEx(fpcHandle, FingerCount, sTemp, sTempV10);
                //เพิ่มนิ้วที่เก็บใหม่เข้า Cache ในรูปแบบสตริง

                Array.Resize(ref FFingerNames, FingerCount + 1);
                FFingerNames[FingerCount] = TextFingerName.Text;
                FingerCount = FingerCount + 1;

                //
            }

        }

        private void ZKFPEngX1_OnImageReceived(object sender, AxZKFPEngXControl.IZKFPEngXEvents_OnImageReceivedEvent e)
        {
            Graphics g = pictureBox5.CreateGraphics();
            Bitmap bmp = new Bitmap(pictureBox5.Width, pictureBox5.Height);
            g = Graphics.FromImage(bmp);
            int dc = g.GetHdc().ToInt32();
            ZKFPEngX1.PrintImageAt(dc, 0, 0, bmp.Width, bmp.Height);
            g.Dispose();
            pictureBox5.Image = bmp;
            //กำหนดความกว้าง ยาวและตำแหน่งที่จะให้แสดงบน PictureBox สามารถกำหนดค่าให้ได้ตรงกับต้องการ
        }

        private void ZKFPEngX1_OnFeatureInfo(object sender, AxZKFPEngXControl.IZKFPEngXEvents_OnFeatureInfoEvent e)
        {
            String strTemp = "ลายนิ้วมือ";
            if (e.aQuality != 0)
            {
                strTemp = strTemp + " ไม่ชัดเจน";
            }
            else
            {
                strTemp = strTemp + " ชัดเจน";
            }
            if (ZKFPEngX1.EnrollIndex != 1)
            {
                if (ZKFPEngX1.IsRegister)
                {
                    if (ZKFPEngX1.EnrollIndex - 1 > 0)
                    {
                        strTemp = strTemp + " Register status: still press finger " + Convert.ToString(ZKFPEngX1.EnrollIndex - 1) + " times!";
                    }
                }
            }
            StatusBar.Text = strTemp;
        }

        private void ZKFPEngX1_OnCapture(object sender, AxZKFPEngXControl.IZKFPEngXEvents_OnCaptureEvent e)
        {
            long fi = 0;
            int Score = new int();
            int ProcessNum = new int();
            bool RegChanged = false;
            //string strSQL = null;
            string sTemp = null;
            string sTemp1 = null;


            //StatusBar.Text = "ข้อมูลของลายนิ้วมือ";
            //1:1 ค้นหาแบบ 1 ต่อ 1 คือ มีการ โหลดลายนิ้วมือต้นแบบ มาเปรียบเทียบกับลายนิ้วมือที่วางลงไป
            if (FMatchType == 1)
            {

                //strSQL = "select fpstring from fptable where name='" + TextFingerName.Text + "'";
                //sTemp = sql2data(strSQL);
                //ดึงลายนิ้วมือที่จะเปรียบเทียบจากฐานข้อมูล


                sTemp1 = ZKFPEngX1.GetTemplateAsString();
                //ขอค่าลายนิ้วมือในรูปแบบ string ที่ได้จากการวาง 'ขอค่าลายนิ้วมือในรูปแบบ string ที่ได้จากการวาง


                //ทำการเปรียบเทียบแบบ 1 ต่อ 1 คือนิ้วมือที่เก็บจากฐานข้อมูล และลายนิ้วมือที่วางลงไปใหม่
                if (ZKFPEngX1.VerFingerFromStr(ref sTemp, sTemp1, false, ref RegChanged))
                {
                    //โดยใช้การเปรียบเทียบใน เวอร์ชั่น 9
                    //MessageBox.Show(TextFingerName.Text + " การค้นหาสำเร็จ");
                    StatusBar.Text = this.NAME + " การค้นหาสำเร็จ";
                }
                else
                {
                    //MessageBox.Show("ไม่ใช่นิ้วของ " + TextFingerName.Text);
                    StatusBar.Text = "ไม่ใช่นิ้วของ " + this.NAME;
                }

                //1:N ค้นหาแบบ 1 ต่อจำนวนที่ เก็บลงใน Mem ตอนแรก
            }
            else if (FMatchType == 2)
            {
                Score = 8;
                sTemp = ZKFPEngX1.GetTemplateAsString();
                fi = ZKFPEngX1.IdentificationFromStrInFPCacheDB(fpcHandle, sTemp, ref Score, ref ProcessNum);
                //ทำการเปรียบเทียบ แบบ 1 ต่อ N
                if (fi == -1)
                {
                    //MsgBox "ค้นหาไม่เจอ", vbCritical, "ผิดพลาด"
                    StatusBar.Text = "ค้นหาไม่เจอ";

                    var thread1 = new Thread(Beep2);
                    thread1.Start();
                    var thread3 = new Thread(RED);
                    thread3.Start();
                }
                else
                {
                    //ส่วนสำคัญที่จะนำไปใช้ในการเขียนโปรแกรมต่อ
                    //MsgBox "การค้นหาสำเร็จ ชื่อ =" & FFingerNames(fi) & " Score = " & Score & " Processed Number = " & ProcessNum, vbMsgBoxRight, "ผ่าน"  'คืนค่าการค้นหาออกมาให้ ซึ่งตรงนี้ สามารถเอาไปเรียกข้อมูลอื่นๆ มาใช้งานต่อ <- จุดสำคัญที่จะนำเอาไปใช้งาน
                    StatusBar.Text = "การค้นหาสำเร็จ " + FFingerNames[fi] + " Score = " + Score + " Processed Number = " + ProcessNum;

                    deviceOut = true;
                    
                    var thread1 = new Thread(Beep);
                    thread1.Start();
                    var thread2 = new Thread(Green);
                    thread2.Start();

                    if (typeSearch.Equals("PERSON"))
                    {
                        setPerson(FFingerNames[fi]);
                    }
                    else if (typeSearch.Equals("EMP"))
                    {
                        setEmployee(FFingerNames[fi]);
                    }
                    
                }
            }
        }

        private async void setPerson(string id)
        {
            try
            {
                Global.PERSON = new Person();
                ApiRest.InitailizeClient();
                Global.PERSON = await ApiProcess.getPersonbyFingerId(id, Global.BRANCH.branch_code);
                this.DialogResult = DialogResult.OK;
            }
            catch {
                this.DialogResult = DialogResult.Cancel;
            }
           
        }

        private async void setEmployee(string id)
        {
            try
            {
                Global.EMPLOYEE = new Employee();
                ApiRest.InitailizeClient();
                Global.EMPLOYEE = await ApiProcess.getEmplyeebyFingerId(id, Global.BRANCH.branch_code);

                this.DialogResult = DialogResult.OK;
            }
            catch
            {
                this.DialogResult = DialogResult.Cancel;
            }

        }

        private void Beep() {
            if (BeepStatus.Equals("Y"))
            {
                ZKFPEngX1.ControlSensor(13, 1); //buzzer
                ZKFPEngX1.ControlSensor(13, 0); //
            }
        }

        private void Beep2()
        {
            if (BeepStatus.Equals("Y"))
            {
                ZKFPEngX1.ControlSensor(13, 1); //buzzer
                ZKFPEngX1.ControlSensor(13, 0); //
                ZKFPEngX1.ControlSensor(13, 1); //buzzer
                ZKFPEngX1.ControlSensor(13, 0); //
            }
            
        }

        private void RED()
        {
            ZKFPEngX1.ControlSensor(12, 1); //สีแดงเปิด
            ZKFPEngX1.ControlSensor(12, 0); //สีแดงปิด
        }

        private void Green()
        {
            ZKFPEngX1.ControlSensor(11, 1); //สีเขียวเปิด
            ZKFPEngX1.ControlSensor(11, 0); //สีเขียวปิด
        }


        private void ZKFPEngX1_OnFingerTouching(object sender, EventArgs e)
        {

        }

        private void cmdRED_Click(object sender, EventArgs e)
        {
            if (deviceOut == true)
            {
                ZKFPEngX1.ControlSensor(12, 1); //สีแดงเปิด
                ZKFPEngX1.ControlSensor(12, 0); //สีแดงปิด
            }
        }

        private void cmdGreen_Click(object sender, EventArgs e)
        {
            if (deviceOut == true)
            {
                ZKFPEngX1.ControlSensor(11, 1); //สีเขียวเปิด
                ZKFPEngX1.ControlSensor(11, 0); //สีเขียวปิด
            }
        }

        private void cmdBeep_Click(object sender, EventArgs e)
        {
            if (deviceOut == true)
            {
                ZKFPEngX1.ControlSensor(13, 1); //buzzer
                ZKFPEngX1.ControlSensor(13, 0); //
            }
        }

        private void frm_fg_cus_Load(object sender, EventArgs e)
        {
            //labName.Text = NAME + "  CODE : " + CODE;
            cmdInit_Click(null, null);
        }

    }
}
