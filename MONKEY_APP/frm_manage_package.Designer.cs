﻿namespace MONKEY_APP
{
    partial class frm_manage_package
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.packageCode = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.alertTxt = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboUnit = new System.Windows.Forms.ComboBox();
            this.picLoading = new System.Windows.Forms.PictureBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDetail = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numMaxUse = new System.Windows.Forms.NumericUpDown();
            this.numMaxday = new System.Windows.Forms.NumericUpDown();
            this.numPrice = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.packageId = new System.Windows.Forms.TextBox();
            this.numericNotify = new System.Windows.Forms.NumericUpDown();
            this.comboUnit2 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboStatus = new System.Windows.Forms.ComboBox();
            this.numSeq = new System.Windows.Forms.NumericUpDown();
            this.TypePackageMem = new System.Windows.Forms.RadioButton();
            this.TypePackagePT = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.chkbranch1 = new System.Windows.Forms.CheckBox();
            this.chkbranch2 = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxUse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericNotify)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSeq)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(715, 58);
            this.panel1.TabIndex = 115;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::MONKEY_APP.Properties.Resources.user__1_;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(13, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(38, 37);
            this.pictureBox1.TabIndex = 117;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(58, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 27);
            this.label1.TabIndex = 116;
            this.label1.Text = "เพิ่มแพคเกจ/สินค้า";
            // 
            // button7
            // 
            this.button7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.button7.BackgroundImage = global::MONKEY_APP.Properties.Resources.close__1_;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(685, 9);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(20, 20);
            this.button7.TabIndex = 114;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.Location = new System.Drawing.Point(106, 89);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(43, 24);
            this.label18.TabIndex = 116;
            this.label18.Text = "รหัส :";
            // 
            // packageCode
            // 
            this.packageCode.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.packageCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.packageCode.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.packageCode.Location = new System.Drawing.Point(155, 89);
            this.packageCode.Name = "packageCode";
            this.packageCode.ReadOnly = true;
            this.packageCode.Size = new System.Drawing.Size(157, 24);
            this.packageCode.TabIndex = 117;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button5.Location = new System.Drawing.Point(376, 593);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(125, 51);
            this.button5.TabIndex = 152;
            this.button5.Text = "ยกเลิก";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button4.FlatAppearance.BorderSize = 2;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(209, 593);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(125, 51);
            this.button4.TabIndex = 151;
            this.button4.Text = "บันทึก";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // alertTxt
            // 
            this.alertTxt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.alertTxt.BackColor = System.Drawing.Color.Transparent;
            this.alertTxt.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.alertTxt.ForeColor = System.Drawing.Color.Red;
            this.alertTxt.Location = new System.Drawing.Point(155, 552);
            this.alertTxt.MinimumSize = new System.Drawing.Size(270, 0);
            this.alertTxt.Name = "alertTxt";
            this.alertTxt.Size = new System.Drawing.Size(407, 28);
            this.alertTxt.TabIndex = 153;
            this.alertTxt.Text = "alert";
            this.alertTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.alertTxt.Visible = false;
            this.alertTxt.Click += new System.EventHandler(this.alertTxt_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.Location = new System.Drawing.Point(367, 277);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 24);
            this.label8.TabIndex = 116;
            this.label8.Text = "กลุ่มอายุ :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(29, 324);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 24);
            this.label4.TabIndex = 116;
            this.label4.Text = "จำนวนการใช้งาน :";
            // 
            // comboUnit
            // 
            this.comboUnit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboUnit.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboUnit.FormattingEnabled = true;
            this.comboUnit.Location = new System.Drawing.Point(380, 322);
            this.comboUnit.Name = "comboUnit";
            this.comboUnit.Size = new System.Drawing.Size(144, 32);
            this.comboUnit.TabIndex = 118;
            this.comboUnit.SelectedIndexChanged += new System.EventHandler(this.comboUnit_SelectedIndexChanged);
            // 
            // picLoading
            // 
            this.picLoading.BackColor = System.Drawing.Color.White;
            this.picLoading.Image = global::MONKEY_APP.Properties.Resources.loading2;
            this.picLoading.Location = new System.Drawing.Point(285, 304);
            this.picLoading.Name = "picLoading";
            this.picLoading.Size = new System.Drawing.Size(109, 96);
            this.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoading.TabIndex = 123;
            this.picLoading.TabStop = false;
            this.picLoading.Visible = false;
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName.Enabled = false;
            this.txtName.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtName.Location = new System.Drawing.Point(155, 204);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(496, 24);
            this.txtName.TabIndex = 117;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(113, 204);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 24);
            this.label2.TabIndex = 116;
            this.label2.Text = "ชื่อ :";
            // 
            // txtDetail
            // 
            this.txtDetail.BackColor = System.Drawing.SystemColors.Window;
            this.txtDetail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDetail.Enabled = false;
            this.txtDetail.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtDetail.Location = new System.Drawing.Point(155, 248);
            this.txtDetail.Multiline = true;
            this.txtDetail.Name = "txtDetail";
            this.txtDetail.Size = new System.Drawing.Size(496, 57);
            this.txtDetail.TabIndex = 117;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.Location = new System.Drawing.Point(65, 251);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 24);
            this.label6.TabIndex = 116;
            this.label6.Text = "รายละเอียด :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(47, 372);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 24);
            this.label3.TabIndex = 116;
            this.label3.Text = "อายุการใช้งาน :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(101, 418);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 24);
            this.label7.TabIndex = 116;
            this.label7.Text = "ราคา :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.Location = new System.Drawing.Point(376, 418);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 24);
            this.label9.TabIndex = 116;
            this.label9.Text = "บาท";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(376, 372);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 24);
            this.label10.TabIndex = 116;
            this.label10.Text = "วัน";
            // 
            // numMaxUse
            // 
            this.numMaxUse.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numMaxUse.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.numMaxUse.Location = new System.Drawing.Point(155, 324);
            this.numMaxUse.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numMaxUse.Name = "numMaxUse";
            this.numMaxUse.Size = new System.Drawing.Size(207, 27);
            this.numMaxUse.TabIndex = 154;
            this.numMaxUse.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numMaxUse.ValueChanged += new System.EventHandler(this.numMaxUse_ValueChanged);
            // 
            // numMaxday
            // 
            this.numMaxday.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numMaxday.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.numMaxday.Location = new System.Drawing.Point(155, 372);
            this.numMaxday.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numMaxday.Name = "numMaxday";
            this.numMaxday.Size = new System.Drawing.Size(207, 27);
            this.numMaxday.TabIndex = 154;
            this.numMaxday.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // numPrice
            // 
            this.numPrice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numPrice.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.numPrice.Location = new System.Drawing.Point(155, 418);
            this.numPrice.Maximum = new decimal(new int[] {
            500000,
            0,
            0,
            0});
            this.numPrice.Name = "numPrice";
            this.numPrice.Size = new System.Drawing.Size(207, 27);
            this.numPrice.TabIndex = 154;
            this.numPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.Location = new System.Drawing.Point(45, 508);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(104, 24);
            this.label11.TabIndex = 116;
            this.label11.Text = "ลำดับการแสดง :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.Location = new System.Drawing.Point(43, 127);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(106, 24);
            this.label12.TabIndex = 116;
            this.label12.Text = "ประเภทเพคเกจ :";
            // 
            // packageId
            // 
            this.packageId.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.packageId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.packageId.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.packageId.Location = new System.Drawing.Point(13, 587);
            this.packageId.Multiline = true;
            this.packageId.Name = "packageId";
            this.packageId.ReadOnly = true;
            this.packageId.Size = new System.Drawing.Size(157, 30);
            this.packageId.TabIndex = 117;
            this.packageId.Visible = false;
            // 
            // numericNotify
            // 
            this.numericNotify.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numericNotify.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.numericNotify.Location = new System.Drawing.Point(155, 462);
            this.numericNotify.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericNotify.Name = "numericNotify";
            this.numericNotify.Size = new System.Drawing.Size(207, 27);
            this.numericNotify.TabIndex = 157;
            this.numericNotify.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboUnit2
            // 
            this.comboUnit2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboUnit2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboUnit2.FormattingEnabled = true;
            this.comboUnit2.Location = new System.Drawing.Point(380, 460);
            this.comboUnit2.Name = "comboUnit2";
            this.comboUnit2.Size = new System.Drawing.Size(144, 32);
            this.comboUnit2.TabIndex = 156;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(15, 462);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(134, 24);
            this.label13.TabIndex = 155;
            this.label13.Text = "จำนวนการแจ้งเตือน :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(380, 507);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 24);
            this.label5.TabIndex = 116;
            this.label5.Text = "สถานะ :";
            // 
            // comboStatus
            // 
            this.comboStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboStatus.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboStatus.FormattingEnabled = true;
            this.comboStatus.Location = new System.Drawing.Point(444, 504);
            this.comboStatus.Name = "comboStatus";
            this.comboStatus.Size = new System.Drawing.Size(207, 32);
            this.comboStatus.TabIndex = 118;
            // 
            // numSeq
            // 
            this.numSeq.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numSeq.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.numSeq.Location = new System.Drawing.Point(155, 505);
            this.numSeq.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numSeq.Name = "numSeq";
            this.numSeq.Size = new System.Drawing.Size(207, 27);
            this.numSeq.TabIndex = 157;
            this.numSeq.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TypePackageMem
            // 
            this.TypePackageMem.AutoSize = true;
            this.TypePackageMem.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TypePackageMem.Location = new System.Drawing.Point(159, 125);
            this.TypePackageMem.Name = "TypePackageMem";
            this.TypePackageMem.Size = new System.Drawing.Size(84, 32);
            this.TypePackageMem.TabIndex = 158;
            this.TypePackageMem.TabStop = true;
            this.TypePackageMem.Text = "Member";
            this.TypePackageMem.UseVisualStyleBackColor = true;
            this.TypePackageMem.Click += new System.EventHandler(this.TypePackageMem_Click);
            // 
            // TypePackagePT
            // 
            this.TypePackagePT.AutoSize = true;
            this.TypePackagePT.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TypePackagePT.Location = new System.Drawing.Point(255, 125);
            this.TypePackagePT.Name = "TypePackagePT";
            this.TypePackagePT.Size = new System.Drawing.Size(132, 32);
            this.TypePackagePT.TabIndex = 158;
            this.TypePackagePT.TabStop = true;
            this.TypePackagePT.Text = "Personal Trainer";
            this.TypePackagePT.UseVisualStyleBackColor = true;
            this.TypePackagePT.Click += new System.EventHandler(this.TypePackagePT_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label14.Location = new System.Drawing.Point(99, 162);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 24);
            this.label14.TabIndex = 159;
            this.label14.Text = "สาขา :";
            // 
            // chkbranch1
            // 
            this.chkbranch1.AutoSize = true;
            this.chkbranch1.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.chkbranch1.Location = new System.Drawing.Point(159, 162);
            this.chkbranch1.Name = "chkbranch1";
            this.chkbranch1.Size = new System.Drawing.Size(87, 32);
            this.chkbranch1.TabIndex = 160;
            this.chkbranch1.Text = "ราชพฤกษ์";
            this.chkbranch1.UseVisualStyleBackColor = true;
            // 
            // chkbranch2
            // 
            this.chkbranch2.AutoSize = true;
            this.chkbranch2.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.chkbranch2.Location = new System.Drawing.Point(256, 162);
            this.chkbranch2.Name = "chkbranch2";
            this.chkbranch2.Size = new System.Drawing.Size(121, 32);
            this.chkbranch2.TabIndex = 160;
            this.chkbranch2.Text = "HomePro จรัญ";
            this.chkbranch2.UseVisualStyleBackColor = true;
            // 
            // frm_manage_package
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(715, 664);
            this.ControlBox = false;
            this.Controls.Add(this.chkbranch2);
            this.Controls.Add(this.chkbranch1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.TypePackagePT);
            this.Controls.Add(this.TypePackageMem);
            this.Controls.Add(this.numSeq);
            this.Controls.Add(this.numericNotify);
            this.Controls.Add(this.comboUnit2);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.picLoading);
            this.Controls.Add(this.numPrice);
            this.Controls.Add(this.numMaxday);
            this.Controls.Add(this.numMaxUse);
            this.Controls.Add(this.alertTxt);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.comboStatus);
            this.Controls.Add(this.comboUnit);
            this.Controls.Add(this.txtDetail);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.packageId);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.packageCode);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_manage_package";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frm_manage_package_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxUse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericNotify)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSeq)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox packageCode;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label alertTxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboUnit;
        private System.Windows.Forms.PictureBox picLoading;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDetail;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numMaxUse;
        private System.Windows.Forms.NumericUpDown numMaxday;
        private System.Windows.Forms.NumericUpDown numPrice;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox packageId;
        private System.Windows.Forms.NumericUpDown numericNotify;
        private System.Windows.Forms.ComboBox comboUnit2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboStatus;
        private System.Windows.Forms.NumericUpDown numSeq;
        private System.Windows.Forms.RadioButton TypePackageMem;
        private System.Windows.Forms.RadioButton TypePackagePT;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox chkbranch1;
        private System.Windows.Forms.CheckBox chkbranch2;
    }
}