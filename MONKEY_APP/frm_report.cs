﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class frm_report : Form
    {
        public frm_report()
        {
            InitializeComponent();
        }

        private void frm_report_Load(object sender, EventArgs e)
        {
            int num = 0;
            tableLayoutPanel.ColumnCount = (num + 1);

            List<Button> buttons = new List<Button>();

            for (var i = 0; i < Global.MENULIST.Count; i++)
            {
                string btnName = "";
                int width = 125;
                string menuName = Global.MENULIST[i].page_name;
                string code = Global.MENULIST[i].page_code;
                string moduleId = Global.MENULIST[i].module_id;
                Image img = global::MONKEY_APP.Properties.Resources.folder2;

                if (moduleId.Equals("8"))
                {
                    if (code.Equals("REP01"))
                    {
                        width = 240;
                    }
                    else if (code.Equals("REP02"))
                    {
                        width = 240;
                    }
                    else if (code.Equals("REP03"))
                    {
                        width = 240;
                    }
                    else if (code.Equals("REP04"))
                    {
                        width = 240;
                    }
                    else if (code.Equals("REP05"))
                    {
                        width = 240;
                    }
                    else if (code.Equals("REP06"))
                    {
                        width = 240;
                    }
                    else if (code.Equals("REP07"))
                    {
                        width = 240;
                    }


                    btnName = code;
                    buttons.Add(genButtonMenu(btnName, width, menuName, img));
                    num++;
                }

            }
            tableLayoutPanel.ColumnCount = 1;
            for (var i = 0; i < buttons.Count; i++)
            {
                Button btn = buttons[i];
                tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
                tableLayoutPanel.Controls.Add(btn, i, 0);
            }
        }

        private Button genButtonMenu(string btnName, int width, string menuName, Image img)
        {
            Button btn = new Button();
            btn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            btn.FlatAppearance.BorderSize = 0;
            btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btn.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            btn.Image = img;
            btn.Name = btnName;
            btn.Tag = btnName;
            btn.Size = new System.Drawing.Size(width, 40);
            btn.Text = "  " + menuName;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btn.UseVisualStyleBackColor = false;
            btn.Click += new System.EventHandler(this.buttonClick);

            return btn;
        }

        private void buttonClick(object sender, EventArgs e)
        {
            string code = ((Button)sender).Tag.ToString();
            crystalReportViewer1.ReportSource = null;
            if (code.Equals("REP01"))
            {
                string rptName = "รายงานสรุปยอดเงินตามใบเสร็จรับเงิน";
                dialogRpt drt = new dialogRpt();
                drt.reportName = rptName;
                var result = drt.ShowDialog();
                if (result == DialogResult.OK)
                {
                    string rptdate  = "ตั้งแต่วันที่ " + Utils.getDateEntoTh(drt.dateStart) + " ถึงวันที่ "+ Utils.getDateEntoTh(drt.dateEnd);
                    setReport01(rptName, rptdate, drt.dateStart, drt.dateEnd, "GM");
                }

            }
            else if (code.Equals("REP02"))
            {
                string rptName = "รายงานสรุปยอดเงินตามใบกำกับภาษี";
                dialogRpt drt = new dialogRpt();
                drt.reportName = rptName;
                var result = drt.ShowDialog();
                if (result == DialogResult.OK)
                {
                    string rptdate = "ตั้งแต่วันที่ " + Utils.getDateEntoTh(drt.dateStart) + " ถึงวันที่ " + Utils.getDateEntoTh(drt.dateEnd);
                    setReport01(rptName, rptdate, drt.dateStart, drt.dateEnd, "INV");
                }
            }
            else if (code.Equals("REP03"))
            {
                string rptName = "รายงานสรุปยอดเงินทั้งหมด";
                dialogRpt drt = new dialogRpt();
                drt.reportName = rptName;
                var result = drt.ShowDialog();
                if (result == DialogResult.OK)
                {
                    string rptdate = "ตั้งแต่วันที่ " + Utils.getDateEntoTh(drt.dateStart) + " ถึงวันที่ " + Utils.getDateEntoTh(drt.dateEnd);
                    setReport01(rptName, rptdate, drt.dateStart, drt.dateEnd, "");
                }
            }
            else if (code.Equals("EEMP"))
            {
                frm_manage_emp fmc = new frm_manage_emp("EDIT", null);
                var result = fmc.ShowDialog();

            } 
        }

        private async void setReport01(string reportName, string rptdate, string dateStart,string dateEnd, string typePayment)
        {
            //string startDate = txtStartDate.Value.ToString("yyy/MM/dd");
            //string endDate = txtEndDate.Value.ToString("yyy/MM/dd");
            ApiRest.InitailizeClient();
            Global.InvoiceList = await ApiProcess.getHistoryInv(typePayment, dateStart, dateEnd);

            DataTable dtm = new DataTable();

            dtm.Columns.Add("invoice_code", typeof(string));
            dtm.Columns.Add("invoice_date", typeof(DateTime));
            dtm.Columns.Add("name", typeof(string));
            dtm.Columns.Add("vat", typeof(decimal));
            dtm.Columns.Add("total_net", typeof(decimal));
            dtm.Columns.Add("type_payment", typeof(string));
            dtm.Columns.Add("total", typeof(decimal));

            for (int i = 0; i < Global.InvoiceList.Count(); i++)
            {
                Invoice invoice = Global.InvoiceList[i];

                DateTime dt = Convert.ToDateTime(invoice.invoice_date);
                
                decimal net = decimal.Parse(invoice.total_net);
                decimal total = net*100/107;
                decimal vat = net - total;

                dtm.Rows.Add(
                   invoice.invoice_code, dt, invoice.name,
                   vat, net, invoice.type_payment, total);
            }

            string address2 = "Tel. " + Global.BRANCH.branch_tel + "  เลขประจำตัวผู้เสียภาษี " + Global.BRANCH.branch_tax;
            rpt01 rpt = new rpt01();
            rpt.Database.Tables["invoice"].SetDataSource(dtm);
            rpt.SetParameterValue("reportName", reportName);
            rpt.SetParameterValue("rptdate", rptdate);
            rpt.SetParameterValue("companyName", Global.BRANCH.branch_name);
            rpt.SetParameterValue("Address", Global.BRANCH.branch_address);
            rpt.SetParameterValue("Address2", address2);

            crystalReportViewer1.ReportSource = rpt;


        }


    }
}
