﻿using iTextSharp.text.pdf;
using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public class Utils
    {
        public static string printerName    = System.Configuration.ConfigurationManager.AppSettings["printerName"];
        public static string printPreview   = System.Configuration.ConfigurationManager.AppSettings["printPreview"];
        public static string printerThermal = System.Configuration.ConfigurationManager.AppSettings["printerThermal"];
        

        public static void setImage(string img, PictureBox obj)
        {
            try
            {
                string bpic = img.Replace("data:image/png;base64,", "");

                int width = obj.Size.Width;
                int height = obj.Height;

                byte[] imageBytes = Convert.FromBase64String(bpic);

                using (MemoryStream ms = new MemoryStream(imageBytes))
                {
                    Bitmap imgBit;
                    using (Bitmap bm2 = new Bitmap(ms))
                    {
                        imgBit = ResizeImage(bm2, width, height);
                    }
                    obj.BackgroundImage = imgBit;
                    obj.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
                }
            }
            catch(Exception ex)
            {
                obj.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                obj.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            }
            

        }

        public static class myPrinters
        {
            [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
            public static extern bool SetDefaultPrinter(string Name);

        }

        private static Bitmap ResizeImage(Bitmap image, int width, int height)
        {
            Bitmap resizedImage = new Bitmap(width, height);
            using (Graphics gfx = Graphics.FromImage(resizedImage))
            {
                gfx.DrawImage(image, new Rectangle(0, 0, width, height),
                    new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
            }
            return resizedImage;
        }

        

        public static string getDateTh()
        {
            System.Globalization.CultureInfo _cultureTHInfo = new System.Globalization.CultureInfo("th-TH");
            DateTime dtNow = new DateTime();
            dtNow = DateTime.Now;
            DateTime dateThai = Convert.ToDateTime(dtNow, _cultureTHInfo);

            return dateThai.ToString("dd/MM/yyyy HH:mm:ss", _cultureTHInfo) + " น.";
        }

        public static string getDateEntoTh(string myDate)
        {
            if (myDate != null && !myDate.Equals(""))
            {
                System.Globalization.CultureInfo _cultureTHInfo = new System.Globalization.CultureInfo("th-TH");
                //DateTime dateThai = DateTime.ParseExact(myDate, "dd/MM/yyyy", _cultureTHInfo);
                String chkD = myDate.Substring(5);
                if (chkD.Equals("02-29")) {
                    myDate = myDate.Substring(0, 5) + "02-28";
                }
                DateTime dateThai = Convert.ToDateTime(myDate, _cultureTHInfo);
                return dateThai.ToString("dd/MM/yyyy", _cultureTHInfo);
            }
            else
            {
                return "";
            }  
        }

        public static string getDateTimeEntoTh(string myDate)
        {
            if (myDate != null && !myDate.Equals(""))
            {
                System.Globalization.CultureInfo _cultureTHInfo = new System.Globalization.CultureInfo("th-TH");
                //DateTime dateThai = DateTime.ParseExact(myDate, "dd/MM/yyyy", _cultureTHInfo);
                String chkD = myDate.Substring(5);
                if (chkD.Equals("02-29"))
                {
                    myDate = myDate.Substring(0, 5) + "02-28";
                }
                DateTime dateThai = Convert.ToDateTime(myDate, _cultureTHInfo);
                return dateThai.ToString("dd/MM/yyyy HH:mm:ss", _cultureTHInfo);
            }
            else
            {
                return "";
            }
        }


        public static string ckeckNull(string str)
        {
            return string.IsNullOrEmpty(str)?"": str;
        }

        public static string getDateThtoEn(string myDate)
        {
            //*** Eng Format
            if (myDate != null && !myDate.Equals(""))
            {
                System.Globalization.CultureInfo _cultureEnInfo = new System.Globalization.CultureInfo("en-US");
                DateTime dateEng = Convert.ToDateTime(myDate, _cultureEnInfo);
                return dateEng.ToString("dd/MM/yyyy", _cultureEnInfo);
            }
            else
            {
                return "";
            }
            
        }

        public static string setFormatDate(string myDate,string format)
        {
            if (myDate != null && !myDate.Equals(""))
            {
                System.Globalization.CultureInfo _cultureEnInfo = new System.Globalization.CultureInfo("en-US");
                DateTime date = Convert.ToDateTime(myDate, _cultureEnInfo);
                return date.ToString(format, _cultureEnInfo);
            }
            else
            {
                return "";
            }
        }


        public static string ThaiBaht(string txt)
        {
            string bahtTxt, n, bahtTH = "";
            double amount;
            try { amount = Convert.ToDouble(txt); }
            catch { amount = 0; }
            bahtTxt = amount.ToString("####.00");
            string[] num = { "ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ" };
            string[] rank = { "", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน" };
            string[] temp = bahtTxt.Split('.');
            string intVal = temp[0];
            string decVal = temp[1];
            if (Convert.ToDouble(bahtTxt) == 0)
            {
                bahtTH = "ศูนย์บาทถ้วน";
            }
            else if (Convert.ToDouble(bahtTxt) == 1)
            {
                bahtTH = "หนึ่งบาทถ้วน";
            }
            else
            {
                for (int i = 0; i < intVal.Length; i++)
                {
                    n = intVal.Substring(i, 1);
                    if (n != "0")
                    {
                        if ((i == (intVal.Length - 1)) && (n == "1"))
                            bahtTH += "เอ็ด";
                        else if ((i == (intVal.Length - 2)) && (n == "2"))
                            bahtTH += "ยี่";
                        else if ((i == (intVal.Length - 2)) && (n == "1"))
                            bahtTH += "";
                        else
                            bahtTH += num[Convert.ToInt32(n)];
                        bahtTH += rank[(intVal.Length - i) - 1];
                    }
                }
                bahtTH += "บาท";
                if (decVal == "00")
                    bahtTH += "ถ้วน";
                else
                {
                    for (int i = 0; i < decVal.Length; i++)
                    {
                        n = decVal.Substring(i, 1);
                        if (n != "0")
                        {
                            if ((i == decVal.Length - 1) && (n == "1"))
                                bahtTH += "เอ็ด";
                            else if ((i == (decVal.Length - 2)) && (n == "2"))
                                bahtTH += "ยี่";
                            else if ((i == (decVal.Length - 2)) && (n == "1"))
                                bahtTH += "";
                            else
                                bahtTH += num[Convert.ToInt32(n)];
                            bahtTH += rank[(decVal.Length - i) - 1];
                        }
                    }
                    bahtTH += "สตางค์";
                }
            }
            return bahtTH;
        }

        public static string CURRENT_DIRECTORY = Directory.GetCurrentDirectory();
        public static void getErrorToLog(String str, String formName)
        {

            DateTime ExpDate = DateTime.Now;
            String ExpDateStr = "_" + ((ExpDate.Year > 2500 ? ExpDate.Year - 543 : ExpDate.Year)) + (ExpDate.Month < 10 ? "0" + ExpDate.Month : "" + ExpDate.Month) + (ExpDate.Day < 10 ? "0" + ExpDate.Day : "" + ExpDate.Day) + (ExpDate.Hour < 10 ? "0" + ExpDate.Hour : "" + ExpDate.Hour) + (ExpDate.Minute < 10 ? "0" + ExpDate.Minute : "" + ExpDate.Minute);

            if (!Directory.Exists(CURRENT_DIRECTORY + @"\logs"))
            {
                System.IO.Directory.CreateDirectory(CURRENT_DIRECTORY + @"\logs");
            }
            TextWriterTraceListener twtl = new TextWriterTraceListener(CURRENT_DIRECTORY + @"\logs\" + formName + ExpDateStr + ".txt");
            twtl.Name = "TextLogger";
            twtl.TraceOutputOptions = TraceOptions.ThreadId | TraceOptions.DateTime;

            ConsoleTraceListener ctl = new ConsoleTraceListener(false);
            ctl.TraceOutputOptions = TraceOptions.DateTime;

            Trace.Listeners.Add(twtl);
            Trace.Listeners.Add(ctl);
            Trace.AutoFlush = true;

            Trace.WriteLine("[" + DateTime.Now + "] Error " + formName + ":" + str);
        }



        public static void eformReceipt(string typeVat, Invoice invoice,PackagePersons packagePersons, string txtDate)
        {
            string pdfTemplate = "";
            string newFile = "";
            string targetProcessName = "";
            string targetProcessNameNew = "";

            if (typeVat.Equals("GM"))
            {
                pdfTemplate = CURRENT_DIRECTORY + @"\form\tmp_receipt.pdf";
                newFile = CURRENT_DIRECTORY + @"\form\receipt.pdf";
                targetProcessName = "tmp_receipt.pdf";
                targetProcessNameNew = "receipt.pdf";
            }
            else
            {
                pdfTemplate = CURRENT_DIRECTORY + @"\form\tmp_inv.pdf";
                newFile = CURRENT_DIRECTORY + @"\form\invoice.pdf";
                targetProcessName = "tmp_inv.pdf";
                targetProcessNameNew = "invoice.pdf";
            }


            try
            {
                Process[] runningProcesses = Process.GetProcesses();
                foreach (Process process in runningProcesses)
                {
                    if (process.MainWindowTitle.IndexOf(targetProcessName) >= 0)
                    {
                        process.Kill();
                    }

                    if (process.MainWindowTitle.IndexOf(targetProcessNameNew) >= 0)
                    {
                        process.Kill();
                    }
                }

                PdfReader pdfReader = new PdfReader(pdfTemplate);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));

                AcroFields pdfFormFields = pdfStamper.AcroFields;

                BaseFont EnCodefont = BaseFont.CreateFont(CURRENT_DIRECTORY + @"\form\THSarabun.ttf", BaseFont.IDENTITY_H, false);
                iTextSharp.text.Font Nfont = new iTextSharp.text.Font(EnCodefont, 14, iTextSharp.text.Font.NORMAL);


                try
                {
                    pdfFormFields.AddSubstitutionFont(EnCodefont);
                    pdfFormFields.SetField("companyName", Global.BRANCH.branch_name);
                    pdfFormFields.SetField("comAddress", Global.BRANCH.branch_address);

                    string tel = Global.BRANCH.branch_tel;
                    if (tel.Equals(""))
                    {
                        tel = "          ";
                    }

                    String address2 = "โทร. " + tel + " เลขประจำตัวผู้เสียภาษีอากร " + Global.BRANCH.branch_tax;

                    pdfFormFields.SetField("comAddress2", address2);

                    pdfFormFields.SetField("invCode", invoice.invoice_code);
                    pdfFormFields.SetField("invDate", txtDate);
                    pdfFormFields.SetField("nameCus", invoice.name);
                    pdfFormFields.SetField("address", invoice.address);
                    pdfFormFields.SetField("Tax", invoice.tax);

                    for (int i = 1; i <= packagePersons.PackagePersonList.Count; i++)
                    {
                        PackagePerson ps = packagePersons.PackagePersonList[(i - 1)];

                        string no = "No" + (i);
                        string Description = "Description" + i;
                        string UnitPriceRow = "UnitPriceRow" + i;
                        string QuantityRow = "QuantityRow" + i;
                        string TotalRow = "TotalRow" + i;

                        Double netTotal = Double.Parse(ps.net_total);
                        int package_num = ps.package_num;
                        Double price = (netTotal / package_num);

                        string packageName = ps.package_name + " (" + ps.date_start_txt + " - " + ps.date_expire_txt + ")";
                        pdfFormFields.AddSubstitutionFont(EnCodefont);
                        pdfFormFields.SetField(no, i.ToString());
                        pdfFormFields.SetField(Description, packageName);
                        pdfFormFields.SetField(UnitPriceRow, price.ToString("#,##0.00"));
                        pdfFormFields.SetField(QuantityRow, ps.package_num.ToString());
                        pdfFormFields.SetField(TotalRow, String.Format("{0:n}", Double.Parse(ps.net_total)));
                    }

                    if (invoice.cash > 0)
                    {
                        pdfFormFields.SetField("CheckBox1", "Yes");
                    }

                    if (invoice.credit > 0)
                    {
                        pdfFormFields.SetField("CheckBox2", "Yes");
                    }

                    if (invoice.cheque > 0)
                    {
                        pdfFormFields.SetField("CheckBox3", "Yes");
                    }
                    else if (invoice.transfer > 0)
                    {
                        pdfFormFields.SetField("CheckBox4", "Yes");
                    }

                    if (typeVat.Equals("GM"))
                    {
                        pdfFormFields.SetField("Total", String.Format("{0:n}", invoice.total_net));
                        pdfFormFields.SetField("textPrice", Utils.ThaiBaht(invoice.total_net.Replace(",", "")));
                    }
                    else
                    {
                        pdfFormFields.SetField("Total", String.Format("{0:n}", invoice.total_price));
                        pdfFormFields.SetField("Vat", String.Format("{0:n}", invoice.vat));
                        pdfFormFields.SetField("Nettotal", String.Format("{0:n}", invoice.total_net));
                        pdfFormFields.SetField("textPrice", Utils.ThaiBaht(invoice.total_net.Replace(",", "")));
                    }



                    pdfStamper.FormFlattening = false;
                    pdfStamper.Close();

                    if (!printerName.Equals(""))
                    {
                        Utils.myPrinters.SetDefaultPrinter(printerName);
                    }

                    if (printPreview.Equals("Y"))
                    {
                        System.Diagnostics.Process.Start(newFile);
                    }
                    else
                    {
                        Process p = new Process();
                        p.StartInfo = new ProcessStartInfo()
                        {
                            CreateNoWindow = true,
                            Verb = "print",
                            FileName = newFile//put the correct path here
                        };
                        p.Start();

                    }
                }
                catch (Exception e)
                {
                    Utils.getErrorToLog(":: eformReceipt ::" + e.ToString(), "frm_invoice");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("กรุณาปิดโปรแกรม PDF ไฟล์เดิมที่เปิดไว้");
            }
        }

        public static void eformContract(PackagePerson ps, Person person, string invoice_date, string typePaymentTxt)
        {
            string pdfTemplate = "";
            string newFile = "";
            string targetProcessName = "";
            string targetProcessNameNew = "";

            pdfTemplate = CURRENT_DIRECTORY + @"\form\contract_member_tmp.pdf";
            newFile = CURRENT_DIRECTORY + @"\form\contract_member.pdf";

            targetProcessName = "contract_member_tmp.pdf";
            targetProcessNameNew = "contract_member.pdf";

            Process p = new Process();

            try
            {
                Process[] runningProcesses = Process.GetProcesses();
                foreach (Process process in runningProcesses)
                {
                    if (process.MainWindowTitle.IndexOf(targetProcessName) >= 0)
                    {
                        process.Kill();
                    }

                    if (process.MainWindowTitle.IndexOf(targetProcessNameNew) >= 0)
                    {
                        process.Kill();
                    }
                }

                PdfReader pdfReader = new PdfReader(pdfTemplate);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));

                AcroFields pdfFormFields = pdfStamper.AcroFields;

                BaseFont EnCodefont = BaseFont.CreateFont(CURRENT_DIRECTORY + @"\form\THSarabun.ttf", BaseFont.IDENTITY_H, false);
                iTextSharp.text.Font Nfont = new iTextSharp.text.Font(EnCodefont, 14, iTextSharp.text.Font.NORMAL);


                try
                {
                    pdfFormFields.AddSubstitutionFont(EnCodefont);

                    DateTime date_start = Convert.ToDateTime(ps.date_start);
                    DateTime date_expire = Convert.ToDateTime(ps.date_expire);
                    DateTime date_invoice = Convert.ToDateTime(invoice_date);
                    DateTime dob = Convert.ToDateTime(person.PERSON_BIRTH_DATE);

                    string date_invoice_txt = date_invoice.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

                    pdfFormFields.SetField("date", date_invoice_txt);
                    pdfFormFields.SetField("mid", person.PERSON_CODE);
                    pdfFormFields.SetField("name", person.PERSON_TITLE + person.PERSON_NAME + "  " + person.PERSON_LASTNAME);
                    pdfFormFields.SetField("id", person.PERSON_CARD_ID);
                    pdfFormFields.SetField("address", person.PERSON_HOME1_ADDR1 + person.PERSON_HOME1_ADDR2.Trim() + " " +
                                                      person.PERSON_HOME1_SUBDISTRICT + " " + person.PERSON_HOME1_DISTRICT + " " +
                                                      person.PERSON_HOME1_PROVINCE + " " + person.PERSON_HOME2_POSTAL);
                    pdfFormFields.SetField("tel", person.PERSON_TEL_MOBILE);

                    int age = 0;
                    int year = dob.Year;
                    int dateInvoiceY = int.Parse(date_invoice.ToString("yyyy"));
                    age = (dateInvoiceY - year);
                    if (dateInvoiceY < dob.DayOfYear)
                    {
                        age = age - 1;
                    }

                    pdfFormFields.SetField("bod", person.PERSON_BIRTH_DATE_TXT);
                    pdfFormFields.SetField("age", age.ToString() + " ปี");

                    if (person.PERSON_SEX.Equals("ชาย"))
                    {
                        pdfFormFields.SetField("CheckBox1", "Yes");
                    }
                    else
                    {
                        pdfFormFields.SetField("CheckBox2", "Yes");
                    }

                    pdfFormFields.SetField("dateStart", ps.date_start_txt);
                    pdfFormFields.SetField("dateEnd", ps.date_expire_txt);
                    pdfFormFields.SetField("sale", ps.create_by);

                    double nettotal = double.Parse(ps.net_total);
                    double total = (nettotal / 1.07);
                    double vat = (nettotal - total);

                    pdfFormFields.SetField("month", ps.num_use + " " + ps.package_unit);
                    pdfFormFields.SetField("total", total.ToString("#,##0.00"));
                    pdfFormFields.SetField("vat", vat.ToString("#,##0.00"));
                    pdfFormFields.SetField("net", nettotal.ToString("#,##0.00"));
                    pdfFormFields.SetField("typePayment", typePaymentTxt);


                    pdfStamper.FormFlattening = true;

                    pdfStamper.Close();

                    if (!printerName.Equals(""))
                    {
                        Utils.myPrinters.SetDefaultPrinter(printerName);
                    }


                    if (printPreview.Equals("Y"))
                    {
                        System.Diagnostics.Process.Start(newFile);
                    }
                    else
                    {
                        Process pc = new Process();
                        pc.StartInfo = new ProcessStartInfo()
                        {
                            CreateNoWindow = true,
                            Verb = "print",
                            FileName = newFile//put the correct path here
                        };
                        pc.Start();

                    }
                }
                catch (Exception e)
                {
                    Utils.getErrorToLog(":: eformReceipt ::" + e.ToString(), "Utils");
                }
                finally
                {

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("กรุณาปิดโปรแกรม PDF ไฟล์เดิมที่เปิดไว้");
            }
            finally
            {

                //p.Kill();
            }

        }
        public static void eformContractPT(PackagePerson ps, Person person, string invoice_date, string typePaymentTxt)
        {
            string pdfTemplate = "";
            string newFile = "";

            pdfTemplate = CURRENT_DIRECTORY + @"\form\contract_pt_tmp.pdf";
            newFile = CURRENT_DIRECTORY + @"\form\contract_pt.pdf";

            string targetProcessName = "contract_pt_tmp.pdf";
            string targetProcessNameNew = "contract_pt.pdf";

            Process p = new Process();

            try
            {
                Process[] runningProcesses = Process.GetProcesses();
                foreach (Process process in runningProcesses)
                {
                    if (process.MainWindowTitle.IndexOf(targetProcessName) >= 0)
                    {
                        process.Kill();
                    }

                    if (process.MainWindowTitle.IndexOf(targetProcessNameNew) >= 0)
                    {
                        process.Kill();
                    }
                }

                PdfReader pdfReader = new PdfReader(pdfTemplate);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));

                AcroFields pdfFormFields = pdfStamper.AcroFields;

                BaseFont EnCodefont = BaseFont.CreateFont(CURRENT_DIRECTORY + @"\form\THSarabun.ttf", BaseFont.IDENTITY_H, false);
                iTextSharp.text.Font Nfont = new iTextSharp.text.Font(EnCodefont, 14, iTextSharp.text.Font.NORMAL);


                try
                {
                    pdfFormFields.AddSubstitutionFont(EnCodefont);

                    DateTime date_invoice = Convert.ToDateTime(invoice_date);
                    DateTime dob = Convert.ToDateTime(person.PERSON_BIRTH_DATE);

                    string date_invoice_txt = date_invoice.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

                    pdfFormFields.SetField("date", date_invoice_txt);
                    pdfFormFields.SetField("mid", person.PERSON_CODE);
                    pdfFormFields.SetField("name", person.PERSON_TITLE + person.PERSON_NAME + "  " + person.PERSON_LASTNAME);
                    pdfFormFields.SetField("id", person.PERSON_CARD_ID);
                    pdfFormFields.SetField("address", person.PERSON_HOME1_ADDR1 + person.PERSON_HOME1_ADDR2.Trim() + " " +
                                                      person.PERSON_HOME1_SUBDISTRICT + " " + person.PERSON_HOME1_DISTRICT + " " +
                                                      person.PERSON_HOME1_PROVINCE + " " + person.PERSON_HOME2_POSTAL);
                    pdfFormFields.SetField("tel", person.PERSON_TEL_MOBILE);

                    int age = 0;
                    int year = dob.Year;
                    int dateInvoiceY = int.Parse(date_invoice.ToString("yyyy"));
                    age = (dateInvoiceY - year);
                    if (dateInvoiceY < dob.DayOfYear)
                    {
                        age = age - 1;
                    }

                    pdfFormFields.SetField("bod", person.PERSON_BIRTH_DATE_TXT);
                    pdfFormFields.SetField("age", age.ToString() + " ปี");

                    if (person.PERSON_SEX.Equals("ชาย"))
                    {
                        pdfFormFields.SetField("CheckBox1", "Yes");
                    }
                    else
                    {
                        pdfFormFields.SetField("CheckBox2", "Yes");
                    }

                    pdfFormFields.SetField("packName", ps.package_name);

                    double nettotal = double.Parse(ps.net_total);
                    double total = (nettotal / 1.07);
                    double vat = (nettotal - total);

                    pdfFormFields.SetField("month", ps.num_use + " ครั้ง");
                    pdfFormFields.SetField("total", total.ToString("#,##0.00"));
                    pdfFormFields.SetField("vat", vat.ToString("#,##0.00"));
                    pdfFormFields.SetField("net", nettotal.ToString("#,##0.00"));
                    pdfFormFields.SetField("typePayment", typePaymentTxt);


                    pdfStamper.FormFlattening = true;

                    pdfStamper.Close();

                    if (!printerName.Equals(""))
                    {
                        Utils.myPrinters.SetDefaultPrinter(printerName);
                    }


                    if (printPreview.Equals("Y"))
                    {
                        System.Diagnostics.Process.Start(newFile);
                    }
                    else
                    {
                        Process pc = new Process();
                        pc.StartInfo = new ProcessStartInfo()
                        {
                            CreateNoWindow = true,
                            Verb = "print",
                            FileName = newFile//put the correct path here
                        };
                        pc.Start();

                    }
                }
                catch (Exception e)
                {
                    Utils.getErrorToLog(":: eformReceipt ::" + e.ToString(), "Utils");
                }
                finally
                {

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("กรุณาปิดโปรแกรม PDF ไฟล์เดิมที่เปิดไว้");
            }
            finally
            {

                //p.Kill();
            }

        }


        public static void eformReserveClass(ReserveClass rs)
        {
            string pdfTemplate = "";
            string newFile = "";


            pdfTemplate = CURRENT_DIRECTORY + @"\form\checkin_class_tmp.pdf";
            newFile = CURRENT_DIRECTORY + @"\form\checkin_class.pdf";

            string targetProcessName = "checkin_class_tmp.pdf";
            string targetProcessNameNew = "checkin_class.pdf";


            Process p = new Process();

            try
            {
                Process[] runningProcesses = Process.GetProcesses();
                foreach (Process process in runningProcesses)
                {
                    if (process.MainWindowTitle.IndexOf(targetProcessName) >= 0)
                    {
                        process.Kill();
                    }

                    if (process.MainWindowTitle.IndexOf(targetProcessNameNew) >= 0)
                    {
                        process.Kill();
                    }
                }

                PdfReader pdfReader = new PdfReader(pdfTemplate);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));

                AcroFields pdfFormFields = pdfStamper.AcroFields;

                BaseFont EnCodefont = BaseFont.CreateFont(CURRENT_DIRECTORY + @"\form\THSarabun.ttf", BaseFont.IDENTITY_H, false);
                iTextSharp.text.Font Nfont = new iTextSharp.text.Font(EnCodefont, 14, iTextSharp.text.Font.NORMAL);


                try
                {
                    pdfFormFields.AddSubstitutionFont(EnCodefont);
                    DateTime dateNow = DateTime.Now;

                    pdfFormFields.SetField("memberID", rs.PERSON_CODE);
                    pdfFormFields.SetField("memberName", rs.person_name);
                    pdfFormFields.SetField("className", rs.name_class);
                    pdfFormFields.SetField("date", getDateEntoTh(dateNow.ToString()));
                    pdfFormFields.SetField("time", dateNow.ToString("HH:mm"));
                    pdfFormFields.SetField("nickname", rs.EMP_NICKNAME);
                    pdfFormFields.SetField("dateClass", getDateEntoTh(rs.date_class));
                    pdfFormFields.SetField("startTime", rs.time_start + " - " + rs.time_end);
                    pdfFormFields.SetField("unit", rs.join_seq + "/" + rs.unit);
                    
                    pdfStamper.FormFlattening = true;

                    pdfStamper.Close();

                    if (!printerThermal.Equals(""))
                    {
                        Utils.myPrinters.SetDefaultPrinter(printerThermal);
                        Process pc = new Process();
                        pc.StartInfo = new ProcessStartInfo()
                        {
                            Verb = "Print",
                            FileName = newFile,//put the correct path here
                            WindowStyle = ProcessWindowStyle.Hidden,
                            CreateNoWindow = true
                        };
                        pc.Start();
                    }
                    else
                    {
                        System.Diagnostics.Process.Start(newFile);
                    }
                }
                catch (Exception e)
                {
                    Utils.getErrorToLog(":: eformReserveClass ::" + e.ToString(), "Utils");
                }
                finally
                {

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("กรุณาปิดโปรแกรม PDF ไฟล์เดิมที่เปิดไว้");
            }
            finally
            {

                //p.Kill();
            }

        }

    }
}
