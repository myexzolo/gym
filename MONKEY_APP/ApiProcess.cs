﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP
{
    public class ApiProcess
    {
        public static async Task<User> Login(string username, string password, string branchCode )
        {
            string url = ApiRest.BaseAddress + "login.php?username=" + username + "&password=" + password + "&branchCode=" + branchCode;
            Console.WriteLine(url);
            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    User user = await response.Content.ReadAsAsync<User>();
                    return user;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task<User> LoginByFingerId(string fingerId, string companyCode, string typeSearch)
        {
            string url = ApiRest.BaseAddress + "login_fng.php?fingerId=" + fingerId + "&companyCode=" + companyCode +"&typeSearch=" + typeSearch;
            Console.WriteLine(url);
            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    User user = await response.Content.ReadAsAsync<User>();
                    return user;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task<Branch> getBranch(string branchCode)
        {
            string url = ApiRest.BaseAddress + "getbranch.php?branchcode=" + branchCode;
            Console.WriteLine(url);
            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    Branch branch = await response.Content.ReadAsAsync<Branch>();
                    return branch;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task <List<User>> getUser()
        {
            string url = ApiRest.BaseAddress + "getuser.php";
            List<User> model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<User>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }


        public static async Task<List<Person>> getPerson(string typeSearch)
        {
            string url = ApiRest.BaseAddress + "getuser.php?typeSearch=" + typeSearch + "&companycode=" + Global.BRANCH.branch_code;
            Console.WriteLine(url);
            List<Person> model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<Person>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task<List<PersonCheckin>> getPersonCheckin(string code, string staus_checkin, string person_code, string limit, string typePakage, string order, string id)
        {
            string url = ApiRest.BaseAddress + "getPersonCheckin.php?companycode=" + code + "&staus_checkin=" + staus_checkin + "&person_code=" + person_code + "&limit=" + limit + "&typePakage=" + typePakage + "&order=" + order + "&id=" + id;
            List<PersonCheckin> model = null;

            Console.WriteLine(url);
            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<PersonCheckin>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task<List<Invoice>> getHistoryInv(string typePayment, string startDate, string endDate)
        {
            string url = ApiRest.BaseAddress + "getHistoryInv.php?typePayment=" + typePayment + "&startDate=" + startDate + "&endDate=" + endDate + "&companycode=" + Global.BRANCH.branch_code;
            List<Invoice> model = null;

            Console.WriteLine(url);
            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<Invoice>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task<List<ScheduleClass>> getScheduleClassList(string code, string date_start, string date_end)
        {
            string url = ApiRest.BaseAddress + "getScheduleClassList.php?branchcode=" + code + "&date_start=" + date_start + "&date_end=" + date_end;
            List<ScheduleClass> model = null;
            Console.WriteLine(url);
            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<ScheduleClass>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }


        public static async Task<List<PackagePerson>> checkPackageExpiration()
        {
            string url = ApiRest.BaseAddress + "checkPackageExpiration.php?branchcode=" + Global.BRANCH.branch_code;
            List<PackagePerson> model = null;
            Console.WriteLine(url);
            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<PackagePerson>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<List<Employee>> getEmployee(string typeSearch)
        {
            string url = ApiRest.BaseAddress + "getEmployees.php?typeSearch=" + typeSearch + "&companycode=" + Global.BRANCH.branch_code;

            Console.WriteLine(url);
            List<Employee> model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<Employee>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task<List<Employee>> getTrainers()
        {
            string url = ApiRest.BaseAddress + "getTrainers.php?companycode=" + Global.BRANCH.branch_code;

            Console.WriteLine(url);
            List<Employee> model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<Employee>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task<List<Employee>> getManagers()
        {
            string url = ApiRest.BaseAddress + "getManagers.php?companycode=" + Global.BRANCH.branch_code;

            Console.WriteLine(url);
            List<Employee> model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<Employee>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        

        public static async Task<List<FingerScan>> getFingerScan(string code, string typeSearch, string companyCode)
        {
            string url = ApiRest.BaseAddress + "getFingerScan.php?code=" + code + "&typeSearch=" + typeSearch + "&companyCode=" + companyCode;
            List<FingerScan> model = null;

            try
            {
                using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        model = await response.Content.ReadAsAsync<List<FingerScan>>();
                        return model;
                    }
                    else
                    {
                        throw new Exception(response.ReasonPhrase);
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return model;

            }
        }


        public static async Task<List<Menu>> MenuList(string role_list)
        {
            string url = ApiRest.BaseAddress + "getMenu.php?role_list=" + role_list;
            List<Menu> model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<Menu>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task<List<MASTER>> getMesterDataGroupList(string dataGroup)
        {
            string url = ApiRest.BaseAddress + "getMaster.php?dataGroup=" + dataGroup;
            List<MASTER> model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<MASTER>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task<List<Province>> getProvince()
        {
            string url = ApiRest.BaseAddress + "getProvince.php";
            List<Province> model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<Province>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<List<Package>> getPackage(string packageStatus)
        {
            string url = ApiRest.BaseAddress + "getPackage.php?companyCode=" + Global.BRANCH.branch_code + "&packageStatus=" + packageStatus;
            List<Package> model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<Package>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }


        public static async Task<List<PackagePerson>> getPackageByPersonCode(string personCode)
        {
            string url = ApiRest.BaseAddress + "getPackageByPersonCode.php?companyCode=" + Global.BRANCH.branch_code + "&personCode=" + personCode;
            List<PackagePerson> model = null;
            Console.WriteLine(url);

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<PackagePerson>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<List<PackagePerson>> getPackageByInvoiceCode(string invoice_code)
        {
            string url = ApiRest.BaseAddress + "getPackageByInvoiceCode.php?companyCode=" + Global.BRANCH.branch_code + "&invoice_code=" + invoice_code;
            List<PackagePerson> model = null;
            Console.WriteLine(url);

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<PackagePerson>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        

        public static async Task<List<District>> getDistrict()
        {
            string url = ApiRest.BaseAddress + "getDistrict.php";
            List<District> model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<District>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<List<Subdistrict>> getSubdistrict()
        {
            string url = ApiRest.BaseAddress + "getSubdistrict.php";
            List<Subdistrict> model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<Subdistrict>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }


        public static async Task<List<ReserveClass>> getReserveClassList(string schedule_day_id)
        {
            string url = ApiRest.BaseAddress + "getReserveClassList.php?branchcode=" + Global.BRANCH.branch_code + "&schedule_day_id="+schedule_day_id;
            List<ReserveClass> model = null;
            Console.WriteLine(url);
            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<List<ReserveClass>>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<Response> saveFingerScan(string code, string typeSearch, string fingNumber, string sTemp, string sTempV10, string companyCode)
        {
            string url = ApiRest.BaseAddress + "saveFingerScan.php";

            var requestContent = new[]
            {
                 new KeyValuePair<string, string>("code", code),
                 new KeyValuePair<string, string>("typeSearch", typeSearch),
                 new KeyValuePair<string, string>("fingNumber", fingNumber),
                 new KeyValuePair<string, string>("sTemp", sTemp),
                 new KeyValuePair<string, string>("sTempV10", sTempV10),
                 new KeyValuePair<string, string>("companyCode", companyCode),
             };

            var encodedItems = requestContent.Select(i => WebUtility.UrlEncode(i.Key) + "=" + WebUtility.UrlEncode(i.Value));
            var encodedContent = new StringContent(String.Join("&", encodedItems), null, "application/x-www-form-urlencoded");

            //Console.WriteLine(url);
            Response model = null;

            try
            {
                using (HttpResponseMessage response = await ApiRest.ApiClient.PostAsync(url, encodedContent))
                //using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        model = await response.Content.ReadAsAsync<Response>();
                        return model;
                    }
                    else
                    {
                        throw new Exception(response.ReasonPhrase);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return model;
            }
        }

        public static async Task<Response> genEmpCode(string code)
        {
            string url = ApiRest.BaseAddress + "genEmpCode.php?companycode=" + code;
            Response model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Response>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task<Response> manageCheckin(string typeActive, string id, string person_code, string package_person_id, string sign_person, string sign_emp, string sign_manager, string status, string use_package, string trainer_code, string manager_code)
        {
            string url = ApiRest.BaseAddress + "manageCheckin.php?typeActive="+ typeActive  +
                "&id="+id+
                "&user_id_update=" + Global.EMPLOYEE.EMP_CODE +
                "&company_code=" + Global.BRANCH.branch_code +
                "&person_code="+ person_code +
                "&sign_person=" + sign_person +
                "&sign_emp=" + sign_emp +
                "&sign_manager=" + sign_manager +
                "&status=" + status +
                "&use_package=" + use_package +
                "&trainer_code=" + trainer_code +
                "&manager_code=" + manager_code +
                "&package_person_id=" + package_person_id;

            Console.WriteLine(url);
            Response model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Response>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }
        

        public static async Task<Response> genRegNo(string code, string companyId, string typePackage)
        {
            string url = ApiRest.BaseAddress + "genRegNo.php?companycode=" + code + "&companyId=" + companyId + "&typePackage=" + typePackage;
            Response model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Response>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<Response> checkExpired()
        {
            string url = ApiRest.BaseAddress + "checkExpired.php?company_code=" + Global.BRANCH.branch_code;
            Response model = null;
            Console.WriteLine(url);
            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Response>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }


        public static async Task<Response> getVersion()
        {
            string url = ApiRest.BaseAddress + "getVersion.php";
            Response model = null;

            Console.WriteLine(url);
            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Response>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<Response> genInvNo(string code, string companyId, string invoiceDate, string typeVat)
        {
            string url = ApiRest.BaseAddress + "genInvNo.php?companycode=" + code + "&companyId=" + companyId + "&invoiceDate=" + invoiceDate + "&typeVat=" + typeVat;
            Response model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Response>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        

        public static async Task<Response> genPackageCode(string code)
        {
            string url = ApiRest.BaseAddress + "genPackageCode.php?package_type=" + code + "&&companycode=" + Global.BRANCH.branch_code;
            Response model = null;
            Console.WriteLine(url);
            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Response>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task<Response> genCusCode(string code, string companyId)
        {
            string url = ApiRest.BaseAddress + "genCusCode.php?companycode=" + code + "&companyId=" + companyId;
            Response model = null;
            Console.WriteLine(url);
            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Response>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task<Employee> getEmployeeById(string code,string empcode)
        {
            string url = ApiRest.BaseAddress + "getEmployeeById.php?companycode=" + code + "&empcode=" + empcode;
            Employee model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Employee>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }


        public static async Task<Package> getPackageById(String id)
        {
            string url = ApiRest.BaseAddress + "getPackageById.php?id=" + id;
            Package model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Package>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<Person> getPersonById(string code, string personCode)
        {
            string url = ApiRest.BaseAddress + "getPersonById.php?companycode=" + code + "&personCode=" + personCode;
            Person model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Person>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

        public static async Task<Person> getPersonbyFingerId(string id,string code)
        {
            string url = ApiRest.BaseAddress + "getPersonbyFingerId.php?companycode=" + code + "&id=" + id;
            Person model = null;
            Console.WriteLine(url);
            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Person>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public static async Task<Employee> getEmplyeebyFingerId(string id, string code)
        {
            string url = ApiRest.BaseAddress + "getEmplyeebyFingerId.php?companycode=" + code + "&id=" + id;
            Employee model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Employee>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }




        public static async Task<Response> manageEmployee(string code, string typeActive, Employee emp,string user_id_update)
        {
            /*string url = ApiRest.BaseAddress + "manageEmployee.php?code=" + code + 
                "&typeActive=" + typeActive +
                "&user_id_update=" + user_id_update +
                "&EMP_CODE=" + emp.EMP_CODE +
                "&EMP_TITLE=" + emp.EMP_TITLE +
                "&EMP_NAME=" + emp.EMP_NAME +
                "&EMP_LASTNAME=" + emp.EMP_LASTNAME +
                "&EMP_POSITION=" + emp.EMP_POSITION +
                "&EMP_DEPARTMENT=" + emp.EMP_DEPARTMENT +
                "&EMP_SEX=" + emp.EMP_SEX +
                "&EMP_BIRTH_DATE=" + emp.EMP_BIRTH_DATE +
                "&EMP_TYPE=" + emp.EMP_TYPE +
                "&EMP_GROUP=" + emp.EMP_GROUP +
                "&EMP_NICKNAME=" + emp.EMP_NICKNAME +
                "&EMP_DATE_INCOME=" + emp.EMP_DATE_INCOME +
                "&EMP_CARD_ID=" + emp.EMP_CARD_ID +
                "&EMP_ADDRESS1=" + emp.EMP_ADDRESS1 +
                "&EMP_ADDRESS2=" + emp.EMP_ADDRESS2 +
                "&EMP_ADDR_SUBDISTRICT=" + emp.EMP_ADDR_SUBDISTRICT +
                "&EMP_ADDR_DISTRICT=" + emp.EMP_ADDR_DISTRICT +
                "&EMP_ADDR_PROVINCE=" + emp.EMP_ADDR_PROVINCE +
                "&EMP_ADDR_POSTAL=" + emp.EMP_ADDR_POSTAL +
                "&EMP_EMAIL=" + emp.EMP_EMAIL +
               "&EMP_TEL=" + emp.EMP_TEL +
               "&EMP_PICTURE=" + emp.EMP_PICTURE;
               */
            

            string url = ApiRest.BaseAddress + "manageEmployee.php";

            var requestContent = new[]
            {
                new KeyValuePair<string, string>("code", code),
                new KeyValuePair<string, string>("typeActive", typeActive),
                new KeyValuePair<string, string>("user_id_update", user_id_update),
                new KeyValuePair<string, string>("EMP_CODE", emp.EMP_CODE),
                new KeyValuePair<string, string>("EMP_TITLE", emp.EMP_TITLE),
                new KeyValuePair<string, string>("EMP_NAME" , emp.EMP_NAME),
                new KeyValuePair<string, string>("EMP_LASTNAME" , emp.EMP_LASTNAME),
                new KeyValuePair<string, string>("EMP_POSITION" , emp.EMP_POSITION),
                new KeyValuePair<string, string>("EMP_DEPARTMENT" , emp.EMP_DEPARTMENT),
                new KeyValuePair<string, string>("EMP_SEX", emp.EMP_SEX),
                new KeyValuePair<string, string>("EMP_BIRTH_DATE" , emp.EMP_BIRTH_DATE),
                new KeyValuePair<string, string>("EMP_TYPE" , emp.EMP_TYPE),
                new KeyValuePair<string, string>("EMP_GROUP" , emp.EMP_GROUP),
                new KeyValuePair<string, string>("EMP_NICKNAME" , emp.EMP_NICKNAME),
                new KeyValuePair<string, string>("EMP_DATE_INCOME" , emp.EMP_DATE_INCOME),
                new KeyValuePair<string, string>("EMP_CARD_ID" , emp.EMP_CARD_ID),
                new KeyValuePair<string, string>("EMP_ADDRESS1" , emp.EMP_ADDRESS1),
                new KeyValuePair<string, string>("EMP_ADDRESS2" , emp.EMP_ADDRESS2),
                new KeyValuePair<string, string>("EMP_ADDR_SUBDISTRICT" , emp.EMP_ADDR_SUBDISTRICT),
                new KeyValuePair<string, string>("EMP_ADDR_DISTRICT" , emp.EMP_ADDR_DISTRICT),
                new KeyValuePair<string, string>("EMP_ADDR_PROVINCE" , emp.EMP_ADDR_PROVINCE),
                new KeyValuePair<string, string>("EMP_ADDR_POSTAL" , emp.EMP_ADDR_POSTAL),
                new KeyValuePair<string, string>("EMP_EMAIL" , emp.EMP_EMAIL),
                new KeyValuePair<string, string>("EMP_TEL" , emp.EMP_TEL),
                new KeyValuePair<string, string>("EMP_PICTURE" , emp.EMP_PICTURE),
                new KeyValuePair<string, string>("EMP_IS_SALE" , emp.EMP_IS_SALE),
                new KeyValuePair<string, string>("EMP_IS_TRAINER" , emp.EMP_IS_TRAINER),
                new KeyValuePair<string, string>("EMP_IS_INSTRUCTOR" , emp.EMP_IS_INSTRUCTOR),
                new KeyValuePair<string, string>("EMP_IS_STAFF" , emp.EMP_IS_STAFF),
                new KeyValuePair<string, string>("branch1" , emp.branch1),
                new KeyValuePair<string, string>("branch2" , emp.branch2),
                new KeyValuePair<string, string>("branch3" , emp.branch3),
                new KeyValuePair<string, string>("branch4" , emp.branch4),
                new KeyValuePair<string, string>("branch5" , emp.branch5),
                new KeyValuePair<string, string>("EMP_WAGE" , emp.EMP_WAGE)
                
            };

            var encodedItems = requestContent.Select(i => WebUtility.UrlEncode(i.Key) + "=" + WebUtility.UrlEncode(i.Value));
            var encodedContent = new StringContent(String.Join("&", encodedItems), null, "application/x-www-form-urlencoded");

            Console.WriteLine(url);
            Response model = null;

            try
            {
                using (HttpResponseMessage response = await ApiRest.ApiClient.PostAsync(url, encodedContent))
                //using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        model = await response.Content.ReadAsAsync<Response>();
                        return model;
                    }
                    else
                    {
                        throw new Exception(response.ReasonPhrase);
                    }

                }

            }
            catch (Exception ex) {
                Console.WriteLine(ex);
                return model;

            }
            
        }

        public static async Task<Response> managePackage(string typeActive, Package package, string user_id_update)
        {
            string url = ApiRest.BaseAddress + "managePackage.php?" + 
                "typeActive=" + typeActive +
                "&user_id_update=" + user_id_update +
                "&package_id=" + package.package_id +
                "&package_code=" + package.package_code +
                "&package_name=" + package.package_name +
                "&package_detail=" + package.package_detail +
                "&package_type=" + package.package_type +
                "&num_use=" + package.num_use +
                "&package_unit=" + package.package_unit +
                "&max_use=" + package.max_use +
                "&package_price=" + package.package_price +
                "&type_vat=" + package.type_vat +
                "&notify_num=" + package.notify_num +
                "&notify_unit=" + package.notify_unit +
                "&company_code=" + Global.BRANCH.branch_code +
                "&seq=" + package.seq +
                "&branch1=" + package.branch1 +
                "&branch2=" + package.branch2 +
                "&branch3=" + package.branch3 +
                "&branch4=" + package.branch4 +
                "&branch5=" + package.branch5 +
                "&package_status=" + package.package_status;
             

            Console.WriteLine(url);
            Response model = null;

            try
            {
                using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        model = await response.Content.ReadAsAsync<Response>();
                        return model;
                    }
                    else
                    {
                        throw new Exception(response.ReasonPhrase);
                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return model;

            }

        }


        public static async Task<Response> manageTransferPackge(string memberCodeTransfer, string id, string memberCodeRecipient, string user_id_update)
        {
            string url = ApiRest.BaseAddress + "manageTransferPackge.php?" +
                "memberCodeTransfer=" + memberCodeTransfer +
                "&id=" + id +
                "&memberCodeRecipient=" + memberCodeRecipient +
                "&user_id_update=" + user_id_update;


            Console.WriteLine(url);
            Response model = null;

            try
            {
                using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        model = await response.Content.ReadAsAsync<Response>();
                        return model;
                    }
                    else
                    {
                        throw new Exception(response.ReasonPhrase);
                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return model;

            }
        }


        public static async Task<Response> managePackageCustomer(string typeActive, PackagePerson p, string user_id_update)
        {
            string url = ApiRest.BaseAddress + "managePackageCustomer.php?" +
                "id=" + p.id +
                "&company_code=" + p.company_code +
                "&person_code=" + p.person_code +
                "&package_code=" + p.package_code +
                "&package_name=" + p.package_name +
                "&package_detail=" + p.package_detail +
                "&reg_no=" + p.reg_no +
                "&num_use=" + p.num_use +
                "&package_unit=" + p.package_unit +
                "&date_start=" + p.date_start +
                "&date_expire=" + p.date_expire +
                "&max_use=" + p.max_use +
                "&type_package="+ p.type_package +
                "&package_price=" + p.package_price +
                "&package_num=" + p.package_num +
                "&package_price_total=" + p.package_price_total +
                "&percent_discount=" + p.percent_discount +
                "&discount=" + p.discount +
                "&vat=" + p.vat +
                "&type_vat=" + p.type_vat +
                "&net_total=" + p.net_total +
                "&notify_num=" + p.notify_num +
                "&notify_unit=" + p.notify_unit +
                "&trainer_code=" + p.trainer_code +
                "&trainer_name=" + p.trainer_name +
                "&invoice_code=" + p.invoice_code +
                "&receipt_code=" + p.receiptCode +
                "&user_id_update=" + user_id_update+
                "&type_buy=" + p.type_buy +
                "&sale_code=" + p.sale_code +
                "&typeActive=" + typeActive;


            Console.WriteLine(url);
            Response model = null;

            try
            {
                using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        model = await response.Content.ReadAsAsync<Response>();
                        return model;
                    }
                    else
                    {
                        throw new Exception(response.ReasonPhrase);
                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return model;

            }

        }

        public static async Task<Response> managerReserveClass(string code, string typeActive, ReserveClass res)
        {
            string url = ApiRest.BaseAddress + "managerReserveClass.php?" +
                        "reserve_id=" + res.reserve_id +
                        "&schedule_day_id=" + res.schedule_day_id +
                        "&PERSON_CODE=" + res.PERSON_CODE +
                        "&status=" + res.status +
                        "&expire_date=" + res.expire_date +
                        "&typeActive=" + typeActive +
                        "&company_code=" + code +
                        "&user_id_update=" + Global.USER.user_id;

            Console.WriteLine(url);
            Response model = null;

            try
            {
                using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        model = await response.Content.ReadAsAsync<Response>();
                        return model;
                    }
                    else
                    {
                        throw new Exception(response.ReasonPhrase);
                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return model;

            }
        }

        public static async Task<Response> manageInvoice(string code, string typeActive, Invoice invoice, string user_id_update)
        {
            string url = ApiRest.BaseAddress + "manageInvoice.php?" +
                        "typeActive=" + typeActive +
                        "&invoice_id=" + invoice.invoice_id +
                        "&company_code=" + code +
                        "&companyId=" + Global.BRANCH.branch_id +
                        "&invoice_code=" + invoice.invoice_code +
                        "&invoice_date=" + invoice.invoice_date +
                        "&name=" + invoice.name +
                        "&address=" + invoice.address +
                        "&tel=" + invoice.tel +
                        "&fax=" + invoice.fax +
                        "&tax=" + invoice.tax +
                        "&cash=" + invoice.cash +
                        "&transfer=" + invoice.transfer +
                        "&credit=" + invoice.credit +
                        "&cheque=" + invoice.cheque +
                        "&type_payment=" + invoice.type_payment.Replace(",", "") +
                        "&total_price=" + invoice.total_price.Replace(",", "") +
                        "&discount=" + invoice.discount.Replace(",", "") +
                        "&vat=" + invoice.vat.Replace(",", "") +
                        "&total_net=" + invoice.total_net.Replace(",", "") +
                        "&receipt=" + invoice.receipt +
                        "&num_print =" + invoice.num_print +
                        "&user_id_update=" + user_id_update;

            Console.WriteLine(url);
            Response model = null;

            try
            {
                using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        model = await response.Content.ReadAsAsync<Response>();
                        return model;
                    }
                    else
                    {
                        throw new Exception(response.ReasonPhrase);
                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return model;

            }

        }



        public static async Task<Response> managePerson(string code, string typeActive, Person person, string user_id_update)
        {

            /*string url = ApiRest.BaseAddress + "managePerson.php?code=" + code + 
                "&typeActive=" + typeActive +
                "&user_id_update=" + user_id_update +
                "&COMPANY_CODE=" + person.COMPANY_CODE +
                "&PERSON_RUNNO=" + person.PERSON_RUNNO +
                "&PERSON_CODE=" + person.PERSON_CODE +
                "&PERSON_CODE_INT=" + person.PERSON_CODE_INT +
                "&PERSON_TITLE=" + person.PERSON_TITLE +
                "&PERSON_NICKNAME=" + person.PERSON_NICKNAME +
                "&PERSON_NAME=" + person.PERSON_NAME +
                "&PERSON_LASTNAME=" + person.PERSON_LASTNAME +
                "&PERSON_NAME_MIDDLE=" + person.PERSON_NAME_MIDDLE +
                "&PERSON_TITLE_ENG=" + person.PERSON_TITLE_ENG +
                "&PERSON_NAME_ENG=" + person.PERSON_NAME_ENG +
                "&PERSON_LASTNAME_ENG=" + person.PERSON_LASTNAME_ENG +
                "&PERSON_IMAGE=" + person.PERSON_IMAGE +
                "&PERSON_SEX=" + person.PERSON_SEX +
                "&PERSON_BIRTH_DATE=" + person.PERSON_BIRTH_DATE +
                "&PERSON_CARD_ID=" + person.PERSON_CARD_ID +
                "&PERSON_TEL_MOBILE=" + person.PERSON_TEL_MOBILE +
                "&PERSON_TEL_MOBILE2=" + person.PERSON_TEL_MOBILE2 +
                "&PERSON_TEL_OFFICE=" + person.PERSON_TEL_OFFICE +
                "&PERSON_FAX=" + person.PERSON_FAX +
                "&PERSON_GROUP=" + person.PERSON_GROUP +
                "&PERSON_GROUP_AGE=" + person.PERSON_GROUP_AGE +
                "&PERSON_GROUP_SERVICE=" + "" +
                "&PERSON_GROUP_DISCOUNT=" + "" +
                "&PERSON_STATUS=" + person.PERSON_STATUS +
                "&PERSON_REGISTER_DATE=" + person.PERSON_REGISTER_DATE +
                "&PERSON_EXPIRE_DATE=" + person.PERSON_EXPIRE_DATE +
                "&PERSON_NOTE=" + person.PERSON_NOTE +
                "&PERSON_LAST_VISIT=" + person.PERSON_LAST_VISIT +
                "&PERSON_GRADE=" + person.PERSON_GRADE +
                "&PERSON_PROVINCE_CODE=" + person.PERSON_PROVINCE_CODE +
                "&PERSON_DOOR_CARDNO=" + person.PERSON_DOOR_CARDNO +
                "&PERSON_DOOR_PIN=" + person.PERSON_DOOR_PIN +
                "&PERSON_DOOR_PASSWORD=" + person.PERSON_DOOR_PASSWORD +
                "&PERSON_DOOR_GROUP=" + person.PERSON_DOOR_GROUP +
                "&PERSON_HOME1_ADDR1=" + person.PERSON_HOME1_ADDR1 +
                "&PERSON_HOME1_ADDR2=" + person.PERSON_HOME1_ADDR2 +
                "&PERSON_HOME1_SUBDISTRICT=" + person.PERSON_HOME1_SUBDISTRICT +
                "&PERSON_HOME1_DISTRICT=" + person.PERSON_HOME1_DISTRICT +
                "&PERSON_HOME1_PROVINCE=" + person.PERSON_HOME1_PROVINCE +
                "&PERSON_HOME1_COUNTRY=" + person.PERSON_HOME1_COUNTRY +
                "&PERSON_HOME1_POSTAL=" + person.PERSON_HOME1_POSTAL +
                "&PERSON_HOME1_MAP=" + "" +
                "&PERSON_BUSINESS_TYPE=" + "" +
                "&PERSON_BUSINESS_CAP=" + "" +
                "&PERSON_BILL_TYPE=" + person.PERSON_BILL_TYPE +
                "&PERSON_BILL_NAME=" + person.PERSON_BILL_NAME +
                "&PERSON_BILL_ADDR1=" + person.PERSON_BILL_ADDR1 +
                "&PERSON_BILL_ADDR2=" + person.PERSON_BILL_ADDR2 +
                "&PERSON_BILL_SUBDISTRICT=" + person.PERSON_BILL_SUBDISTRICT +
                "&PERSON_BILL_DISTRICT=" + person.PERSON_BILL_DISTRICT +
                "&PERSON_BILL_PROVINCE=" + person.PERSON_BILL_PROVINCE +
                "&PERSON_BILL_POSTAL=" + person.PERSON_BILL_POSTAL +
                "&PERSON_BILL_COUNTRY=" + person.PERSON_BILL_COUNTRY +
                "&PERSON_BILL_TAXNO=" + person.PERSON_BILL_TAXNO +
                "&PERSON_SALE_CODE=" + person.PERSON_SALE_CODE +
                "&PERSON_ACC_NO=" + person.PERSON_ACC_NO;

            */

            string url = ApiRest.BaseAddress + "managePerson.php";

             var requestContent = new[]
             {
                 new KeyValuePair<string, string>("code", code),
                 new KeyValuePair<string, string>("typeActive", typeActive),
                 new KeyValuePair<string, string>("user_id_update", user_id_update),

                 new KeyValuePair<string, string>("COMPANY_CODE", person.COMPANY_CODE),
                 new KeyValuePair<string, string>("PERSON_RUNNO", person.PERSON_RUNNO),
                 new KeyValuePair<string, string>("PERSON_CODE", person.PERSON_CODE),
                 new KeyValuePair<string, string>("PERSON_CODE_INT", person.PERSON_CODE_INT),
                 new KeyValuePair<string, string>("PERSON_TITLE", person.PERSON_TITLE),
                 new KeyValuePair<string, string>("PERSON_NICKNAME", person.PERSON_NICKNAME),
                 new KeyValuePair<string, string>("PERSON_NAME", person.PERSON_NAME),
                 new KeyValuePair<string, string>("PERSON_LASTNAME", person.PERSON_LASTNAME),
                 new KeyValuePair<string, string>("PERSON_NAME_MIDDLE", person.PERSON_NAME_MIDDLE),
                 new KeyValuePair<string, string>("PERSON_TITLE_ENG", person.PERSON_TITLE_ENG),
                 new KeyValuePair<string, string>("PERSON_NAME_ENG", person.PERSON_NAME_ENG),
                 new KeyValuePair<string, string>("PERSON_LASTNAME_ENG", person.PERSON_LASTNAME_ENG),
                 new KeyValuePair<string, string>("PERSON_SEX", person.PERSON_SEX),
                 new KeyValuePair<string, string>("PERSON_BIRTH_DATE", person.PERSON_BIRTH_DATE),
                 new KeyValuePair<string, string>("PERSON_CARD_ID", person.PERSON_CARD_ID),
                 new KeyValuePair<string, string>("PERSON_TEL_MOBILE", person.PERSON_TEL_MOBILE),
                 new KeyValuePair<string, string>("PERSON_TEL_MOBILE2", person.PERSON_TEL_MOBILE2),
                 new KeyValuePair<string, string>("PERSON_TEL_OFFICE", person.PERSON_TEL_OFFICE),
                 new KeyValuePair<string, string>("PERSON_ER_TEL", person.PERSON_ER_TEL),
                 new KeyValuePair<string, string>("PERSON_FAX", person.PERSON_FAX),
                 new KeyValuePair<string, string>("PERSON_IMAGE", person.PERSON_IMAGE),
                 new KeyValuePair<string, string>("PERSON_GROUP", person.PERSON_GROUP),
                 new KeyValuePair<string, string>("PERSON_GROUP_AGE", person.PERSON_GROUP_AGE),
                 new KeyValuePair<string, string>("PERSON_GROUP_SERVICE", ""),
                 new KeyValuePair<string, string>("PERSON_GROUP_DISCOUNT", ""),
                 new KeyValuePair<string, string>("PERSON_STATUS", person.PERSON_STATUS),
                 new KeyValuePair<string, string>("PERSON_REGISTER_DATE", person.PERSON_REGISTER_DATE),
                 new KeyValuePair<string, string>("PERSON_EXPIRE_DATE", person.PERSON_EXPIRE_DATE),
                 new KeyValuePair<string, string>("PERSON_NOTE", person.PERSON_NOTE),
                 new KeyValuePair<string, string>("PERSON_LAST_VISIT", person.PERSON_LAST_VISIT),
                 new KeyValuePair<string, string>("PERSON_GRADE", person.PERSON_GRADE),
                 new KeyValuePair<string, string>("PERSON_PROVINCE_CODE", person.PERSON_PROVINCE_CODE),
                 new KeyValuePair<string, string>("PERSON_DOOR_CARDNO", person.PERSON_DOOR_CARDNO),
                 new KeyValuePair<string, string>("PERSON_DOOR_PIN", person.PERSON_DOOR_PIN),
                 new KeyValuePair<string, string>("PERSON_DOOR_PASSWORD", person.PERSON_DOOR_PASSWORD),
                 new KeyValuePair<string, string>("PERSON_DOOR_GROUP", person.PERSON_DOOR_GROUP),
                 new KeyValuePair<string, string>("PERSON_HOME1_ADDR1", person.PERSON_HOME1_ADDR1),
                 new KeyValuePair<string, string>("PERSON_HOME1_ADDR2", person.PERSON_HOME1_ADDR2),
                 new KeyValuePair<string, string>("PERSON_HOME1_SUBDISTRICT", person.PERSON_HOME1_SUBDISTRICT),
                 new KeyValuePair<string, string>("PERSON_HOME1_DISTRICT", person.PERSON_HOME1_DISTRICT),
                 new KeyValuePair<string, string>("PERSON_HOME1_PROVINCE", person.PERSON_HOME1_PROVINCE),
                 new KeyValuePair<string, string>("PERSON_HOME1_COUNTRY", person.PERSON_HOME1_COUNTRY),
                 new KeyValuePair<string, string>("PERSON_HOME1_POSTAL", person.PERSON_HOME1_POSTAL),
                 new KeyValuePair<string, string>("PERSON_HOME1_MAP", ""),
                 new KeyValuePair<string, string>("PERSON_BUSINESS_TYPE", ""),
                 new KeyValuePair<string, string>("PERSON_BUSINESS_CAP",""),
                 new KeyValuePair<string, string>("PERSON_EMAIL",person.PERSON_EMAIL),
                 new KeyValuePair<string, string>("PERSON_BILL_TYPE", person.PERSON_BILL_TYPE),
                 new KeyValuePair<string, string>("PERSON_BILL_NAME", person.PERSON_BILL_NAME),
                 new KeyValuePair<string, string>("PERSON_BILL_ADDR1", person.PERSON_BILL_ADDR1),
                 new KeyValuePair<string, string>("PERSON_BILL_ADDR2", person.PERSON_BILL_ADDR2),
                 new KeyValuePair<string, string>("PERSON_BILL_SUBDISTRICT", person.PERSON_BILL_SUBDISTRICT),
                 new KeyValuePair<string, string>("PERSON_BILL_DISTRICT", person.PERSON_BILL_DISTRICT),
                 new KeyValuePair<string, string>("PERSON_BILL_PROVINCE", person.PERSON_BILL_PROVINCE),
                 new KeyValuePair<string, string>("PERSON_BILL_POSTAL", person.PERSON_BILL_POSTAL),
                 new KeyValuePair<string, string>("PERSON_BILL_COUNTRY", person.PERSON_BILL_COUNTRY),
                 new KeyValuePair<string, string>("PERSON_BILL_TAXNO", person.PERSON_BILL_TAXNO),
                 new KeyValuePair<string, string>("PERSON_SALE_CODE", person.PERSON_SALE_CODE),
                 new KeyValuePair<string, string>("PERSON_ACC_NO", person.PERSON_ACC_NO),
                 new KeyValuePair<string, string>("EMP_CODE_SALE", person.EMP_CODE_SALE)
             };
             
            var encodedItems = requestContent.Select(i => WebUtility.UrlEncode(i.Key) + "=" + WebUtility.UrlEncode(i.Value));
            var encodedContent = new StringContent(String.Join("&", encodedItems), null, "application/x-www-form-urlencoded");
    

            Console.WriteLine(url);
            Response model = null;

            try
            {
                using (HttpResponseMessage response = await ApiRest.ApiClient.PostAsync(url, encodedContent))
                //using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        model = await response.Content.ReadAsAsync<Response>();
                        return model;
                    }
                    else
                    {
                        throw new Exception(response.ReasonPhrase);
                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return model;

            }

        }

        public static async Task<Response> delFingerScan(string code, string typeSearch, string fingID)
        {
            string url = ApiRest.BaseAddress + "delFingerScan.php?code=" + code + "&typeSearch=" + typeSearch + "&fingId=" + fingID;
            Response model = null;

            using (HttpResponseMessage response = await ApiRest.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    model = await response.Content.ReadAsAsync<Response>();
                    return model;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }

            }
        }

    }
}
