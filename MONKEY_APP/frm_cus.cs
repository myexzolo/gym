﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThaiNationalIDCard;

namespace MONKEY_APP
{
    public partial class frm_cus : Form
    {

        private string picCus = "";

        private DataTable dt;
        string txtStatus = "";

        private void bs_Click(object sender, EventArgs e)
        {
            DataView dv = dt.DefaultView;
            dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);
            dataGridView1.DataSource = dv.ToTable();
        }

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        public frm_cus()
        {
            InitializeComponent();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }



        private void frm_manage_cus_Load(object sender, EventArgs e)
        {
            getPersons();
        }


        private void getPersons()
        {
            try
            {
                dt = new DataTable();
                dt.Columns.Add("รหัส", typeof(string));
                dt.Columns.Add("ชื่อเล่น", typeof(string));
                dt.Columns.Add("ชื่อ", typeof(string));
                dt.Columns.Add("วันเกิด", typeof(string));
                dt.Columns.Add("Note", typeof(string));
                dt.Columns.Add("รูป", typeof(string));
                dt.Columns.Add("เพศ", typeof(string));
                dt.Columns.Add("ID", typeof(string));
                dt.Columns.Add("regDate", typeof(string));
                dt.Columns.Add("expDatre", typeof(string));
                dt.Columns.Add("status", typeof(string));
                dt.Columns.Add("EMP_CODE_SALE", typeof(string));
                dt.Columns.Add("PERSON_REGISTER_DATE", typeof(string));
                dt.Columns.Add("PERSON_EXPIRE_DATE", typeof(string));

                for (int i = 0; i < Global.PersonList.Count(); i++)
                {
                    Person person = Global.PersonList[i];

                    dt.Rows.Add(
                        person.PERSON_CODE, person.PERSON_NICKNAME, person.PERSON_TITLE + person.PERSON_NAME+" "+ person.PERSON_LASTNAME,
                        person.PERSON_BIRTH_DATE_TXT,
                        person.PERSON_NOTE, person.PERSON_IMAGE, person.PERSON_SEX, person.PERSON_CARD_ID,
                        person.PERSON_REGISTER_DATE_TXT, person.PERSON_EXPIRE_DATE_TXT, person.PERSON_STATUS, person.EMP_CODE_SALE,
                        person.PERSON_REGISTER_DATE, person.PERSON_EXPIRE_DATE
                    );
                }

                dataGridView1.DataSource = dt;

                dataGridView1.Columns[0].Width = 100;
                dataGridView1.Columns[1].Width = 100;
                dataGridView1.Columns[2].Width = 200;
                dataGridView1.Columns[3].Width = 100;

                dataGridView1.Columns[4].Visible = false;
                dataGridView1.Columns[5].Visible = false;
                dataGridView1.Columns[6].Visible = false;
                dataGridView1.Columns[7].Visible = false;
                dataGridView1.Columns[8].Visible = false;
                dataGridView1.Columns[9].Visible = false;
                dataGridView1.Columns[10].Visible = false;
                dataGridView1.Columns[11].Visible = false;
                dataGridView1.Columns[12].Visible = false;
                dataGridView1.Columns[13].Visible = false;

                dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: getPersons ::" + ex.ToString(), "frm_cus");
            }
        }


        public static byte[] ImageToByte(Image img)
        {
            byte[] byteArr;
            try
            {
                ImageConverter converter = new ImageConverter();
                var i2 = new Bitmap(img);
                byteArr = (byte[])converter.ConvertTo(i2, typeof(byte[]));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
            return byteArr;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = dt.DefaultView;
                dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);
                dataGridView1.DataSource = dv.ToTable();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                int rowindex = dataGridView1.CurrentCell.RowIndex;


                cusCode.Text = dataGridView1.Rows[rowindex].Cells[0].Value.ToString();
                txtNickName.Text = dataGridView1.Rows[rowindex].Cells[1].Value.ToString();
                txtName.Text = dataGridView1.Rows[rowindex].Cells[2].Value.ToString();
                bod.Text = Utils.getDateEntoTh(dataGridView1.Rows[rowindex].Cells[3].Value.ToString());
                txtNote.Text = dataGridView1.Rows[rowindex].Cells[4].Value.ToString();
                textSex.Text = dataGridView1.Rows[rowindex].Cells[6].Value.ToString();
                txtId.Text = dataGridView1.Rows[rowindex].Cells[7].Value.ToString();
                txtRegDateTxt.Text = dataGridView1.Rows[rowindex].Cells[8].Value.ToString();
                txtExpDateTxt.Text = dataGridView1.Rows[rowindex].Cells[9].Value.ToString();
                txtStatus = dataGridView1.Rows[rowindex].Cells[10].Value.ToString();
                txtEmpCodeSale.Text = dataGridView1.Rows[rowindex].Cells[11].Value.ToString();
                txtRegDate.Text = dataGridView1.Rows[rowindex].Cells[12].Value.ToString();
                txtExpDate.Text = dataGridView1.Rows[rowindex].Cells[13].Value.ToString();
                if (!dataGridView1.Rows[rowindex].Cells[5].Value.ToString().Equals(""))
                {
                    picCus = dataGridView1.Rows[rowindex].Cells[5].Value.ToString();
                    Utils.setImage(picCus, picPerson);
                }
                else
                {
                    picCus = "";
                    picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                    picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
                    picPerson.Size = new System.Drawing.Size(143, 152);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Global.PERSON.PERSON_CODE           = cusCode.Text;
            Global.PERSON.PERSON_NAME           = txtName.Text;
            Global.PERSON.PERSON_IMAGE          = picCus;
            Global.PERSON.PERSON_NICKNAME       = txtNickName.Text;
            Global.PERSON.PERSON_EXPIRE_DATE    = txtExpDate.Text;
            Global.PERSON.PERSON_REGISTER_DATE  = txtRegDate.Text;
            Global.PERSON.PERSON_STATUS         = txtStatus;
            Global.PERSON.EMP_CODE_SALE         = txtEmpCodeSale.Text;

            this.DialogResult = DialogResult.OK;
        }
    }
}
