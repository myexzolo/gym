﻿using iTextSharp.text.pdf;
using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThaiNationalIDCard;

namespace MONKEY_APP
{
    public partial class frm_history_trainer : Form
    {

        private DataTable dtm;
        public PackagePersons packagePersons;
        public string CURRENT_DIRECTORY = Directory.GetCurrentDirectory();

        public string printerName       = System.Configuration.ConfigurationManager.AppSettings["printerName"];
        public string TopMostDisplay    = System.Configuration.ConfigurationManager.AppSettings["TopMost"];

        string first = "";

        private void bs_Click(object sender, EventArgs e)
        {

        }

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        public frm_history_trainer()
        {
            InitializeComponent();
            if (TopMostDisplay.Equals("Y"))
            {
                this.TopMost = true;
            }
            else
            {
                this.TopMost = false;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }


        private void frm_manage_cus_Load(object sender, EventArgs e)
        {
            getPersons("ACTIVE");

        }

        private async void getPersons(string typeSearch)
        {
            try
            {
                ApiRest.InitailizeClient();
                List<Person> PersonList = await ApiProcess.getPerson(typeSearch);

                dtm = new DataTable();

                dtm.Columns.Add("รหัส", typeof(string));
                dtm.Columns.Add("ชื่อเล่น", typeof(string));
                dtm.Columns.Add("ชื่อ - นามสกุล", typeof(string));


                for (int i = 0; i < PersonList.Count(); i++)
                {
                    Person person = PersonList[i];


                    dtm.Rows.Add(
                        person.PERSON_CODE, person.PERSON_NICKNAME, person.PERSON_TITLE + person.PERSON_NAME + " " + person.PERSON_LASTNAME);
                }

                dataGridView1.DataSource = dtm;

                dataGridView1.Columns[0].Width = 90;
                dataGridView1.Columns[1].Width = 90;
                dataGridView1.Columns[2].Width = 200;


                dataGridView1.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;;


                if (!txtSearch.Text.Equals(""))
                {
                    button7_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: getPersons ::" + ex.ToString(), "frm_history_trainer");
            }
            finally
            {
                picLoading.Visible = false;
                ClearData();

            }


        }

        public byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }



        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }



        private void btnSearch_Click(object sender, EventArgs e)
        {
            //getInvoices();
        }


        
        private void showloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = true;
                }));
            }
            else
            {
                picLoading.Visible = true;
            }

        }

        private int rowindex = -1;


        private void button1_Click(object sender, EventArgs e)
        {
            DataView dv = dtm.DefaultView;
            dv.RowFilter = string.Format("[ชื่อ - นามสกุล] like '%{0}%' or รหัส like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);

            dataGridView1.DataSource = dv.ToTable();

            if (dataGridView1.Rows.Count > 0)
            {
                rowindex = -1;
            }
            else if (dataGridView1.Rows.Count == 0)
            {
                ClearData();
            }

            if (txtSearch.Text.Equals(""))
            {
                ClearData();
            }
        }

        private string perSonCode = "";

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            
        }

        private async void setPersonCheckout(string stausCheck, string typePakage, string perSonCode)
        {

            try
            { ApiRest.InitailizeClient();
                var psList = await ApiProcess.getPersonCheckin(Global.BRANCH.branch_code, stausCheck, perSonCode, "100", typePakage, "DESC", "");

                dataGridView2.Rows.Clear();
                DateTime dDate;
                int use = 0;
                for (var i = 0; i < psList.Count; i++)
                {
                    PersonCheckin ps = psList[i];



                    string checkinDate = "";
                    if (DateTime.TryParse(ps.checkin_date, out dDate))
                    {
                        DateTime ed = Convert.ToDateTime(ps.checkin_date);
                        checkinDate = ed.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("th-TH"));
                    }


                    if (ps.use_pack != null && ps.use_pack != "")
                    {
                        use = int.Parse(ps.use_pack);
                    }

                    int total = int.Parse(ps.num_use) - use;

                    dataGridView2.Rows.Add(checkinDate, ps.PERSON_NICKNAME, ps.PERSON_NAME + " " + ps.PERSON_LASTNAME,
                    ps.package_name, use, total, ps.num_use, ps.package_unit);
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: setPersonCheckin ::" + ex.ToString(), "frm_checkin");

            }
        }


        private void frm_history_trainer_Shown(object sender, EventArgs e)
        {
            ClearData();
        }

        private void ClearData()
        {
            dataGridView1.ClearSelection();
            dataGridView2.Rows.Clear();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = dtm.DefaultView;
                dv.RowFilter = string.Format("[ชื่อ - นามสกุล] like '%{0}%' or รหัส like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);

                dataGridView1.DataSource = dv.ToTable();

                if (dataGridView1.Rows.Count > 0)
                {
                    rowindex = -1;
                    dataGridView1_CellClick(sender, null);
                }
                else if (dataGridView1.Rows.Count == 0)
                {
                    ClearData();
                }

                if (txtSearch.Text.Equals(""))
                {
                   ClearData();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            perSonCode = "";
            dataGridView2.Rows.Clear();
            if (dataGridView1.CurrentCell != null)
            {
                rowindex = dataGridView1.CurrentCell.RowIndex;

                if (!dataGridView1.Rows[rowindex].Cells[0].Value.ToString().Equals(perSonCode))
                {
                    perSonCode = dataGridView1.Rows[rowindex].Cells[0].Value.ToString();
                    setPersonCheckout("'CO'", "'PT'", perSonCode);
                }
            }
        }
    }
}
